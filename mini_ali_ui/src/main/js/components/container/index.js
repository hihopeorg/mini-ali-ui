export default {
  mixins: [],
  data: {},
  props: {
    className: {
      default: ''
    },
    type: {
      default: 'line'
    },
    title: {
      default: ''
    },
    thumb: {
      default: ''
    },
    icon: {
      default: ''
    },
    custom: {
      default: ''
    },
    bindActionTap: () => {},
  },

    onTitleClick(e) {
      this.$emit('onActionTap', e)
    },
}
