# Container 容器

容器依据最佳实践统一了子元素的间距、圆角。


## 截图
![Image text](../../../../../../gifs/Container.gif)

## 属性介绍
| 属性名 | 类型 | 默认值 | 可选值 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义样式名 | 暂不支持 |
| type | String | line | line, onewithtwo | 容器排版类型 | - |
| title | String | '' | - | 当不为空时可展示 **title** 组件 | - |
| thumb | String | '' | - | 标题区域的 icon URL | - |
| icon | String | '' | arrow、close、more | 标题区域右侧的 icon 图标 | - |
| @on-action-tap | EventHandle | () => {} | - | 标题区域右侧可点击事件 | - |

## tips
* type 为 line 时会等分所有子元素；
* 当 `icon` 的值为空时，可添加 `slot="operation"` 元素展示在标题的右侧区域；
* 如使用 `title` 属性后，就不需要再在 json 中引入 **title** 组件；

## 示例

```xml
<element name='container' src="../../../../../../../mini_ali_ui/src/main/js/components/container/index.hml"></element>
<element name='title' src="../../../../../../../mini_ali_ui/src/main/js/components/title/index.hml"></element>

<div class="container">
  <container class="container-item"
    title="带有 title 标题，可自定义设置"
    thumb="https://img.alicdn.com/tfs/TB1Go8lh9R26e4jSZFEXXbwuXXa-84-84.png"
    @on-action-tap="titleClick"
  >
    <text slot="operation" style="color: red;">is slot</text>
    <text class="item">container 组件自带的 title 属性值效果。icon 无值或者为空时，可插入一个名为 operation 的 slot 元素。</text>
  </container>

  <container class="container-item"
    title="带有 title 标题，箭头"
    icon="arrow"
    thumb="https://img.alicdn.com/tfs/TB1Q19sTNv1gK0jSZFFXXb0sXXa-112-112.png"
    @on-action-tap="titleClick"
  >
    <text class="item">container 组件自带的 title 属性值效果</text>
  </container>

  <container class="container-item"
    title="带有 title 标题，关闭"
    icon="close"
    thumb="https://img.alicdn.com/tfs/TB1Go8lh9R26e4jSZFEXXbwuXXa-84-84.png"
    @on-action-tap="titleClick"
  >
    <text class="item">container 组件自带的 title 属性值效果</text>
  </container>

  <container class="container-item"
    title="带有 title 标题，更多"
    icon="more"
    thumb="https://img.alicdn.com/tfs/TB1Q19sTNv1gK0jSZFFXXb0sXXa-112-112.png"
    @on-action-tap="titleClick"
  >
    <text class="item">container 组件自带的 title 属性值效果</text>
  </container>

  <container class="container-item"
    title="带有 title 标题，无"
    thumb="https://img.alicdn.com/tfs/TB1Go8lh9R26e4jSZFEXXbwuXXa-84-84.png"
    @on-action-tap="titleClick"
  >
    <text class="item">container 组件自带的 title 属性值效果</text>
  </container>

  <container class="container-item">
    <text class="item">a1</text>
  </container>

  <container class="container-item">
    <div>
      <text class="item">b1</text>
      <text class="item">b2</text>
    </div>
  </container>

  <container class="container-item">
    <title slot="header" has-Line="true" show-Icon="true" icon-Url="https://gw.alipayobjects.com/mdn/miniProgram_mendian/afts/img/A*wiFYTo5I0m8AAAAAAAAAAABjAQAAAQ/original">
      <text>内部标题无操作</text>
    </title>
    <div>
      <text class="item">c1</text>
      <text class="item">c2</text>
      <text class="item">c3</text>
    </div>
    <text slot="footer" class="footer" style="padding-left: 12px;">底部展示区</text>
  </container>

  <container class="container-item">
    <title slot="header">
      <text>滑动</text>
    </title>
    <swiper indicator="{{ true }}" class="item" loop="false">
      <div style="background-color : #0abc80; width : 100%; height : 100%; border-radius : 8px;"></div>
      <div style="background-color : #00b7f4; width : 100%; height : 100%; border-radius : 8px;"></div>
    </swiper>
  </container>

  <container className="container-item" type="onewithtwo">
    <text class="grid-item" style ="width:210px; height: 150px;" slot="first">first</text>
    <text class="grid-item" slot="second" style="width: 150px; height: 71px;">second</text>
    <text class="grid-item" slot="third" style="width: 150px; height: 71px; margin-top: 8px;">third</text>
  </container>

</div>
```

```javascript
export default{
  data: {},
  titleClick() {
    prompt.showDialog({
      title: 'onActionTap 回调',
      message: "标题后面操作区域点击",
      buttons:[{text:'确定', color: '#1677ff'}]
    });
  },
}
```

```css
.container {
  background-color: #F5F5F5;
  flex-direction: column;
  padding: 12px;
  min-height: 100%;
}

.container-item {
  margin-bottom: 12px;
}

.footer {
  color: #333333;
  margin-top: 12px;
  font-size: 16px;
}

.item {
  width: 100%;
  background-color: #eeeeee;
  text-align: center;
  font-size: 16px;
  height: 100px;
  margin: 5px;
}

.grid-item {
  background-color: #eeeeee;
  text-align: center;
  font-size: 16px;
  border-radius: 4px;
}
```