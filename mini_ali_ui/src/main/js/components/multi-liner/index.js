import fmtEvent from '../../common/_util/fmtevent';

export default{
  props: {
    className: {default:''},
    labelCls: {default:''},
    inputCls: {default:''},
    last: {default:false},
    value: {default:''},
    name: {default:''},
    type: {default:'text'},
    password: {default:false},
    placeholder: {default:''},
    placeholderClass: {default:''},
    placeholderStyle: {default:''},
    disabled: {default:false},
    maxlength: {default:140},
    showCount: {default:true},
    autoHeight: {default:false},
    focus: {default:false},
    syncInput: {default:false},
    controlled: {default:true},
    enableNative: {default:false}, // 兼容安卓input的输入bug
//    onInput: {default:function onInput(){}},
//    onConfirm: () => {},
//    onFocus: () => {},
//    onBlur: () => {},
//    onClear: () => {},
  },
  data: {
    focus: false,
  },
  onInit() {
    //this.focus= this.focus;
  },
  onBlur(e) {
    const event = fmtEvent(this.props, e);
    this.$emit("blur",event);
  },
//  onConfirm(e) {
//    const event = fmtEvent(this.props, e);
//    this.onConfirm(event);
//  },
  onFocus(e) {
    const event = fmtEvent(this.props, e,this);
    this.$emit("focus",event);
  },
  onInput(e) {
    const event = fmtEvent(this.props, e , this);
    this.$emit("onInput",event);
  },
//  onClear(e) {
//    const event = fmtEvent(this.props, e);
//    this.onClear(event);
//  },
};
