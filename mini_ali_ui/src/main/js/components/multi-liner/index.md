# Multi-liner 多行文本

多行输入框，可输入多行内容。

## 预览

![Image text](../../../../../../gifs/multi-liner.gif)


## 属性
| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| --- | --- | --- | --- | --- | --- | --- |
| class-name | String | '' |  | 自定义的class | 暂不支持 |  |
| input-cls | String | '' |  | 自定义input的class | 暂不支持 |  |
| last | Boolean | false |  | 是否最后一行 |  |  |
| value | String | '' |  | 初始内容 |  |  |
| name | String | '' |  | 组件名字，用于表单提交获取数据 | 暂不支持 |  |
| placeholder | String | '' |  | 占位符 |  |  |
| placeholderStyle | String | '' |  | 指定 placeholder 的样式 | 暂不支持 |  |
| placeholderClass | String | '' |  | 指定 placeholder 的样式类 | 暂不支持 |  |
| disabled | Boolean | false |  | 是否禁用 |  |  |
| maxlength | Number | 140 |  | 最大长度 |  |  |
| focus | Boolean | false |  | 获取焦点 |  |  |
| auto-height | Boolean | false |  | 是否自动增高 |  |  |
| showcounter | Boolean | true |  | 是否渲染字数统计功能（**是否删除默认计数器/是否显示字数统计**） |  |  |
| controlled | Boolean |  |  | 是否为受控组件。为 true 时，value 内容会完全受 setData 控制 | 暂不支持 |  |
| @on-input | (e: Object) => void |  |  | 键盘输入时触发input事件 |  |  |
| @on-confirm | (e: Object) => void |  |  | 点击键盘完成时触发 | 暂不支持 |  |
| @focus | (e: Object) => void |  |  | 聚焦时触发 |  |  |
| @blur | (e: Object) => void |  |  | 失去焦点时触发 |  |  |


## 示例代码


```hml
<div class="container">
  <div style="margin-top: 10px;" ></div>
  <div class="title"><text>多行输入</text></div>
  <multi-liner data-field="area"
               placeholder="字数统计↘"
               type="text"
               value="{{value}}"
               @on-input="onInput"
               last="{{true}}"
               auto-height="{{true}}"
               controlled="{{controlled}}">
  </multi-liner>
  <div style="margin: 10px;" ></div>
</div>
```

```javascript
export default{
  data: {
    value: '内容',
    controlled: true,
  },
  onInput(e) {
    this.value= e.text;
  },
};

```