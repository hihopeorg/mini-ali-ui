# Filter 筛选

用作标签卡筛选。

## 截图
![Image text](../../../../../../gifs/Filter.gif)

## 属性介绍

### filter

| 属性名 | 类型 | 默认值 | 可选项 | 必选 | 描述 |
| ---- | ---- | ---- | ---- | ---- |---- |
| is-show | Boolean | true | false, true | - | 是否显示 |
| max |  Number | 10000 | - | - | 可选数量最大值，1为单选 |
| equal-rows |  Number | - | 2,3 | - | 把filter-item等分成2或者3列 |
| @on-change | (e: Object) => void |- | - | - | 多选时提交选中回调 |
| @on-mask | () => void | -| - | - | 点击遮罩层时触发，可用于关闭 filter |

### filter-item

| 属性名 | 类型 | 默认值 | 可选项 |必选 | 描述 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| class-ame | String | - | - | 暂不支持 | 自定义样式 |
| value | String | - | - | yes | 值 |
| id | String | - | - | - | 自定义标识符 |
| selected | Boolean | false | true,false | - | 默认选中 |
| @on-change | (e: Object) => void | - | - | - | 单选时提交选中回调 |

## 示例

```xml
<element name='filter' src="../../../../../../../mini_ali_ui/src/main/js/components/filter/index.hml"></element>
<element name='filter-item' src="../../../../../../../mini_ali_ui/src/main/js/components/filter/filter-item/index.hml"></element>

<div style="width : 100%; height : 100%;">
    <filter show="{{ isShow }}" max="{{ 1 }}" equal-Rows="{{ 3 }}" max-Height="150">
        <div for="{{ items }}">
            <filter-item value="{{ $item.value }}" subtitle="{{ $item.subtitle }}" id="{{ $item.id }}" equal-Rows="{{ 3 }}"
                         @on-Change="handleCallBack" selected="{{ $item.selected }}">
            </filter-item>
        </div>
    </filter>
</div>

```

```javascript
export default{
  data: {
    isShow: true,
    items: [
      { id: 1, value: '衣服啊', selected: true },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣服' },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣服' },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣服' },
      { id: 1, value: '橱柜' },
      { id: 1, value: '橱柜' },
    ],
  },
  handleCallBack(data) {
    prompt.showDialog({
      message: JSON.stringify(data.detail.results),
      buttons: [{text:'确定', color: '#1677ff'}]
    })
  },
  toggleFilter() {
    this.isShow = !this.isShow
  },
}

```
