import lifecycle from './mixins/lifecycle';

export default {
    data: {
    },
    props: {
        className: {
            default: ''
        },
        max: {
            default: 10000
        },
        equalRows: {
            default: 0
        },
        isShow : {
            default: true
        },
        maxHeight:{
            default: 0
        },
        mask: {
            default: false
        }
    },

    onPageShow() {
        const { commonProps } = lifecycle.data;
        const { max } = this;
        commonProps.max = max;
    },
    resetFn() {
        const { items, results } = lifecycle.data;
        items.forEach((element) => {
            element.setData('');
        });
        results.splice(0, results.length);
    },
    confirmFn() {
        const { results } = lifecycle.data;
        this.$emit('onChange', {results:results});
    },
    maskTap(e) {
        if (this.mask) {
            this.$emit('onMask', e);
        }
    },

    onPageHide() {
        lifecycle.didUnmount()
    }
}
