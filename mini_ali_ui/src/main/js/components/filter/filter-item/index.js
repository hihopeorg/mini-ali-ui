import lifecycle from '../mixins/lifecycle';

export default {
    data: {
        confirmStyle: '',
    },

    props: {
        className: {
            default: ''
        },
        item: {
            default: ''
        },
        id: {
            default: ''
        },
        value: {
            default: ''
        },
        selected: {
            default: false
        },
        subtitle: String,
        equalRows: {
            default: 0
        },
    },

    onPageShow() {
        const { results, items } = lifecycle.data;
        const { selected, id, value } = this;
        if (selected) {
            results.push({
                id, value
            });
            items.push({
                id, value, setData: this.setData
            });
            this.confirmStyle = true;
        }
    },

    handleClick() {
        const { id, value } = this;
        let { confirmStyle } = this;
        const { results, items, commonProps } = lifecycle.data;
        if (commonProps.max === 1) {
            if (confirmStyle === '') {
                items.forEach((element) => {
                    element.setData('');
                });
                results.splice(0, results.length);
                confirmStyle = true;
                results.push({
                    id, value
                });
                items.push({
                    id, value, setData: this.setData
                });
                this.$emit("onChange", {results:results})
            }
            this.confirmStyle = confirmStyle
            return;
        }
        if (confirmStyle === '' && results.length < commonProps.max) {
            confirmStyle = true;
            results.push({
                id, value
            });
            items.push({
                id, value, setData: this.setData
            });
        } else {
            confirmStyle = '';
            results.some((key, index) => {
                if (JSON.stringify(key) === JSON.stringify({
                    id, value
                })) {
                    results.splice(index, 1);
                    return true;
                } else {
                    return false;
                }
            });
        }
        this.confirmStyle = confirmStyle
    },

    setData(confirmStyle) {
        this.confirmStyle = confirmStyle
    },

    onPageHide() {
        lifecycle.didUnmount()
    }
}
