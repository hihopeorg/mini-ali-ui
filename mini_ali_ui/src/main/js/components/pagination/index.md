# Pagination 分页

分页标识符。

## 截图
![pagination 分页符](../../../../../../gifs/pagination.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| infinite | Boolean | false | - | 是否无限滚动分页 | - | - |
| fill-color | String | #ddd | - | 无限滚动分页符背景色 | - | - |
| front-color | String | #006EFF | - | 无限滚动分页符颜色，当前页高亮颜色 | - | - |
| pager-name | String | - | - | 无限滚动分页符名称 | - | - |
| height | String | 100px | - | 无限滚动分页容器的高度 | - | - |
| white | Boolean | false | - | 是否显示反白分页符 | - | - |
| max | Number | 5 | - | 常规分页符最大显示数量 | - | - |
| current-page | Number | 1 | - | 常规分页符当前页 | - | - |

## Bug & Tip
* 分页符组件共有两种模式：无限滚动模式和常规分页符：
  * 无限滚动模式 `<pagination infinite="{{true}}"></pagination>`；
  * 常规分页符：`<pagination />`；
* 无限滚动模式可通过 `fillColor` 和 `frontColor` 设置颜色；
* 常规分页符可通过 `frontColor` 改变当前页 icon 颜色；
* 当 `white` 反白为 `true` 时，不可再修改颜色；
* 无限滚动模式是双标签形式包含内容，可通过 `height` 设置容器高度；
* 如一个页面中存在多个无限滚动分页符，建议设置 `pagerName`，避免分页效果显示错误；

## 代码示例
```xml
<element name="pagination" src="../../../../../../../mini_ali_ui/src/main/js/components/pagination/index"></element>
<div style="flex-direction: column;">
<pagination pagerName='test1' infinite="{{true}}">
  <div style="display: flex;height: 100%;">
    <div style="text-align: center;padding-top: 20px;flex: 0 0;width: 324px;height: 100%;background-color: #f8d7d7;justify-content: center;"><image id="img_1" src="https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ" style="width: 144px;height: 35px;" /></div>
    <div style="text-align: center;padding-top: 20px;flex: 0 0;width: 324px;height: 100%;background-color: #d5f7d5;justify-content: center;"><image src="https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ" style="width: 144px;height: 35px;" /></div>
    <div style="text-align: center;padding-top: 20px;flex: 0 0;width: 180px;height: 100%;background-color: #f0e39e;justify-content: center;"><image src="https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ" style="width: 70px;height: 70px;" /></div>
    <div style="text-align: center;padding-top: 20px;flex: 0 0;width: 432px;height: 100%;background-color: #F8F8F8;justify-content: center;"><image src="https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ" style="width: 144px;height: 35px;" /></div>
    <div style="text-align: center;padding-top: 20px;flex: 0 0;width: 72px;height: 100%;background-color: #d3d3d3;justify-content: center;"><image src="https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ" style="width: 50px;height: 50px;" /></div>
  </div>
</pagination>

<div style="width: 50%;margin: 20px 0 0;background-color: #8f8f8f;align-self: center;">
  <pagination pagerName='test2' height="106px" white="{{ true }}" infinite="true" >
    <image src="https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ" style="width: 374px;height: 89px;margin: 10px 20px;" />
  </pagination>
</div>

<text style="margin-top: 12px;padding-bottom: 8px;font-weight: bold;font-size: 18px;">正常翻页样式</text>
<div style="padding: 20px 20px 0;background-color: #f8f8ba;justify-content: center;">
  <pagination current-page="{{3}}" max="{{10}}" height="200px"></pagination>
</div>

<text style="margin-top: 12px;padding-bottom: 8px;font-weight: bold;font-size: 18px;">反白翻页样式</text>
<div style="padding: 20px 20px 0;background-color: #0e98d8;justify-content: center;">
  <pagination current-page="{{5}}" white="true" ></pagination>
</div>
</div>

```
