import fmtUnit from '../../common/_util/fmtunit';

export default {
  props: {
    infinite: {default:false,},
    className: {default:'',},
    fillColor: {default:'#ddd',},
    frontColor: {default:'#1677FF',},
    pagerName: {default:'',},
    height: {default:fmtUnit('100px'),},
    white: {default:false,},
    max: {default:5,},
    currentPage: {default:1,},
    array:{default:[]},
  },
  data:{
    pageDeg:0,
  },
  updateArray(newV) {
    this.array = this.generateArray(0, newV - 1);
  },
  onInit() {
    this.updateArray(this.max, this.max);
    this.$watch('max','updateArray');
  },
  generateArray(start, end) {
    return Array.from(new Array(end + 1).keys()).slice(start);
  },
    onScroll() {
      //const infinitePageNumber = {};
      var pageInfiniteContent = this.$element('page_infinite_content');
      var slotContainer = this.$element('slot_container');
      var parentWidth = pageInfiniteContent.getBoundingClientRect().width;
      var childWidth = slotContainer.getBoundingClientRect().width;
      if (parentWidth >= childWidth) {
        this.pageDeg = 0;
      } else {
        var scrollOffsetX = pageInfiniteContent.getScrollOffset().x;
        var totalScrollable = childWidth - parentWidth;
        this.pageDeg = Math.ceil(scrollOffsetX / totalScrollable * 100);
      }
    },
};
