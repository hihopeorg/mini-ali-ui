# Card 卡片

卡片。



## 预览
![Image text](../../../../../../gifs/Card.gif)

## 属性

| 属性名             | 类型     | 默认值 | 可选值 | 描述                                           | 必选 |
| ------------------ | -------- | ------ | ------ | ---------------------------------------------- | ---- |
| thumb              | String   | -      | -      | Card缩略图地址                                 | -    |
| bg-img              | String   | -      | -      | Card背景图地址                                 | -    |
| title              | String   | -      | -      | Card标题                                       | true |
| subititle           | String   | -      | -      | Card副标题                                     | -    |
| action             | String   | -      | -      | 按钮文案, 当有两个按钮时action在左侧           | -    |
| extra-action        | String   | -      | -      | 额外按钮文案， 当有两个按钮时extraAction在右侧 | -    |
| info               | String   | -      | -      | 用于点击卡片时往外传递数据                     | -    |
| expand             | Boolean  | false  | -      | 卡片是否展开                                   | -    |
| @on-action     | Function | -      | -      | action的点击事件回调                           | -    |
| @on-extra | Function | -      | -      | extraAction的点击事件回调                      | -    |
| @on-card       | Function | -      | -      | Card点击的回调                                 | -    |



## 示例

### hml
```xml
<element name='card' src="../../../../../../../mini_ali_ui/src/main/js/components/card/index.hml"></element>
<div class="container">
    <card title="卡片标题1"
          @on-Card="onCardClick"
          info="点击了第一个card"></card>
    <card thumb="{{ thumb }}"
          title="卡片标题2"
          sub-Title="副标题非必填2"
          @on-Card="onCardClick"
          info="点击了第二个card"
          style="margin-top: 10px;"></card>
    <div>
        <card thumb="{{ thumb }}"
              title="卡片标题3"
              sub-Title="副标题非必填3"
              @on-Card="toggle"
              action="描述文字"
              @on-Action="onActionClick"
              extra-Action="点击卡片展开/收起↑"
              @on-Extra="onExtraActionClick"
              info="点击了第三个card"
              expand="{{ expand3rd }}"
              bg-Img="common/images/background.jpg"
              style="margin-top: 10px;"></card>
    </div>
    <div>
        <card thumb="{{ thumb }}"
              title="卡片标题3"
              sub-Title="副标题非必填3"
              @on-Card="onCardClick"
              info="点击了第三个card"
              bg-Img="common/images/background.jpg"
              style="margin-top: 40px;"
              expand='{{true}}'></card>
    </div>
</div>

```


### js
```javascript
export default{
  data: {
    thumb: 'common/images/head.jpg',
    expand3rd: false,
  },
  onCardClick(ev) {
    prompt.showToast({
      message: ev.detail.info,
      duration: 2000,
    });
  },
  onActionClick() {
    prompt.showToast({
      message: "action clicked",
      duration: 2000,
    });
  },
  onExtraActionClick() {
    prompt.showToast({
      message: "extra action clicked",
      duration: 2000,
    });
  },
  toggle() {
    this.expand3rd = !this.expand3rd
  },
}
```

