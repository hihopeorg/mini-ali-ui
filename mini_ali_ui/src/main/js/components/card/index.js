export default {
    props: {
        title: {
            default:''
        },
        info: {
            default:''
        },
        subTitle : {
            default:''
        },
        thumb: {
            default:''
        },
        action: String,
        extraAction: String,
        bgImg: String,
        expand: {
            default: false
        },
    },
    onCardClick() {
        this.$emit('onCard', {info: this.info})
    },
    onActionClick() {
        const { action } = this;
        if (action) {
            this.$emit('onAction', {info: this.action})
        }
    },
    onExtraActionClick() {
        const { extraAction } = this;
        if (extraAction) {
            this.$emit('onExtra', {info: this.extraAction})
        }
    },
}
