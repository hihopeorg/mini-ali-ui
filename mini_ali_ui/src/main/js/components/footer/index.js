import fmtUnit from '../../common/_util/fmtunit';
import getI18n from '../../common/_util/getI18n';
import router from '@system.router';

const i18n = getI18n().footer;

export default{
  props: {
    className: {default:''},
    // normal: 基础样式；
    // guide：文案加引导；
    // copyright：声明；
    // brand：带品牌；
    // link：带链接
    // end: 没有更多
    type: {default:'normal'},
    content: {default:''},
    extend: {default:[]},
    brandTap: {default:''},
    showEndIcon: {default:false},
    iconName: {default:'selected'},
    // 为了兼容 mini-antui 转 mini-ali-ui 而添加的 props
    copyright: {default:''},
    links: {default:[]},
    linkTop:{default:''},
    footerEndColor:{default:''},
    iconURL:{default:''},
    iconSize:{default:18},
  },
  data: {
    defaultSize: fmtUnit(18),
    maxSize: fmtUnit(22),
    valueUnit: fmtUnit('px'),
    isCustomLinkHandler: false,
    i18nEndLine: i18n.endLine,
  },
  onAttached() {
    this.isCustomLinkHandler = typeof this.linkTop == 'function'
    this.onLinkTap = this.linkTop
  },

  onBrandClick(e) {
    const brandLink = e.target.dataSet.url;
    let brandTap = this.brandTap
    let extend = this.extend
    if (brandTap !== '' && brandLink) {
      router.push({
        uri:brandLink
      })
    }
    if (brandTap !== '' && !brandLink && typeof brandTap === 'function') {
      this.brandTap(extend[e.target.dataSet.index]);
    }
  },
  onLinkTap(e) {

    if(typeof this.linkTop === 'function'){
        this.linkTop(e.target.dataSet.item)
    }
  },

  goto(e){
    router.push({
       uri:e
     })
  }
};
