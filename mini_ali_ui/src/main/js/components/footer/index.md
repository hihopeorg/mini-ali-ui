# Footer 页脚

显示页面页脚组件。

## 截图
![avatar](../../../../../../gifs/Footer.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| type | String | normal | normal、guide、copyright、brand、link、end | 选择使用指定的页脚类型 | - | - |
| content | String | - | - | 页脚文本内容 | - | - |
| extend | Array | - | - | 页脚部分的链接、logo 等信息 | - | - |
| brand-tap | function | () => {} | - | 品牌 logo 事件回调 | - | - |
| link-tap | function | () => {} | - | 链接事件回调 | - | - |
| show-end-icon | Boolean | false | - | type="end" 时的 footer 组件是否以 icon 方式展示，为 true 将不会显示 `content` 的文本内容 | - | - |
| icon-name | String | selected | - | 使用 am-icon，具体的值可参考 am-icon 的 type 值 | - | - |
| icon-u-r-l | String | - | - | 使用网络图片。当确定使用网络图片后，`iconName` 将失效；且 网络图片目前仅支持宽高相同且小于等于 44rpx； | - | - |
| icon-size | Number | 18 | - | 小于等于 22px 的值 | - | - |
| footer-end-color | String | - | - | type="end" 时文本的颜色 | - | - |


## Bug & Tip
* `brand-tap` 仅在 `type: brand` 中有效，且是无链接的品牌 logo；
* `link-ap` 仅在 `type: link` 中有效，用于点击链接时进行自定义处理，参数为当前 extend item；
* 当选择不同的 `type` 时，`extend` 中的值也将会有所不同；
  * `normal`：无 `extend`；
  * `guide`：`extend` 的值为 `[{ link: '', text: '',},]`；
  * `copyright`：无 `extend`；
  * `brand`：`extend` 的值为 `[{ logo: '', width: '', height: '', link: '',},]`，如果无 `link` 的话，可选择触发 `onBrandTap` 事件；
  * `link`：`extend` 的值为 `[{ link: '', text: '',},]`，但有多个值时，文本链接之间会有间隔线出现；
  * `end`：显示为“没有更多了”字样的结尾，可更改为 am-icon 中的类型或者自定图片 url；
    * `end` 类型 `content` 默认值为“没有更多了”；
    * `show-end-icon` 时，`content` 内容将不再显示；
    * `icon-u-r-l` 有值时，am-icon 中的类型将不会展示，显示为 icon 的 url，请确保该 url 是可访问的

## 代码示例

```xml
<element name='footer' src="../../../../../../../mini_ali_ui/src/main/js/components/footer/index.hml"></element>
<div class='page' style="width: 100%; flex-direction: column;">
    <footer
            type="{{footerInfo1.type}}"
            content="{{footerInfo1.content}}"
            ></footer>
    <div style="height: 35px;"></div>

    <footer
            type="{{footerInfo2.type}}"
            content="{{footerInfo2.content}}"
            extend="{{footerInfo2.extend}}"
            ></footer>
    <div style="height: 25px;"></div>
    <footer
            type="{{footerInfo3.type}}"
            content="{{footerInfo3.content}}"
            ></footer>
    <div style="height: 25px;"></div>
    <footer
            type="{{footerInfo4.type}}"
            content="{{footerInfo4.content}}"
            extend="{{footerInfo4.extend}}"
            brand-tap="{{ brandClick }}"
            ></footer>
    <div style="height: 25px;"></div>

    <footer
            type="{{footerInfo5.type}}"
            content="{{footerInfo5.content}}"
            extend="{{footerInfo5.extend}}"
            ></footer>
    <div style="height: 25px;"></div>
    <footer
            type="{{footerInfo6.type}}"
            content="{{footerInfo6.content}}"
            extend="{{footerInfo6.extend}}"
            link-top="{{ linkTap }}"
            ></footer>
    <footer
            type="{{footerInfo7.type}}"
            content="{{footerInfo7.content}}"
            footer-end-color="{{footerInfo7.footerEndColor}}"
            ></footer>
    <footer
            type="{{footerInfo8.type}}"
            ></footer>
    <footer
            type="{{footerInfo8.type}}"
            content="{{footerInfo8.content}}"
            show-end-icon="{{footerInfo8.showEndIcon}}"
            icon-size="{{footerInfo8.iconSize}}"
            ></footer>

</div>
```

```css
.page {
  padding-top: 20px;
  background-color: #ffffff;
  justify-content: center;
  align-items: center;
}
.am-footer {
  margin-bottom: 40px;
}
```

```javascript
import prompt from '@system.prompt';

export default{
  data: {
    footerInfo1: {
      type: 'normal',
      content: '底部文案置底说明',
    },
    footerInfo2: {
      type: 'guide',
      content: '没找到需要的？搜一下试试',
      extend: [
        {
          link: 'pages/list/entry',
          text: '蚂蚁借呗',
        },
        {
          link: 'pages/list/entry',
          text: '备用金',
        },
        {
          link: 'pages/list/entry',
          text: '花呗收钱',
        },
      ],
    },
    footerInfo3: {
      type: 'copyright',
      content: '© 2004-2020 Alipay.com. All rights reserved.',
    },
    footerInfo4: {
      type: 'brand',
      content: '过往业绩不预示产品未来表现，市场有风险，投资需谨慎',
      extend: [
        {
          logo: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ',
          width: '30px',
          height: '30px',
          link: 'pages/list/entry',
        },
        {
          logo: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ',
          width: '210px',
          height: '58px',
        },
      ],
    },
    footerInfo5: {
      type: 'link',
      content: '© 2004-2020 Alipay.com. All rights reserved.',
      extend: [
        {
          link: 'pages/list/entry',
          text: '底部链接',
        },
      ],
    },
    footerInfo6: {
      type: 'link',
      content: '© 2004-2020 Alipay.com. All rights reserved.',
      extend: [
        {
          link: 'pages/list/entry',
          text: '底部链接',
        },
        {
          link: 'pages/list/entry',
          text: '底部链接',
        },
      ],
    },
    footerInfo7: {
      type: 'end',
      content: '自定义的没有更多内容的底线',
      footerEndColor: 'red',
    },
    footerInfo8: {
      type: 'end',
      showEndIcon: true,
      iconSize: 50,
    },
  },
  brandClick() {
    prompt.showDialog({
      message: '这个品牌 logo 没有链接，可通过 js 自定义点击事件。',
      buttons:[{text:'确定', color: '#1677ff'}]
    })
  },
  linkTap(e) {
    prompt.showDialog({
      title: 'onLinkTap 回调',
      message: '{link: \'/pages/list/app\', text: \'底部链接\'}',
      buttons:[{text:'确定', color: '#1677ff'}]
    })
  },
}

```
