# steps 步骤进度条

根据步骤显示的进度条。


## 预览
![steps 步骤进度条](../../../../../../gifs/steps.gif)


## 属性

| 属性名      | 类型                                                | 默认值     | 可选项                    | 描述                                       | 最低版本 | 必填  |
| ----------- | --------------------------------------------------- | ---------- | ------------------------- | ------------------------------------------ | -------- | ----- |
| class-name   | String                                              |            | -                         | 最外层覆盖样式                             | -        | false |
| active-index | Number                                              | 1          | -                         | 当前激活步骤                               | -        | true  |
| fail-index   | Number                                              | 0          | -                         | 当前失败步骤(只在vertical模式下生效)       | -        | false |
| direction   | String                                              | horizontal | vertical <br/> horizontal | 显示方向，可选值：`vertical`、`horizontal` | -        | false |
| size        | Number                                              | 0          | -                         | 统一的icon大小，单位为px                   | -        | false |
| items       | Array[{title, description, icon, activeIcon, size}] | []         | -                         | 步骤详情                                   | -        | true  |
| show-step-number | Boolean | false | - | 是否以数字序列展示步骤 icon |  | false |
| horiz-highlight | Boolean | false | - | 用于控制水平方向是否启用高亮展示 title | | false |
| iconFail | String | close | | false |
| icon-success | String | check | | false |
| icon-successBg | String |  | | false |
| icon-success-color | String |  || false |

### slot
steps 组件中的 slot 插槽可根据具体的步骤数设置，如有 4 个步骤点，那么可插入 4 个 slot。

slot 名称的格式为：`desc`、`tilte`、`tilte2`

```hml
<div slot="title" style="color: green; font-weight: bold;font-size: 12px;text-overflow: ellipsis;"><text>titile 的内容</text></div>
<div slot="desc"><text style="text-overflow: ellipsis;"><span>当前 item 没有</span><span style="color: green;">description</span><span>时，使用 slot 内容。</span></text></div>
```


### items属性

| 属性名            | 类型   | 默认值 | 可选项 | 描述                                                       | 最低版本 | 必须 |
| ----------------- | ------ | ------ | ------ | ---------------------------------------------------------- | -------- | ---- |
| items.title       | String | -      | -      | 步骤详情标题                                               | -        | true |
| items.description | String | -      | -      | 步骤详情描述                                               | -        | true |
| items.icon        | String | -      | -      | 尚未到达步骤的icon(只在vertical模式下生效)                 | -        | true |
| items.activeIcon  | String | -      | -      | 已到达步骤的icon(只在vertical模式下生效)                   | -        | true |
| items.size        | Number | -      | -      | 已到达步骤icon的图标大小，单位为px(只在vertical模式下生效) | -        | true |

### tips
* 当 `show-step-number` 为 `true` 时将会忽略 items 属性中 icon 相关属性，仅以数字序列方式展示；
* slot 中的数字与 items 的序列相对应，将会在 items 属性中没有 description 时展示；
* `icon-success-bg` 改变的不仅仅是当前已激活的步骤背景色，同时也将会修改线条颜色；如果 `items.activeIcon` 所使用的图片是含有透明背景的图片时，背景颜色也将会显示；
* 当使用的 icon 非图片形式时，`iconSuccessColor` 可修改 icon 的颜色以及数字序列的数字文本颜色；
* `icon-success-bg` 和 `icon-success-color` 仅修改当前已激活的步骤，尚未达到的步骤保持灰色模式，且不可配置；
* 步骤中失败的提示仅提供修改 icon 类型，无修改颜色的配置；


## 示例


### hml
```hml
<div class="container">
    <steps
            class="second"
            class-name="demo-steps-class"
            active-index="{{activeIndex}}"
            items="{{items}}"
            >
        <div slot="title" style="color: green; font-weight: 800;font-size: 12px;text-overflow: ellipsis;"><text>titile 的内容</text></div>
    </steps>
    <steps
            class="second"
            class-name="demo-steps-class"
            class-named="{{classNamed}}"
            active-index="{{activeIndex}}"
            fail-index="{{failIndex}}"
            horiz-highlight="{{true}}"
            items="{{items2}}"
            >
        <div slot="title" style="color: green; font-weight: bold;font-size: 12px;text-overflow: ellipsis;"><text>titile 的内容</text></div>
        <div slot="desc"><text style="text-overflow: ellipsis;"><span>当前 item 没有</span><span style="color: green;">description</span><span>时，使用 slot 内容。</span></text></div>
    </steps>
    <steps
            class-name="demo-steps-class"
            direction="vertical"
            fail-index="{{failIndex}}"
            active-index="{{activeIndex}}"
            items="{{items2}}"
            size="{{size}}"
            show-step-number="{{showNumberSteps}}"
            >
        <div slot="title2" style="color: green; font-weight: bold;text-overflow: ellipsis;"><text>titile 的内容</text></div>
        <div slot="desc"><text  style="text-overflow: ellipsis;"><span>当前 item 没有</span><span style="color: green;">description</span><span> 时，使用 slot 内容。</span> </text></div>
    </steps>
    <div class="demo-btn-container">
        <am-button valued="上一步" class="demo-btn" @on-tap="preStep">上一步</am-button>
        <am-button valued="下一步" class="demo-btn" @on-tap="nextStep">下一步</am-button>
    </div>
    <div class="demo-btn-container">
        <am-button valued="设置错误项" class="demo-btn" @on-tap="setFailIndex">设置错误项</am-button>
        <am-button valued="取消错误项" class="demo-btn" @on-tap="cancelFailIndex">取消错误项</am-button>
    </div>
    <div class="demo-btn-container">
        <am-button valued="设置图标大小+" class="demo-btn" @on-tap="setIconSizeAdd">设置图标大小+</am-button>
        <am-button valued="设置图标大小-" class="demo-btn" @on-tap="setIconSizeReduce">设置图标大小-</am-button>
    </div>
    <am-button type="primary" valued="以{{!showNumberSteps?'数字':'图片/icon'}}方式展示步骤序列" @on-tap="showNumberList">以{{!showNumberSteps?'数字':'图片/icon'}}方式展示步骤序列</am-button>
</div>
```


### js
```javascript
export default {
  data: {
    activeIndex: 1,
    failIndex: 0,
    classNamed:true,
    size:20,
    items: [{
              index:0,
              title: '步骤1',
            }, {index:1}, {
      index:2,
              title: '步骤3',
            }],
    items2: [{
               index:0,
               title: '步骤1 如果标题足够长的话也会换行的',
               description: '这是步骤1的描述文档，文字够多的时候会换行，设置了成功的icon',
               activeIcon: 'https://i.alipayobjects.com/common/favicon/favicon.ico',
               size: 20,
             }, {
               index:1,
               description: '这是步骤2，同时设置了两种状态的icon',
               icon: 'https://gw.alipayobjects.com/mdn/miniProgram_mendian/afts/img/A*lVojToO-qZIAAAAAAAAAAABjAQAAAQ/original',
               activeIcon: 'https://i.alipayobjects.com/common/favicon/favicon.ico',
             }, {
               index:2,
               title: '步骤3',
               description: '这是步骤3',
             }, {
               index:3,
               title: '步骤4',
             }],
    showNumberSteps: true,
  },
  nextStep() {
          this.activeIndex=this.activeIndex + 1;
  },
  preStep() {
      if(this.activeIndex>=0){
          this.activeIndex=this.activeIndex - 1;
      }
  },
  setFailIndex() {
    this.failIndex= 3;
  },
  cancelFailIndex() {
      this.failIndex= -1;
  },
  setIconSizeAdd() {
    this.size= this.size < 30 && this.size > 14 ? this.size + 1 : 15;
  },
  setIconSizeReduce() {
    this.size= this.size > 15 ? this.size - 1 : 15;
  },
  showNumberList() {
    this.showNumberSteps=!this.showNumberSteps;
  },
};
```

### css
```css
.demo-steps-class {
  margin: 20px 0;
  border-bottom: 1px solid #e5e5e5;
}
.demo-btn-container {
  display: flex;
  justify-content: space-between;
  margin: 20px;
}
.demo-btn {
  width: 47%;
}
.container{
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: #F5F5F5;
}
.second{
  margin-top: 20px;
}
.title-h{
  font-size: 20px;
  margin-top: 20px;
}
.demo-btn{
  height: 45px;
  text-align: center;
  width: 200px;
  background-color: white;
  font-size: 18px;
  margin-left: 10px;
}
.demo-ty{
  font-size: 18px;
  text-align: center;
  color: white;
  width: 100%;
  height: 45px;
  background-color: #1677ff;
  border-radius: 5px;
}
```