//import fmtUnit from '../_util/fmtUnit';
export default {
    props: {
        className: '',
        classNamed:{default:false},
        activeIndex:{default:1},
        failIndex: {default:-1},
        size: {default:20},
        direction: {default:'horizontal'},
        items: [],
        showStepNumber: {default:true},
        horizHighlight: {default:false},
        // 用于控制水平方向是否启用高亮展示 title
        iconFail: {default:'close'},
        // 提示失败的 icon 图标
        iconSuccess: {default:'check'},
        // 提示成功的 icon 图标
        iconSuccessBg: '#1677ff',
        // 成功 icon 图标的背景颜色，且同时修改步骤线条颜色
        iconSuccessColor: '' // 成功 icon 的文本颜色

    },
    data: {
//        size12: fmtUnit(12),
//        size8: fmtUnit(8),
//        valueUnit: fmtUnit('px')
    },
    onInit(){
        console.log(this.size);
    },
};