# Loading 加载

loading 加载动画。

## 扫码体验

![image](../../../../../../gifs/loading.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String| - | - | 自定义class | 暂不支持 | - |
| size | String | 100px | - | 设置 loading 尺寸大小 | - | - |
| color | String | #1677ff | - | 设置 loading 的颜色 | - | - |

## 代码示例
```xml
<element name="am-loading" src="../../../../../../../mini_ali_ui/src/main/js/components/loading/index"></element>

<div class="page" style="width: 100%;">
  <div style="flex-direction:row;height: 50px;background-color: #ddd;">
    <text style="font-size: 20px;">加载中</text>
    <am-loading size="80px" class-name="inlineBlock" ></am-loading>
  </div>
  <div style="padding-left: 10px;padding-right: 10px;">
  <text class="title first" value='loading size="6rpx"'></text>
    <am-loading size="6px" color="red" ></am-loading>
  <text class="title" value='loading height="36rpx"'></text>
    <am-loading height="36px" color="red" ></am-loading>
  <text class="title" value='loading height="36rpx" size="6rpx"'></text>
    <am-loading height="36px" size="6px" color="red"></am-loading>
  <text class="title" value='loading size="100px"'></text>
    <am-loading size="100px" color="red" ></am-loading>
  <text class="title" value='loading'></text>
    <am-loading color="blue"></am-loading>
  </div>

</div>
```

```css
@import '../../app.css';

.inlineBlock {
  display: flex;
  vertical-align: middle;
}

.title {
  font-size: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 10px;
  margin-top: 10px;
  margin-bottom: 10px;
  border-bottom: 0.3px solid #eeeeee;
  margin-left: -10px;
  margin-right: -10px;
  font-weight: 700;
}

.first {
  margin-to:0px;
}
```