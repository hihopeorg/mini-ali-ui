import fmtUnit from '../../common/_util/fmtunit';

export default {
    props: {
        size: {
            default: fmtUnit('100px'),
        },
        className: {
            default: '',
        },
        color: {
            default: '#999',
        },
        height: {
            default: '',
        },
    },
    calc(origin, ratio) {
        if (!origin) {
            return origin;
        }
        var originStr = origin + "";
        var unit = "";
        var number = originStr;
        if (originStr.length > 1 && originStr.substring(originStr.length - 1) === "%") {
            unit = "%";
            number = originStr.substring(0, originStr.length - 1);
        } else if (originStr.length > 2 && originStr.substring(originStr.length - 2) === "px") {
            unit = "px";
            number = originStr.substring(0, originStr.length - 2);
        }
        number = parseInt(number) * ratio;
//                if (number > 4) {
//                    number = 4;
//                }
//                if (number < 3) {
//                    number = 3;
//                }
        var result = number + unit;
        return result;
    },
    showTarget() {
    }
};
