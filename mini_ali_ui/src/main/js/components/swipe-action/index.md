## swipe-action 可滑动单元格

可滑动单元格

## 截图
![Image text](../../../../../../gifs/Swipe-Action.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 |
| right | Array | - | - | 滑动选项，最多三项 | - |
| @on-item-tap | EventHandle | ({index, detail, extra, done}) => void | - | 右侧滑开后的元素点击事件 | - |
| @on-swipe | EventHandle | (e: Object) => void | - | 滑动的回调 | - |
| swiping | Boolean | false | - | 还原组件到初始状态 | - |
| border-radius | Boolean | false | - | 右侧 item 是否为圆角 | - |
| swipe-width | String | | - | 设置 swipe-action 组件的宽度 | - |

## Bug & Tip
* 当有多个 SwipeAction 组件时，当滑动其中一个时，需要将其他的组件的 `restore` 属性设置为 `true`，避免一个页面同时存在多个swipeAction处于活动状态；
* `right` 数组中的格式：`[{ type: '', text: '', fColor: '', bgColor: '',},]`
* `right`中的 `type` 共有三个值：`edit`、`delete` 和 `other` ；
* `fColor` 为文本颜色，`bgColor` 为背景色颜色，三种 `type` 均可自定义颜色，如未设置，默认值为：
  * `edit`：`#ccc`；
  * `delete`：`#FF3B30`；
  * `other`：`#1677FF`；
* SwipeAction 是与 list 组件组合使用的
* `border-radius` 是为了结合带圆角的 list-item 而存在的，如果为 `true` 时，将会把右侧 item 显示为圆角模式；

## 代码示例
```xml
<element name='ali-list' src="../../../../../../../mini_ali_ui/src/main/js/components/list/index.hml"></element>
<element name='ali-list-item' src="../../../../../../../mini_ali_ui/src/main/js/components/list/list-item/index.hml"></element>
<element name='swipe-action' src="../../../../../../../mini_ali_ui/src/main/js/components/swipe-action/index.hml"></element>
<div style="height: 100%; background-color: #F5F5F5;">
	<ali-list>
		<div for="{{list}}" key="{{$item.content}}">
			<swipe-action
				index="{{$idx}}"
				right="{{$item.right}}"
				swiping="{{$item.swiping}}"
				@on-item-tap="onRightItemClick"
				@on-Swipe="onSwipe"
				@on-swipe-tap="onItemTap"
				border-Radius="{{$idx <= 2 ? true : false}}"
			>
				<ali-list-item
					arrow="horizontal"
					index="{{$idx}}"
					key="items-{{$idx}}"
					last="{{$idx === list.length - 1}}"
					upper-Subtitle="{{$idx >= 4?'这是一个有副标题的列表':''}}"
					lower-Subtitle="{{$idx === 5?'这是一个小的副标题':''}}"
					border-Radius="{{$idx <= 2 ? true : false}}"
				>
					<text>{{$item.content}}</text>
				</ali-list-item>
			</swipe-action>
		</div>
	</ali-list>
</div>
```

```javascript
export default{
  data: {
    swipeIndex: null,
    list: [
      { right: [{ type: 'delete', text: '删除', bgColor: '#f00', fColor: 'black' }], content: '更换文字颜色', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏', fColor: 'rgba(0,0,0,0.5)' }, { type: 'delete', text: '删除', fColor: '#FFFF00' }, { type: 'other', text: '新增一个',fColor: '#ffffff' }], content: '改变文字颜色', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏', bgColor: '#333333', fColor: 'white'}, { type: 'delete', text: '删除', fColor: '#ffffff'}], content: '其中一个背景色变化', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏', bgColor: '#cccccc', fColor: '#f00' }, { type: 'delete', text: '删除', bgColor: '#0ff', fColor: '#333' }], content: '文字和背景色同时改变', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏取消收藏取消', fColor: 'white'}, { type: 'delete', text: '删除删除删除删除', fColor: 'white'}], content: '默认颜色样式', swiping: false },
      { right: [{ type: 'edit', text: '取消关注', fColor: 'white'}, { type: 'other', text: '免打扰', fColor: 'white' }, { type: 'delete', text: '删除', fColor: 'white' }], content: '三个选项的卡片', swiping: false },
      { right: [{ type: 'edit', text: '取消关注', fColor: 'white' }, { type: 'other', text: '免打扰', fColor: 'white' }, { type: 'delete', text: '删除', fColor: 'white' }], content: '三个选项的卡片三个选项的卡片三个...', swiping: false },
    ],
  },
  onRightItemClick(e) {
    const type = e.detail.type;
    prompt.showDialog({
      title: '温馨提示',
      message: JSON.stringify(this.list[this.swipeIndex].right),
      buttons: [{text:'取消', color: '#000000'}, {text:'确定', color: '#1677ff'}],
      success:  (result) => {
        if (result.index == 1) {
          if (type === 'delete') {
            prompt.showToast({
              message: '确认 => 可进行删除数据操作',
              duration: 2000
            })
          }
          this.list[this.swipeIndex].swiping = false;
        } else {
          prompt.showToast({
            message: '取消 => 滑动删除状态保持不变',
            duration: 2000
          })
        }
      }
    })
  },
  onItemTap(e) {
    prompt.showDialog({
      message: `dada${e.detail.index}`,
      buttons: [{text:'确定', color: '#1677ff'}]
    })
    for (var i = 0; i < this.list.length; i++) {
        this.list[i].swiping = false;
    }
  },
  onSwipe(e) {
    const { list } = this
    this.swipeIndex = e.detail.index;
    for (var i = 0; i < this.list.length; i++) {
      if (this.swipeIndex === i) {
        list[i].swiping = true;
      } else {
        list[i].swiping = false;
      }
    }

  },
}
```
