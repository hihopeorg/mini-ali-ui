import fmtUnit from '../../common/_util/fmtunit';

export default {
    data: {
        actionWidth: 0,
        radiusItemSpace: fmtUnit('12px'),
        touchObject: {
            startX: 0,
            endX: 0,
        }
    },
    props: {
        className: {
            default: ''
        },
        right: {
            default: []
        },
        borderRadius: {
            default: false
        },
        index: null,
        height: {
            default: ''
        },
        swipeWidth: {
            default: ''
        },
        swiping: {
          default: false
        },
        textHeight: String
    },

    onInit() {
        this.$watch('swiping', 'onSwipingChange')
    },

    onPageShow() {
        this.textHeight = this.$element('swipeItem').getBoundingClientRect().height
        this.swipeWidth = this.$element('action').getBoundingClientRect().width;
    },

    onSwipingChange(newValue) {
        this.swiping = newValue;
        if(!this.swiping) {
            this.$element('swipe').scrollBy({
                dx: -375,
                smooth: true
            })
        }
    },

    onSwipeTap() {
        this.swiping = false;
        this.$emit('onSwipeTap', {
            index: this.index
        })
    },

    onSwipeStart(e) {
        this.touchObject.startX = e.touches[0].globalX;
    },

    onSwipeEnd(e) {
        const { touchObject } = this;
        touchObject.endX = e.changedTouches[0].globalX;
        const distance = touchObject.endX - touchObject.startX;
        const { index } = this;
        if (distance >= 5) {
            this.$element('swipe').scrollBy({
                dx: -375,
                smooth: true
            })
            this.swiping = false
        } else if(distance <= -5){
            this.$element('swipe').scrollBy({
                dx: 375,
                smooth: true
            })
            this.swiping = true
            this.$emit('onSwipe', {index: index})
        } else {
            if (this.swiping) {
                for(var i = 0; i < this.right.length; i++) {
                    const width = this.$element('swipe-btn-' + i).getBoundingClientRect().width;
                    const left = this.$element('swipe-btn-' + i).getBoundingClientRect().left;
                    if (touchObject.endX >= left && touchObject.endX <= left + width) {
                        this.$emit('onItemTap', {type: this.right[i].type, index: this.index, item: i})
                    }
                }
            }
        }
    },
}
