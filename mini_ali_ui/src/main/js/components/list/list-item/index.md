## ListItem 列表项

列表项

![img](../../../../../../../gifs/list.gif)

## 属性

| 属性名        | 类型    | 默认值 | 可选值                     | 描述                                               | 最低版本 | 必填           |
| ------------- | ------- | ------ | -------------------------- | -------------------------------------------------- | -------- | -------------- |
| arrow         | Boolean | true   | -                          | 是否启用箭头                                       | -        | -              |
| thumb         | String  | -      | -                          | 缩略图地址                                         | -        | -              |
| index         | String  | -      | -                          | 用于记录位置的index, 在事件回调中会将这个index回传 | -        | -              |
| border-radius  | Boolean | false  | -                          | 列表项是否圆角                                     | -        | -              |
| upper-subtitle | String  | -      | -                          | 上副标题                                           | -        | -              |
| lower-subtitle | String  | -      | -                          | 下副标题                                           | -        | -              |
| title-position | String  | top    | top<br/> middle<br/>bottom | 主标题位置                                         | -        | -              |
| thumb-size     | String  | 40px | - | 缩略图大小 | - | 有缩略图时必填 |
| @on-tap       | Function| -      | -                          | 点击列表项事件                                       | -        | -              |
| last | Boolean | false | - | 用于处理下划线是否显示 | - | - |

### Tips
* 如要删除最后一个 list-item 的下划线，可以通过 list 长度判断，将最后一个 list-item 的 `last` 属性设为 `true` 即可；

## Slot

list-item共有6个插槽，位置和名称如下图所示

| 插槽名称           | 描述                                     |
| ------------------ | ---------------------------------------- |
| supporting         | 列表头部插槽 ohos需结合属性slot-supporting="{{true}}" |
| default            | 默认插槽，用于放置主标题 |
| afterTitle         | 主标题后面的插槽，可用于放置标签，图标 ohos需结合属性slot-after-title="{{true}}" |
| afterUpperSubtitle  | 上副标题后面的插槽，可用于放置标签，图标 ohos需结合属性slot-after-upper-subtitle="{{true}}" |
| afterLowerSubtitle | 下副标题后面的插槽，可用于放置标签，图标 ohos需结合属性slot-after-lower-subtitle="{{true}}" |
| extra              | 列表尾部插槽，用于放置辅助信息 ohos需结合属性slot-extra="{{true}}" |

## 代码示例

### hml
```xml
<am-list-item elif="{{$item.actionType ==='check'}}"
	thumb="{{thumbUrl}}"
	index="{{$idx}}"
	@on-tap="onCheckClick"
	value="{{checkValues[index]}}"
	key="items-{{$idx}}"
	last="{{$idx === (items.length - 1)}}"
	slot-extra="true"	>
	<text>{{$item.title}}</text>
	<am-radio slot="extra" checked="{{changeCheckbox}}"></am-radio>
</am-list-item>
```

### js
```javascript
export default{
  onCheckClick() {

  }
}
```