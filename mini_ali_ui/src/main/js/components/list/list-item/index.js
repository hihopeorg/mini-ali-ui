import fmtUnit from '../../../common/_util/fmtunit';

export default {
    props: {
        className: {
            default: '',
        },
        align: {
            default: false,
        },
        disabled: {
            default: false,
        },
        multipleLine: {
            default: false,
        },
        wrap: {
            default: false,
        },
        enforceExtra: {
            default: false,
        },
        titlePosition: {
            default: 'top',
        },
        last: {
            default: false,
        },
        thumb: {
            default:'',
        },
        slotPrefix : {
             default: false,
        },
        slotSupporting:{
            default: false,
        },
        slotAfterTitle: {
            default: false,
        },
        slotAfterUpperSubtitle: {
            default: false,
        },
        slotAfterLowerSubtitle: {
            default: false,
        },
        slotExtra: {
            default: false,
        },
        thumbSize: {
            default: fmtUnit('40px'),
        },
        upperSubtitle: {
            default: '',
        },
        lowerSubtitle: {
            default: '',
        },
        arrow: {
            default: false,
        },
        index: {
            default: 0,
        },
        borderRadius : {
            default: false,
        },
        dataset: {
            default: {},
        }
    },
    data: {
        iconSize: fmtUnit(18),
    },
    onItemTap(ev) {
        const disabled = this.disabled;
        if (!disabled) {
            this.$emit('onTap', {
                index: ev.target.dataSet.index,
                target: {
                    dataset: this.dataset
                },
            });
        }
    },
};
