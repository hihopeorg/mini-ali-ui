## List 列表

列表。

## list

| 属性名      | 类型   | 默认值                               | 可选值 | 描述                                                         | 最低版本 | 必填 |
| ----------- | ------ | ------------------------------------ | ------ | ------------------------------------------------------------ | -------- | ---- |
| class-name   | String | -                                    | -      | 自定义class                                                  | - | -  |
| load-more    | String | -                                    | -      | 显示加载更多 item。`load`：显示加载更多；`over`：显示加载完成无更多 | - | -  |
| load-content | Array  | ['加载更多...','-- 数据加载完了 --'] | -      | 需结合 `loadMore` 属性使用，用于文案展示                     | - | -  |
| loading-size | String | 16px | - | loading icon 的大小 | - | - |

### loadMore 使用介绍
当需要使用无限循环列表时，可将 `list` 组件放置入到 [`div`](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-container-div-0000000000611484) 中，根据需求添加相对应的属性，比如：
```xml
<div style="height: 100%;overflow: scroll;flex-direction: column;"
	onreachbottom="onScrollToLower">
  <am-list load-more="{{loadMore}}" load-content="{{loadContent}}">
    <list-item>...</list-item>
  </am-list>
</div>
```
```javascript
import prompt from '@system.prompt';
// const imgUrl = '';
const newitems = [
  {
    thumb:
      'https://gw.alipayobjects.com/zos/rmsportal/KXDIRejMrRdKlSEcLseB.png',
    title: '固定到头部',
    arrow: true,
    sticky: true,
  },
  {
    title: '标题文字不换行很长很长很长很长很长很长很长很长很长很长',
    arrow: true,
  },
  {
    title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
    arrow: true,
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '没有箭头',
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '子元素垂直对齐',
    textMode: 'wrap',
    align: 'top',
  },
  {
    title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
    arrow: true,
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '没有箭头',
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '子元素垂直对齐',
    textMode: 'wrap',
    align: 'top',
  },
  {
    title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
    arrow: true,
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '没有箭头',
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '子元素垂直对齐',
    textMode: 'wrap',
    align: 'top',
  },
  {
    title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
    arrow: true,
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '没有箭头',
    textMode: 'wrap',
  },
  {
    title: '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
    extra: '子元素垂直对齐',
    textMode: 'wrap',
    align: 'top',
  },
];
export default {
  data: {
    items5: [
      {
        thumb:
          'https://gw.alipayobjects.com/zos/rmsportal/KXDIRejMrRdKlSEcLseB.png',
        title: '固定到头部',
        brief: '描述信息',
        arrow: true,
        sticky: true,
      },
      {
        title: '标题文字不换行很长很长很长很长很长很长很长很长很长很长',
        arrow: true,
        align: 'middle',
      },
      {
        title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
        arrow: true,
        align: 'top',
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '没有箭头',
        align: 'bottom',
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '子元素垂直对齐',
        align: 'top',
      },
      {
        title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
        arrow: true,
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '没有箭头',
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '子元素垂直对齐',
        align: 'top',
      },
      {
        title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
        arrow: true,
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '没有箭头',
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '子元素垂直对齐',
        align: 'top',
      },
      {
        title: '标题文字换行很长很长很长很长很长很长很长很长很长很长',
        arrow: true,
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '没有箭头',
      },
      {
        title:
          '标题文字很长很长很长很长很长很长很长很长很长很长很长很长很长很长',
        extra: '子元素垂直对齐',
        align: 'middle',
      },
    ],
    loadMore: '',
    loadContent: [
      '马不停蹄加载更多数据中...',
      '-- 已经到底了，加不了咯 --',
    ],
    maxList: 5,
  },
  onScrollToLower() {
    this.loadMore ='load';
    const { items5 } = this;
    // 加入 maxList 用于判断“假设”数据加载完毕后的情况
    if (this.maxList > 0) {
      const newItems = items5.concat(newitems);
      const MAXList = this.maxList - 1;
      this.items5=newItems;
      this.maxList= MAXList;
    } else {
      // 数据加载完毕之后，改变 loadMore 为 over 即可
      this.loadMore = 'over';
    }
  },
};

Page({
  data: {
    loadMore: '',
    loadContent: [
      '马不停蹄加载更多数据中...',
      '-- 已经到底了，加不了咯 --',
    ],
  },
  onScrollToLower() {
    // 根据实际数据加载情况设定 loadMore 的值即可，分别为 load 和 over
    this.setData({
      loadMore: 'load',
    })
  },
})
```

### slots

| slotName | 说明 |
| ---- | ---- |
| header | 可选，列表头部 ohos结合属性slot-header="{{true}}" |
| footer | 可选，用于渲染列表尾部  ohos需要结合属性slot-footer="{{true}}" |
