## ListSecondary 列表次级信息子组件

位于列表右侧的次级信息组件，用于列表项的extra插槽。

![img](../../../../../../../gifs/list-secondary.gif)

## 属性

| 属性名        | 类型    | 默认值 | 可选值                     | 描述                                               | 最低版本 | 必填           |
| ------------- | ------- | ------ | -------------------------- | -------------------------------------------------- | -------- | -------------- |
| thumb         | String  | -      | -                          | 缩略图地址                                         | -        | -              |
| title         | String  | -      | -                          | 标题 | -        | -              |
| subtitle      | String  | -      | -                          | 副标题                                           | -        | -              |
| thumb-size     | String  | - | - | 缩略图大小，建议手动设值。<br/> 不设值时图片的高度会有一定的自适应能力，**但不能保证跟文案内容高度完全一致** | - | 有缩略图时必填 |



## Slot

list-item共有6个插槽，位置和名称如下图所示
list-secondary用于放在**extra**插槽

具体可以查看list-item组件

## 示例

### hml
```xml
<element name="am-list" src="../../../../../../../mini_ali_ui/src/main/js/components/list/index"></element>
<element name="am-list-item" src="../../../../../../../mini_ali_ui/src/main/js/components/list/list-item/index"></element>
<element name="list-secondary" src="../../../../../../../mini_ali_ui/src/main/js/components/list/list-secondary/index"></element>
<div class="new-list-container">
  <am-list slot-header="{{true}}" style="flex:0 0;">
    <am-list-item thumb="{{thumb}}"
      arrow="{{arrow}}"
      index="{{index}}"
      border-radius="{{borderRadius}}"
      upper-subtitle="{{upperSubtitle}}"
      lower-subtitle="{{lowerSubtitle}}"
      title-position="{{titlePositions[titlePosIndex]}}"
      thumb-size="{{thumbSize ? thumbSize+'px' : ''}}"
      last="{{true}}"
      slot-extra="{{true}}"
            >
        <list-secondary
          title="{{secondary.title}}"
          subtitle="{{secondary.subtitle}}"
          thumb="{{secondary.useThumb ? secondary.thumb : ''}}"
          thumbSize="{{secondary.thumbSize}}"
          slot="extra" ></list-secondary>
    </am-list-item>
  </am-list>
</div>
```