export default {
  props: {
    title: {default:'',},
    subtitle: {default:'',},
    thumb: {default:'',},
    thumbPlaceholder: {default:'',},
    thumbSize:  {default:'40px',}
  },
  onPageShow() {
    const { title = '', subtitle = '' } = this;
    const thumbPlaceholder = title.slice(0, 1) + subtitle.slice(0, 1);
    this.thumbPlaceholder = thumbPlaceholder;
  },
};
