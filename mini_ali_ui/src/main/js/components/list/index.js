import fmtUnit from '../../common/_util/fmtunit';
import getI18n from '../../common/_util/getI18n';

const i18n = getI18n().list;

export default {
    props: {
        className: {
            default: '',
        },
        loadMore: {
            default: false,
        },
        loadContent: {
            default: [
                '',
                '',
            ],
        },
        loadingSize: {
            default: fmtUnit('16px'),
        },
        loadingColor: {
            default: '',
        },
        loadingHeight: {
            default: fmtUnit('16px'),
        },
        slotHeader: {
            default: false,
        },
        slotFooter: {
            default: false,
        }
    },
    data: {
        mloadContent: [
            i18n.loadMore,
            i18n.loadOver,
        ],
    },
    onPageShow() {
        const loadTxt = (this.loadContent && this.loadContent[0]) ? this.loadContent[0] : this.mloadContent[0];
        const overTxt = (this.loadContent && this.loadContent[1]) ? this.loadContent[1] : this.mloadContent[1];
        setTimeout(() => {
            this.mloadContent = [loadTxt, overTxt];
        }, 0);
    },
    didUpdate() {
        const loadTxt = this.loadContent[0] ? this.loadContent[0] : this.mloadContent[0];
        const overTxt = this.loadContent[1] ? this.loadContent[1] : this.mloadContent[1];
        if (loadTxt !== this.mloadContent[0] || overTxt !== this.mloadContent[1]) {
            setTimeout(() => {
                this.mloadContent = [loadTxt, overTxt];
            }, 0);
        }
    },
};
