export default {
  props: {
    alphabet: {
      default:[],
    },
      disabled:{
          default:false,
      }
  },
  data: {
    current: -1,
  },
  onPageShow() {
    this._updateDataSet();
  },
  didUpdate() {
    this._updateDataSet();
  },
    _updateDataSet() {
      this.dataset = {};
      for (const key in this.props) {
        if (/data-/gi.test(key)) {
          this.dataset[key.replace(/data-/gi, '')] = this[key];
        }
      }
    },
    onItemTap(ev) {
      const disabled = this.disabled;
      if (!disabled) {
        this.$emit('onTap',
          {
            data: ev.target.dataSet,
            target: {
              dataset: this.dataset
            },
          }
        );
      }
    },
    onTouchStart(ev) {
      const { disabled } = this.disabled;
      if (!disabled) {
        this.current = ev.target.dataSet.index;
      }
    },
    onTouchEnd() {
      setTimeout(()=>{this.current = -1;},300);
      //this.current = -1;
    }
};
