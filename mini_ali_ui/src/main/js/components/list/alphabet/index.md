## Alphabet 字母检索表

字母检索表

## 预览
<img src="../../../../../../../gifs/alphabet.gif" />


## 属性介绍

| 属性名        | 类型    | 默认值 | 可选值                     | 描述                                               | 最低版本 | 必填           |
| ------------- | ------- | ------ | -------------------------- | -------------------------------------------------- | -------- | -------------- |
| alphabet      | Array   | -      | -                       | 字母表内容 | -        | -              |


## 代码示例

```xml
<element name="alphabet" src="../../../../../../../mini_ali_ui/src/main/js/components/list/alphabet/index"></element>
<element name="am-icon" src="../../../../../../../mini_ali_ui/src/main/js/components/am-icon/index"></element>
<div class="dyt-list"
	style="position: relative; height: 100%;">
	<alphabet alphabet="{{alphabet}}" @on-tap="onAlphabetClick" >
		<div slot="prefix"><am-icon size="12" type="check_"></am-icon></div>
	</alphabet>
</div>
```

```javascript
import prompt from '@system.prompt';
export default {
  data: {
    alphabet: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
  },
  onAlphabetClick(ev) {
    prompt.showToast({
      message: JSON.stringify(ev._detail.data)
    });
  },
};

```