import fmtUnit from '../../../common/_util/fmtunit';

export default {
    props: {
        size: {
            default: fmtUnit('40px'),
        },
        src: {
            default: '',
        }
    },
};
