export default{
  props: {
    fixed: {default:false},
    related: {default:true},
    capsuleSize: {default:'large'},
    shape: {default:'default'},
    agreeBtn: {default:null},
    cancelBtn: {default:null},
    capsuleMinWidth: {default:false},
    hasDesc: {default:false},
  },
  data: {
    disabled: false,
    status: 0,
    agreeBtnAttr: {},
    cancelBtnAttr: {},
  },
  onReady() {
    const { agreeBtn, cancelBtn, related } = this;
    const agreeBtnCfg = agreeBtn ? Object.assign({
      title: '',
      subtitle: '',
      type: 'primary',
      data: 1,
      checked: false,
    }, agreeBtn) : {};
    const cancelBtnCfg = cancelBtn ? Object.assign({
      title: '',
      subtitle: '',
      type: 'default',
      data: 2,
    }, cancelBtn) : {};
    if ((agreeBtnCfg.checked && related) || !related) {
      this.disabled = false;
      this.status = true;
      this.agreeBtnAttr = agreeBtnCfg;
      this.cancelBtnAttr = cancelBtnCfg;
    } else {
      this.disabled = true;
      this.status = false;
      this.agreeBtnAttr = agreeBtnCfg;
      this.cancelBtnAttr = cancelBtnCfg;
    }
  },
  onTap(e) {
    this.$emit("onSelect",e)
  },
  onChange(e) {
    const { related } = this;
    const isSeleted = e._detail.checked;
    if (related && isSeleted) {
      this.disabled = false;
      this.status = true;
    } else {
      this.disabled = true;
      this.status = false;
    }
  },
};

