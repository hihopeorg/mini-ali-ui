# Terms 协议

需用户主动操作后，才能开通或激活服务时；通常包含了用户授权协议详细说明的跳转入口。


## 截图
![terms 协议](../../../../../../gifs/terms.gif)


## 属性介绍



| 属性名          | 类型        | 默认值                                                       | 可选值     | 描述             | 最低版本 | 必填 |
| --------------- | ----------- | ------------------------------------------------------------ | ---------- | ---------------- | -------- | ---- |
| fixed           | Boolean     | false                                                        | false,true | 需要常驻页面底部 | -        | -    |
| related         | Boolean     | true                                                         | true, false | 是否需要勾选复选框 | - | - |
| agree-btn        | Object      | {"title":"", "subtitle":"", "type":"primary", "data":1, "checked":false } |            | 同意按钮配置 | - | - |
| cancel-btn       | Object      | {"title":"", "subtitle":"", "type":"default", "data":2 }         |  | 取消按钮配置 | - | - |
| capsule-size     | String      | medium | large, medium, small | 胶囊按钮大小  | - | - |
| shape           | String      | default | default, capsule | 按钮形状 | - | - |
| capsule-min-width | Boolean     | false | true, false | 是否启用胶囊按钮最小宽度 | - | - |
| has-desc         | Boolean     | false | true, false | 是否有协议相关的描述信息 | - | - |
| @on-select | EventHandle |                                                              |            | 点击按钮事件 | - | - |


```hml
<div style="flex-direction: column;position: relative;padding: 12px;background-color: #eeeeee;">
  <div>
    <terms @on-select="onSelect" related="{{c1.related}}" has-desc="{{c1.hasDesc}}" agree-btn="{{c1.agreeBtn}}" cancel-btn="{{c1.cancelBtn}}">
      <div class="text" slot="header">
        <text>
          <span class="agree">同意</span>
          <span class="linked"  @click="goWeb">《用户授权协议》</span>
        </text>
      </div>
    </terms>
    <text class="title">双按钮</text>
  </div>
  <div>
    <terms @on-select="onSelect" fixed="{{c2.fixed}}" related="{{c2.related}}" has-desc="{{c2.hasDesc}}" agree-btn="{{c2.agreeBtn}}" cancel-btn="{{c2.cancelBtn}}" shape="{{c2.shape}}" capsule-min-width="{{c2.capsuleMinWidth}}" capsule-size="{{c2.capsuleSize}}">
      <div class="text" slot="desc">
        <text>
         <span class="agree1">查看</span>
          <span class="linked1"  @click="goWeb">《ETC服务用户协议》</span>
          <span class="agree1">，授权ETC服务获取身份证、收货地址用于申请ETC，关注车主服务生活号获取审核； </span>
        </text>
      </div>
    </terms>
    <text class="title">带辅助描述</text>
  </div>
  <div>
    <terms @on-select="onSelect" fixed="{{c3.fixed}}" related="{{c3.related}}" has-desc="{{c3.hasDesc}}" agree-btn="{{c3.agreeBtn}}" cancel-btn="{{c3.cancelBtn}}">
      <div class="text" slot="header">
        <text>
          <span class="agree">同意</span>
          <span class="linked"  @click="goWeb">《用户授权协议》</span>
        </text>
      </div>
    </terms>
    <text class="title">捆绑协议已选中</text>
  </div>
  <div>
    <terms @on-select="onSelect" fixed="{{c4.fixed}}" related="{{c4.related}}" has-desc="{{c4.hasDesc}}" agree-btn="{{c4.agreeBtn}}" cancel-btn="{{c4.cancelBtn}}" shape="{{c4.shape}}" capsule-min-width="{{c4.capsuleMinWidth}}" capsule-size="{{c4.capsuleSize}}">
      <div class="text" slot="header">
        <text>
          <span class="agree">同意</span>
          <span class="linked"  @click="goWeb">《用户授权协议》</span>
        </text>
      </div>
    </terms>
    <text class="title">捆绑协议未选中</text>
  </div>
  <div>
    <terms @on-select="onSelect" fixed="{{c5.fixed}}" related="{{c5.related}}" has-desc="{{c5.hasDesc}}"  agree-btn="{{c5.agreeBtn}}" cancel-btn="{{c5.cancelBtn}}" shape="{{c5.shape}}" capsule-min-width="{{c5.capsuleMinWidth}}" capsule-size="{{c5.capsuleSize}}">
      <div class="text" slot="header">
        <text>
          <span class="agree">同意</span>
          <span class="linked"  @click="goWeb">《用户授权协议》</span>
        </text>
      </div>
    </terms>
    <text class="title">无捆绑协议</text>
  </div>
  <div class="fixBtn" style="padding-bottom:30px;">
    <terms @on-select="onSelect" fixed="{{c6.fixed}}" related="{{c6.related}}" has-desc="{{c6.hasDesc}}" agree-btn="{{c6.agreeBtn}}" cancel-btn="{{c6.cancelBtn}}" shape="{{c6.shape}}" capsule-min-width="{{c6.capsuleMinWidth}}" capsule-size="{{c6.capsuleSize}}">
      <div class="text" slot="header">
        <text>
          <span class="agree">同意</span>
          <span class="linked"  @click="goWeb">《用户授权协议》</span>
        </text>
      </div>
    </terms>
    <text class="title">吸底</text>
  </div>
</div>
```

```css
.title{
    text-align: center;
    width: 100%;
    margin: 20px 0;
}
page {
    padding: 24px  12px;
}
div{
    flex-direction: column;
}
.linked,.linked1{
    color: #1677FF;
}
.agree,.linked,.title{
    font-size: 18px;
}
.agree1,.linked1{
    font-size: 12px;
}
```

```javascript
const cfg = {
    c1: {
        related: false,
        agreeBtn: {
            title: '同意协议并开通',
            data: 'custom data 1',
        },
        cancelBtn: {
            title: '暂不开通，仅手动缴费',
            data: 'custom data 2',
        },
        hasDesc: false,
    },
    c2: {
        related: false,
        agreeBtn: {
            title: '同意协议并开通',
        },
        hasDesc: true,
    },
    c3: {
        related: true,
        agreeBtn: {
            checked: true,
            title: '提交',
        },
    },
    c4: {
        related: true,
        agreeBtn: {
            title: '提交',
        },
    },
    c5: {
        related: false,
        agreeBtn: {
            title: '同意协议并提交',
        },
    },
    c6: {
        related: true,
        fixed: true,
        agreeBtn: {
            checked: true,
            title: '提交',
            data: 'custom data 1',
        },
    },
};
import router from '@system.router';
import prompt from '@system.prompt';
export default{
    data: cfg,
    onInit() {
    },
    onSelect(e) {
        const selectedData = (e._detail.currentTarget.dataset.name || '').toString();
        prompt.showDialog({title:'Terms Btns',
        message:selectedData,
        buttons:[{text:"确定",color:"#1677ff"}]});
    },
    //没有类似navigator组件  用下面实现
    goWeb(){
        console.info("goWeb");
        router.push({uri:'xxx'})//跳转的页面
    },
};

```



