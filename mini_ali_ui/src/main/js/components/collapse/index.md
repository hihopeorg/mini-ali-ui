# Collapse 折叠面板

可以折叠 / 展开的内容区域。
* 对复杂区域进行分组和隐藏，保持页面的整洁。
* **手风琴模式**是一种特殊的折叠面板，只允许单个内容区域展开。


## 截图
![avatar](../../../../../../gifs/Collapse.gif)

## 属性介绍
Collapse 折叠面板主要是有 `<collapse>` 和 `<collapse-item>` 两部分组成，所以，属性也有所不同。

### collapse
| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| active-key | Array / String | 默认无，accordion模式下默认第一个元素 | - | 当前激活 tab 面板的 key | - | - |
| accordion | Boolean | false | - | 是否为手风琴模式 | - | - |
| collapse-key | String | - | - | 唯一标识 collapse 所对应的 collapse-item | - | - |
| change | function | (activeKeys: Array): void | - | 切换面板的回调 | - | - |

### collapse-item
| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| title-class | String | - | - | 自定义标题的 class | 暂不支持 | - |
| content-class | String | - | - | 自定义内容区域的 class | 暂不支持 | - |
| is-pen | Boolean | false | - | 面板内容是否展开 | - | - |
| show-arrow | Boolean | true | - | 是否显示箭·头 | - | - |
| item-key | String | - | - | 对应 activeKey，组件唯一标识 | - | - |
| header | String | - | - | 面板头内容 | - | - |
| collapse-key | String | - | - | 唯一标识 collapse-item 所对应的 collapse | - | - |
| disabled | Boolean | false | - | 当前面板是否可点击使用 | - | - |

## Bug & Tip

* 当页面中存在多个 collapse 组件时，collapse 所对应的collapse-item 的 collapseKey 属性为必选值并且必须相等；
* 如 `accordion` 为 `true` 时，`activeKey` 传值仅为**字符串**，如果传数组将导致取值错误

## 代码示例

```xml
<element name='collapse' src="../../../../../../../mini_ali_ui/src/main/js/components/collapse/index.hml"></element>
<element name='collapse-item' src="../../../../../../../mini_ali_ui/src/main/js/components/collapse/collapse-item/index.hml"></element>

<div style="flex-direction: column;">
  <text class="demo-title">基础用法</text>
  <collapse
    class-name="demo-collapse"
    collapse-key="collapse1"
    active-key="{{['item-11', 'item-13']}}"
    change="onChange"
  >
    <collapse-item header="标题1" item-key="item-11" collapse-key="collapse1">
      <div class="item-content">
        <block for="{{randomLine}}">
          <text>自适应高度的内容区域 共 {{$idx + 1}} 行</text>
        </block>
      </div>
    </collapse-item>
    <collapse-item header="标题2" item-key="item-12" collapse-key="collapse1">
      <div class="item-content content2">
        <text>内容区域</text>
      </div>
    </collapse-item>
    <collapse-item header="标题3" item-key="item-13" collapse-key="collapse1">
      <div class="item-content content3">
        <text>内容区域</text>
      </div>
    </collapse-item>
  </collapse>
  <text class="demo-title">手风琴模式</text>
  <collapse
    class-name="demo-collapse"
    collapse-key="collapse2"
    active-key="item-22"
    change="onChange"
    accordion="{{true}}"
  >
    <collapse-item header="标题1" item-key="item-21" collapse-key="collapse2">
      <div class="item-content">
        <block for="{{randomLine}}">
          <text>自适应高度的内容区域 共 {{$idx + 1}} 行</text>
        </block>
      </div>
    </collapse-item>
    <collapse-item header="标题2" item-key="item-22" collapse-key="collapse2">
      <div class="item-content content2">
        <text>内容区域</text>
      </div>
    </collapse-item>
    <collapse-item header="标题3" item-key="item-23" collapse-key="collapse2">
      <div class="item-content content3">
        <text>内容区域</text>
      </div>
    </collapse-item>
  </collapse>

</div>
```

```css
.item-content {
  width: 100%;
  flex-direction: column;
  padding: 12px;
  font-size: 17px;
  color: #333333;
  line-height: 24px;
}

.content1 {
  height: 150px;
}

.content2 {
  height: 50px;
}

.content3 {
  height: 100px;
}

.demo-title {
  padding: 14px 16px;
  color: #999999;
}

.demo-collapse {
  border-bottom: 1px solid #eeeeee;
}

```

```javascript
export default{
  data: {
    randomLine: [],
  },
  onInit() {
    for (let hos = 0; hos < parseInt(Math.random() * 20 + 1); hos++) {
      this.randomLine.push('HarmonyOS');
    }
  },
  onChange(e) {
    console.log('collapse change', e);
  },
};
```
