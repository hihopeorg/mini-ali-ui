export default{
  data: {
    aa:'down',
    isTest:false,
    isActive: false,
    contentHeight: 0,
    contentId: '',
    id: '',
    activeKey: [],
    current:false
  },

  props: {
    itemKey: {
      default:''
    }, // 默认随机数
    header: {
      default:''
    },
    isOpen: {
      default:false
    },
    showArrow: {
      default:true
    },
    activeClass: {
      default:''
    },
    className: {
      default:''
    },
    titleClass: {
      default:''
    },
    contentClass: {
      default:''
    },
    defaultContentHeight: {
      default:0
    },
    disabled: {
      default:false
    },
    collapseKey: {
      default:''
    },
  },
  onInit(){
    let that = this
    this.$parent()[this.collapseKey+'activeKey'].forEach(function(value){
        if(value == that.itemKey){
          that.current = true
        }
    })

    this.$parent()[this.collapseKey].push({
      item:that,
      itemKey:that.itemKey,
      status:that.current
    })
  },



  onCollapseTap(){
    let that = this
    this.$parent()[this.collapseKey].forEach(function(value){
      if(value.itemKey==that.itemKey){
        that.current=!that.current
        value.status = that.current
      }else{
        if(that.$parent()[that.collapseKey+'accordion']&&value.item.current == true){
          value.item.current = false
          value.status = false
        }
      }
    })
  }
};
