export default{
  data: {
    id: '',
    activeArr: [],
  },

  props: {
    activeKey: {
      default:[]
    },
    accordion: {
      default:false
    },
    change: {
      default:()=>{}
    },
    openAnimation: {
      default: {}
    },
    collapseKey: {
      default:''
    },
    className: {
      default:''
    },
  },

  onInit(){
    let itemArray = []
    if(this.activeKey instanceof String){
      this.activeArr.push(this.activeKey)
    }else{
      this.activeArr = this.activeArr.concat(this.activeKey)
    }

    this.$parent()[this.collapseKey]=itemArray
    this.$parent()[this.collapseKey+'accordion']=this.accordion
    this.$parent()[this.collapseKey+'activeKey']=this.activeArr
  },


};
