# VerifyCode 验证码输入。

验证码输入框。  

受控模式，使用时需要用bind-input事件来回设value。

## 预览
<img src="../../../../../../gifs/verify-code.gif">

## 属性
| 属性名           | 类型                | 默认值 | 可选值                      | 描述                                                      | 最低版本 | 必填 |
| ---------------- | ------------------- | ------ | --------------------------- | --------------------------------------------------------- | -------- | ---- |
| class-name        | String              | ''     | -                           | 自定义的class                                             |  暂不支持  | 否   |
| label            | String              | '验证码'| -                           | 自定义label文案                                           |          | 否   |
| label-cls         | String              | ''     | -                           | 自定义label的class                                        |  暂不支持  | 否   |
| input-cls         | String              | ''     | -                           | 自定义input的class                                        |  暂不支持  | 否   |
| last             | Boolean             | false  | -                           | 是否最后一行                                              |          | 否   |
| value            | String              | ''     | -                           | 输入框值                                                 |          | 否   |
| name             | String              | ''     | -                           | 组件名字，用于表单提交获取数据                            |          | 否   |
| placeholder      | String              | ''     | -                           | 占位符                                                    |          | 否   |
| placeholder-style | String              | ''     | -                           | 指定 placeholder 的样式                                   |  暂不支持  | 否   |
| placeholder-class | String              | ''     | -                           | 指定 placeholder 的样式类                                 |  暂不支持  | 否   |
| disabled         | Boolean             | false  | -                           | 是否禁用                                                  |          | 否   |
| maxlength        | Number              | 140    | -                           | 最大长度                                                  |          | 否   |
| focus            | Boolean             | false  | -                           | 获取焦点                                                  |          | 否   |
| clear            | Boolean             | true   | -                           | 是否带清除功能，仅disabled为false才生效                   |          | 否   |
| bind-input          | (e: Object) => void |        | -                           | 键盘输入时触发input事件                                   |          | 否   |
| bind-confirm        | (e: Object) => void |        | -                           | 点击键盘完成时触发                                        |          | 否   |
| bind-focus          | (e: Object) => void |        | -                           | 聚焦时触发                                                |          | 否   |
| bind-blur           | (e: Object) => void |        | -                           | 失去焦点时触发                                            |          | 否   |
| bind-clear          | () => void          |        | -                           | 点击清除icon时触发                                        |          | 否   |
| count-down | Number | 60 | - | 发送倒计时时间，单位秒 | - | 否 |
| txt-send | String | '发送验证码' | - | 发送按钮的默认文案 | | 否 |
| txt-send-again | String | '重发验证码' | - | 重发按钮的默认文案 | | 否 |
| txt-count-down | String | '秒后重试' | - | 按钮倒计时的默认文案（不包含倒计时） | | 否 |
| init-active | Boolean | false | - | 是否主动触发发送按钮 | | 否 |

## 示例

### hml
```xml
<element name="verify-code" src="../../../../../../../mini_ali_ui/src/main/js/components/verify-code/index"></element>
<div style="flex-direction: column;background-color: #f5f5f5;height: 100%;">
  <div style="margin-top: 10px;" ></div>
  <text style="padding: 0 10px;font-size: 16px;">验证码框</text>
  <div style="margin-top: 10px;" ></div>
  <verify-code data-field="verifyCode"
    bind-input="{{onInput}}"
    value="{{verifyCode}}" 
    bind-clear="{{onClear}}"
    last="{{true}}"
    count-down="{{10}}"
    init-active="{{false}}"
    bind-send="{{onSend}}"></verify-code>
</div>
```

### js
```javascript
import prompt from '@system.prompt';
export default{
  data: {
    verifyCode: '',
  },
  onSend() {
    prompt.showDialog({
      message: 'verify code sent',
      buttons : [{text:'确认', color:'#1677ff'},],
    });
  },
  onInput(e) {
    this.verifyCode = e.text;
  },
  onClear() {
    this.verifyCode = '';
  },
};
```
