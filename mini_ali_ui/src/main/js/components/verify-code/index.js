import fmtEvent from '../../common/_util/fmtevent';
import fmtUnit from '../../common/_util/fmtunit';
import getI18n from '../../common/_util/getI18n';

const i18n = getI18n().verifyCode;

export default{
  props: {
    className: {default:'',},
    labelCls: {default:'',},
    label: {default:i18n.label,},
    inputCls: {default:'',},
    last: {default:false,},
    value: {default:'',},
    name: {default:'',},
    type: {default:'text',},
    password: {default:false,},
    placeholder: {default:i18n.placeholder,},
    placeholderClass: {default:'',},
    placeholderStyle: {default:'',},
    disabled: {default:false,},
    maxlength: {default:140,},
    focus: {default:false,},
    clear: {default:true,}, // 默认有清除功能
    syncInput: {default:false,},
    controlled: {default:true,},
    enableNative: {default:false,}, // 兼容安卓input的输入bug
    countDown: {default:60,},
    isInitialActive: {default:true,},
    initActive: {default:false,}, // 是否自动触发点击发送事件
    bindInput: {default:() => {},},
    bindConfirm: {default:() => {},},
    bindFocus: {default:() => {},},
    bindBlur: {default:() => {},},
    bindClear: {default:() => {},},
    bindSend: {default:() => {},},
    txtSend: {default:i18n.sendBtn,},
    txtSendAgain: {default:i18n.sendAgainBtn,},
    txtCountDown: {default:i18n.countDown,},
  },
  data: {
    mfocus: false,
    mactionActive: true,
    mcountDown: 60,
    resent: false,
    iconSize: fmtUnit(18),
    mtimeout:0,
  },
  onPageShow() {
    this.mfocus = this.focus;
    this.mactionActive = this.isInitialActive;
    this.mcountDown = this.countDown;
    this.actedBefore = false;
    // 在组件加载的时候是否主动触发点击发送验证码事件
    if (this.initActive) {
      this.noSendCountDown();
    } else {
      this.mactionActive = !this.initActive;
    }
  },
  onPageHide() {
    clearInterval(this.mtimeout);
  },
    onBlur(e) {
      this.mfocus = false;
      const event = fmtEvent(this.props, e, this);
      if (this.bindBlur && typeof this.bindBlur === 'function') {
        this.bindBlur(event);
      }
    },
    onConfirm(e) {
      const event = fmtEvent(this.props, e, this);
      if (this.bindConfirm && typeof this.bindConfirm === 'function') {
        this.bindConfirm(event);
      }
    },
    onFocus(e) {
      this.mfocus = true;
      const event = fmtEvent(this.props, e, this);
      if (this.bindFocus && typeof this.bindFocus === 'function') {
        this.bindFocus(event);
      }
    },
    onInput(e) {
      const event = fmtEvent(this.props, e, this);
      if (this.bindInput && typeof this.bindInput === 'function') {
        this.bindInput(event);
      }
    },
    onClear(e) {
      const event = fmtEvent(this.props, e, this);
      if (this.bindClear && typeof this.bindClear === 'function') {
        this.bindClear(event);
      }
    },
    onTapSend(e) {
      const { countDown, bindSend } = this;
      if (this.mactionActive) {
        this.mactionActive = false;
        this.mtimeout = setInterval(() => {
          const subOne = this.mcountDown - 1;
          if (subOne <= 0) {
            clearInterval(this.mtimeout);
            this.mactionActive = true;
            this.resend = true;
            this.mcountDown = countDown;
            this.actedBefore = true;
          } else {
            this.mcountDown = subOne;
          }
        }, 1000);
        const event = fmtEvent(this.props, e, this);
        if (bindSend && typeof bindSend === 'function') {
          bindSend(event);
        }
      }
    },
    noSendCountDown() {
      const { countDown } = this.props;
      if (this.mactionActive) {
        this.mactionActive = false;
        this.mtimeout = setInterval(() => {
          const subOne = this.mcountDown - 1;
          if (subOne <= 0) {
            clearInterval(this.mtimeout);
            this.mactionActive = true;
            this.resend = true;
            this.mcountDown = countDown;
            this.actedBefore = true;
          } else {
            this.mcountDown = subOne;
          }
        }, 1000);
      }
    },
};
