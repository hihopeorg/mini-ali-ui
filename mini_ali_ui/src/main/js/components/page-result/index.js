import getI18n from '../../common/_util/geti18n';
const i18n = getI18n().pageResult;

//const defaultEvent = () => { };
const COUNT_DOWN_TIMES = 10;

export default{
  props: {
    className:{default:''},
    type: {default:'network'},
    local: {default:false},
    footer: {default:null},
//    onTapLeft: defaultEvent,
//    onTapRight: defaultEvent,
    isCountDown: {default:false},
    countDownText: {default:i18n.refresh},
    brief:{default:null},
  },
  data: {
    defaultTitle: {
      network: i18n.networkTitle,
      error: i18n.errorTitle,
      busy: i18n.busyTitle,
      empty: i18n.emptyTitle,
      logoff: i18n.logoffTitle,
      payment: i18n.paymentTitle,
      redpacket: i18n.redpacketTitle,
    },
    defaultBrief: {
      network: i18n.networkBrief,
      error: i18n.errorBrief,
      busy: i18n.busyBrief,
      empty: i18n.emptyBrief,
      logoff: i18n.logoffBrief,
      payment: i18n.paymentBrief,
      redpacket: i18n.redpacketBrief,
    },
    countDownTitle: '',
  },
  onReady() {
    const { countDownText, isCountDown } = this;
    if (!isCountDown) {
      return;
    }
    let countDownTimes = COUNT_DOWN_TIMES;
    this._timer = null;
    const execCountDown = () => {
      this.countDownTitle=`${countDownTimes} ${i18n.timeOut} ${countDownText}`;
      countDownTimes -= 1;
      if (countDownTimes < 0) {
        clearTimeout(this._timer);
        this.isCountDown = false;
      } else {
        this._timer = setTimeout(execCountDown, 1000);
      }
    };
    execCountDown();
  },
  onDestroy() {
    if (this._timer) {
      clearTimeout(this._timer);
      this._timer = null;
    }
  },
  onLeftButton(...args) {
    this.$emit('onTapLeft', ...args)
  },
  onRightButton(...args) {
    this.$emit('onTapRight', ...args)
  },
  getButton(e){
    //这个地方目前最多只有两个按钮  判断点的是哪一个
    if(this.footer.length==1){
      this.onLeftButton(e);
    }else{
      for(let i=0;i<this.footer.length;i++){
        if(this.footer[i].text == e._detail.target.dataset.value){
          if(i==0){
            this.onLeftButton(e);
          }else{
            this.onRightButton(e);
          }
        }
      }
    }
  },
};
