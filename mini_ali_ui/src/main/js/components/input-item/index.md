# InputItem 文本输入框。
- 输入区内容不折行，如用户输入的字数超出显示区，输入框内的文字可左右滑动
- 如无特殊情况，清空按钮在框内有内容且获得焦点时默认出现

## 截图
![avatar](../../../../../../gifs/InputItem.gif)

## 属性介绍

| 属性名           | 类型                | 默认值 | 可选值                      | 描述                                                      | 最低版本 | 必填 |
| ---------------- | ------------------- | ------ | --------------------------- | --------------------------------------------------------- | -------- | ---- |
| class-name        | String              | ''     | -                           | 自定义的class                                             | 暂不支持 | 否   |
| label-cls         | String              | ''     | -                           | 自定义label的class                                        | 暂不支持 | 否   |
| input-cls         | String              | ''     | -                           | 自定义input的class                                        | 暂不支持 | 否   |
| last             | Boolean             | false  | -                           | 是否最后一行                                              |          | 否   |
| value            | String              | ''     | -                           | 初始内容                                                  |          | 否   |
| name             | String              | ''     | -                           | 组件名字，用于表单提交获取数据                            |          | 否   |
| type             | String              | text   | text, number | input 的类型，有效值：`text`, `number`, |          | 否   |
| password         | Boolean             | false  | -                           | 是否是密码类型                                            |          | 否   |
| disabled         | Boolean             | false  | -                           | 是否禁用                                                  |          | 否   |
| maxlength        | Number              | 140    | -                           | 最大长度                                                  |          | 否   |
| focus            | Boolean             | false  | -                           | 获取焦点                                                  |          | 否   |
| clear            | Boolean             | true   | -                           | 是否带清除功能，仅disabled为false才生效                   |          | 否   |
| input          | function |        | -                           | 键盘输入时触发input事件                                   |          | 否   |
| confirm        | function |        | -                           | 点击键盘完成时触发                                        |          | 否   |
| focus-method          | function |        | -                           | 聚焦时触发                                                |          | 否   |
| blur           | function |        | -                           | 失去焦点时触发                                            |          | 否   |
| clear-method          | function          |        | -                           | 点击清除icon时触发                                        |          | 否   |
| layer | String | '' | vertical | 文本输入框是否为垂直排列，`vertical` 时为垂直排列，空值为横向排列 |  | 否 |

### type 属性值介绍
* `text`： 字符输入框
* `number`： 纯数字输入框（ 0-9 之间的数字）

**注**：`type` 的属性值影响的是真机中的键盘类型，在模拟器中并不一定会有效果。

### bug & tips
* input-item 组件的特性主要来源于 [input](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-basic-input-0000000000611673)

## slots

| slotname | 说明 |
| ---- | ---- |
| extra | 可选，用于渲染input-item项右边说明 |



## 代码示例

```xml
<element name='input-item' src="../../../../../../../mini_ali_ui/src/main/js/components/input-item/index.hml"></element>
<element name='am-icon' src="../../../../../../../mini_ali_ui/src/main/js/components/am-icon/index.hml"></element>
<div style="flex-direction: column;width: 100%;align-content: center;align-items: center;">
  <div style="margin-top: 10px;"></div>
    <input-item data-field="cardNo" 
      clear="{{ false }}"
      value="{{cardNo}}"
      class-name="dadada"
      placeholder="银行卡号" 
      input="{{ onItemInput }}"
      blur="{{ onItemBlur }}"
      confirm="{{ onItemConfirm }}"
      controlled="true"
      clear-method="{{ onClear }}">
      <text style="width: 80px;">卡号</text>
      <div slot="extra" class="extra" onclick="onExtraTap"></div>
    </input-item>
    <input-item data-field="name"
      placeholder="姓名"
      value="{{name}}" 
      clear="{{ true }}"
      input="{{ onItemInput }}"
      clear-method="{{ onClear }}">
      <text style="width: 80px;">姓名</text>
    </input-item>
    <input-item data-field="password" password="{{ true }}"
      placeholder="密码">
      <text style="width: 80px;">密码</text>
    </input-item>
    <input-item data-field="layerShow1"
      placeholder="layer 为 vertical 的排列"
      layer="vertical"
      value="{{layerShow1}}"
      clear="{{ true }}"
      input="{{ onItemInput }}"
      clear-method="{{ onClear }}">
      <text style="color: #99000000; font-size: 14px;">竖向表单</text>
      <div onclick="onExtraTap" slot="extra">
        <am-icon type="phone-book_" size="30" color="#1677ef"></am-icon>
      </div>
    </input-item>
    <input-item data-field="layerShow2"
      placeholder="layer 为 vertical 的排列"
      layer="vertical"
      value="{{layerShow2}}"
      clear="{{ true }}"
      clear-method="{{onClear}}"
      >
      <text style="color: #99000000; font-size: 14px;">竖向表单</text>
    </input-item>
    <input-item data-field="layerShow3"
      placeholder="{{layerShow3}}"
      layer="vertical"
      disabled="true"
      clear="{{ true }}"
      input="{{ onItemInput }}"
      clear-method="{{ onClear }}">
      <text style="color: #99000000; font-size: 14px;">竖向表单</text>
      <div onclick="onExtraTap" slot="extra">
        <am-icon type="phone-book_" size="30" color="#1677ef"></am-icon>
      </div>
    </input-item>
    <input-item data-field="remark" placeholder="备注" clear="{{ false }}">
    </input-item>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
  data: {
    cardNo: '1234****',
    name: '',
    layerShow1: '',
    layerShow2: '垂直输入框的布局',
    layerShow3: 'disabled 状态的 input',
  },
  onExtraTap() {
    prompt.showToast({
      message: 'extra tapped',
      duration: 2000,
      bottom: 400
    })
  },
  onItemInput(e) {},
  onItemFocus() {},
  onItemBlur() {},
  onItemConfirm() {},
  onClear(e) {
    e.target.dataSet.field = ''
  },
  onSend() {
    prompt.showToast({
      message: 'verify code sent',
      duration: 2000,
      bottom: 400
    })
  },
};

```

```css
.extra {
  background-image: url('/common/images/camera.png');
  background-size: contain;
  background-repeat: no-repeat;
  background-position: right center;
  opacity: 0.2;
  height: 30px;
  width: 30px;
}
```