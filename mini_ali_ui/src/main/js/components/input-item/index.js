import fmtEvent from '../../common/_util/fmtevent';
import fmtUnit from '../../common/_util/fmtunit';

export default{
  props: {
    className: {
      default:''
    },
    labelCls: {
      default:''
    },
    inputCls: {
      default:''
    },
    last: {
      default:false
    },
    value: {
      default:''
    },
    name: {
      default:''
    },
    type: {
      default:'text'
    },
    password: {
      default:false
    },
    placeholder: {
      default:''
    },
    placeholderClass: {
      default:''
    },
    placeholderStyle: {
      default:''
    },
    disabled: {
      default:false
    },
    maxlength: {
      default:140
    },
    focus: {
      default:false
    },
    clear: {
      default:true
    }, // 默认有清除功能
    syncInput: {
      default:false
    },
    enableNative: {
      default:false
    }, // 兼容安卓input的输入bug
    input: {
      default:()=>{}
    },
    confirm: {
      default:()=>{}
    },
    focusMethod: {
      default:()=>{}
    },
    blur: {
      default:()=>{}
    },
    clearMethod: {
      default:()=>{}
    },
    layer:  {
      default:''
    }, // 表单排列位置，当为空时默认横向排列， vertical 为竖向排列
    controlled:  {
      default:false
    },
    dataset: {
      default:{},
    }
  },
  data: {
    showClear:false,
    iconSize: fmtUnit(18),
  },
  onInit() {
    if(this.password == true){
      this.type = 'password'
      this.clear = false
    }
  },

  onBlur(e) {
    this.focus = false
    const event = fmtEvent(this.props, e,this);
    if(typeof this.blur === 'function'){
      this.blur(event);
    }
  },
  onConfirm(e) {
    const event = fmtEvent(this.props, e,this);
    if(typeof this.confirm === 'function'){
      this.confirm(event);
    }

  },
  onFocus(e) {
    this.focus = true
    const event = fmtEvent(this.props, e,this);
    if(typeof this.focusMethod === 'function'){
      this.focusMethod(event);
    }
  },
  onInput(e) {
    this.value = e.value
    const event = fmtEvent(this.props, e,this);
    if (typeof this.input === 'function') {
      this.input(event);
    }
  },
  onClear(e) {
    var event = fmtEvent(this.props,e,this);
    this.value = ''
    if (typeof this.clearMethod === 'function') {
      this.clearMethod(event);
    }
  },
};
