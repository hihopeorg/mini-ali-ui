import fmtUnit from '../../common/_util/fmtunit';

export default {
    data: {
        opaReduce: 1,
        opaAdd: 1,
    },
    props: {
        className: {
            default: ''
        },
        min: {
            default: 0
        },
        max: {
            default: 100000
        },
        disabled: {
            default: false
        },
        value: {
            default: 10
        },
        readOnly: {
            default: false
        },
        showNumber: {
            default: false
        },
        inputWidth: {
            default: fmtUnit('36')
        },
        step: {
            default: 1
        },
        controlled: {
            default: true
        },
        enableNative: undefined, // false 处理 fixed 定位后输入框内容闪动的问题
    },
    onAttached() {
        const { value, min, max } = this;
        this.value = Math.min(Math.max(min, value), max)
    },
    onPageShow(preProps) {
        const { value, min, max } = this;
        if (preProps.value !== value) {
            const newValue = Math.min(Math.max(min, value), max);
            this.value = newValue;
            this.resetFn(newValue);
        }
    },
    changeFn(ev) {
        const { min, max, disabled, step } = this;
        const evType = ev.target.dataSet.type;
        let { opaReduce, opaAdd, value } = this;
        if (!disabled) {
            if (evType === 'reduce') {
                if (value > min) {
                    opaAdd = 1;
                    value = Math.max(min, this.getCalculateValue('reduce', (+value), (+step)));
                    opaReduce = value === min ? 0.4 : 1;
                }
            } else {
                /* eslint-disable no-lonely-if */
                if (value < max) {
                    opaReduce = 1;
                    value = Math.min(this.getCalculateValue('add', (+value), (+step)), max);
                    opaAdd = value === max ? 0.4 : 1;
                }
            }
            this.value = value;
            this.opaAdd = opaAdd;
            this.opaReduce = opaReduce;
            this.$emit(value, 'click')
        }
    },
    onInput(e) {
        const { max } = this;
        const value = e.value;
        if (value >= max) {
            this.value = max;
        }
        this.resetFn(Number(value), 'input');
    },
    onBlur(event) {
        const value = event.value;
        const { max } = this;
        if (value >= max) {
            this.value = max;
        }
        this.resetFn(Number(value), 'input');
    },
    resetFn(value, mode) {
        const { max, min } = this;
        let calculatedVal = value;
        let opaAdd = 1;
        let opaReduce = 1;
        if (value >= max) {
            calculatedVal = max;
            opaAdd = 0.4;
        } else if (value <= min) {
            calculatedVal = min;
            opaReduce = 0.4;
        }
        this.value = calculatedVal;
        this.opaAdd = opaAdd;
        this.opaReduce = opaReduce;
        this.$emit("onChange", [calculatedVal, mode]);
    },
    getCalculateValue(type, arg1, arg2) {
        const numFloat = arg1.toString().split('.')[1] || '';
        const num2Float = arg2.toString().split('.')[1] || '';
        const length = Math.max(numFloat.length, num2Float.length);
        const times = 10**length;
        return type === 'reduce' ? (((+arg1) * times - (+arg2) * times) / times).toFixed(length) : (((+arg1) * times + (+arg2) * times) / times).toFixed(length);
    },
}
