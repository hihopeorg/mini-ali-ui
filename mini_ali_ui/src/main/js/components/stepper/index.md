# Stepper 步进器

用作增加或者减少当前数值。

**注意：**
* 输入最大值无提示，超过最大值时系统会自动回显数值为最大值。
* 不支持输入小数，可通过 + 和 - 改变数值大小。

## 截图
![Image text](../../../../../../gifs/Stepper.gif)

## 扫码体验

## 属性介绍
| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 |
| min | Number | 0 | - | 最小值 | - |
| max | Number | 100000 | - | 最大值 | - |
| value | Number | 10 | - | 初始值 | - |
| step | Number | 1 | - | 每次改变步数，可以为小数 | Number | - |
| disabled | Boolean | false | - | 是否禁用 | - |
| read-only | Boolean | false | - | 是否只读 | - |
| show-number | Boolean | false | - | 是否显示数值 | - |
| @on-change | EventHandle | (value: Number, mode: String) => void | - | 变化时回调函数 | - |
| input-width | String | 36px | - | 输入框的宽度 | - |

## Bug & Tip
* `read-only` 为 `true` 后，只可通过 + - 按钮来控制数字增加；
* `disabled` 为 `true` 后，步进器将不可用；
* 输入框的宽度由开发者自行设置，默认宽度为 `36px`；

## 代码示例
```xml
<element name='ali-stepper' src="../../../../../../../mini_ali_ui/src/main/js/components/stepper/index.hml"></element>
<element name='ali-list' src="../../../../../../../mini_ali_ui/src/main/js/components/list/index.hml"></element>
<element name='ali-list-item' src="../../../../../../../mini_ali_ui/src/main/js/components/list/list-item/index.hml"></element>

<div style="flex-direction : column; background-color : #F5F5F5; height : 100%;">
    <ali-list>
        <ali-list-item disabled="{{ true }}" slot-Extra="{{ true }}">
            <text>Show number value</text>
            <div slot="extra">
                <ali-stepper
                        onChange="callBackFn"
                        step="{{ 1 }}"
                        show-Number="{{ true }}"
                        read-Only="{{ false }}"
                        value="{{ value }}"
                        input-Width="100"
                        min="{{ 2 }}"
                        ></ali-stepper>
            </div>
        </ali-list-item>
        <ali-list-item disabled="{{ true }}" slot-Extra="{{ true }}">
            <text>step: 0.01</text>
            <div slot="extra">
                <ali-stepper
                        onChange="callBackFn"
                        step="{{ 0.01 }}"
                        show-Number="{{ true }}"
                        read-Only="{{ false }}"
                        value="{{ value }}"
                        input-Width="100"
                        min="{{ 2 }}"
                        ></ali-stepper>
            </div>
        </ali-list-item>
        <ali-list-item disabled="{{ true }}" slot-Extra="{{ true }}">
            <text>Do not show number value</text>
            <div slot="extra">
                <ali-stepper
                        onChange="callBackFn"
                        step="{{ 1 }}"
                        read-Only="{{ false }}"
                        value="{{ value }}"
                        input-Width="80"
                        show-Number="{{ false }}"
                        min="{{ 2 }}"
                        ></ali-stepper>
            </div>
        </ali-list-item>
        <ali-list-item disabled="{{ true }}" slot-Extra="{{ true }}">
            <text>Disabled</text>
            <div slot="extra">
                <ali-stepper
                        onChange="callBackFn"
                        show-Number="{{ true }}"
                        value="{{ 11 }}"
                        min="{{ 2 }}"
                        input-Width="90"
                        disabled="{{ true }}"
                        ></ali-stepper>
            </div>
        </ali-list-item>
        <ali-list-item disabled="{{ true }}" slot-Extra="{{ true }}">
            <text>readOnly</text>
            <div slot="extra">
                <ali-stepper
                        onChange="callBackFn"
                        show-Number="{{ true }}"
                        value="{{ 11 }}"
                        min="{{ 2 }}"
                        read-Only="{{ true }}"
                        input-Width="90"
                        ></ali-stepper>
            </div>
        </ali-list-item>
    </ali-list>
    <button onclick="modifyValue" type="text" style="text-color : black; background-color : white; border-radius: 0px;">修改setper初始值</button>
</div>
```

```javascript
export default{
  data: {
    value: 8,
  },
  callBackFn(value, mode) {
    console.log(value, mode);
  },
  modifyValue() {
    this.value = 9
  },
}
```