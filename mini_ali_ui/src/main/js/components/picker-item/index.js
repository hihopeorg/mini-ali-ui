import fmtUnit from '../../common/_util/fmtunit';
export default{
    props: {
        value:{default:''},
        slotcontent:{default:''},
        className: {default:''},
        extra: {default:[]},
        placeholder: {default:''},
//        onSelect: function onSelect() {},
        labelCls: {default:''},
        pickerCls: {default:''},
        layer: {default:''},
        // 表单排列位置，当为空时默认横向排列， vertical 为竖向排列
        iconType: {default:'right'},
    },
    data: {
        iconSize: fmtUnit(18)
    },
    onInit(){
    },
    onPickerTap(e) {
        this.$emit('onPickerTap',e);
    }
};