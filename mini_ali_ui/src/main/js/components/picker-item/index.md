## PickerItem 选择输入

选择输入。

## 截图

![picker-item 信息](../../../../../../gifs/picker-item.gif)


## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| --- | --- | --- | --- | --- | --- | --- |
| class-name | String |  |  | 自定义的class |  |  |
| label-cls | String |  |  | 自定义label的class |  |  |
| picker-cls | String |  |  | 自定义选择区域的class |  |  |
| last | Boolean | false |  | 是否最后一行 |  |  |
| value | String |  |  | picker-item 的值 |  |  |
| valueed | String |  |  | picker-item 获取的值 |  |  |
| placeholder | String |  |  | 占位符 |  |  |
| @on-picker-tap | (e: Object) => void |  |  | 点击pickeritem时触发 |  |  |
| layer | String | '' | vertical | 文本输入框是否为垂直排列，`vertical` 时为垂直排列，空值为横向排列 |  | 否 |
| icon-type | String | 'right' | - | 更改 picker-item 的 icon 类型，参考 am-icon 的 type 值 |  | - |

### slots

| slot | 说明 |
| ---- | ---- |
| extra | 可选，用于渲染picker-item项右边说明 |

## 代码示例


```hml
<div class="container">
  <am-list class="listed">
    <div style="width: 100%;">
      <picker-item
              slotcontent="发卡银行"
              data-field="bank"
              placeholder="选择发卡银行"
              extra="{{bank}}"
              value="发卡银行"
              @on-picker-tap="onPickerTap"
              >
<!--        发卡银行-->
      </picker-item>
    </div>
    <div style="width: 100%;">
      <picker-item
              slotcontent="发卡银行"
              data-field="bank"
              placeholder="选择发卡银行"
              extra="{{bank}}"
              value="发卡银行"
              @on-picker-tap="onPickerTap"
              layer="vertical"
              >
<!--        发卡银行-->
      </picker-item>
    </div>
  </am-list>
  <div class="selected" if="{{selectds}}">
    <div class="top" @click="forbid">
    </div>
    <div class="lists" style="">
      <am-list class="todo-wrapper">
        <list-item class="todo-title">
          <text class="todo-title-text">选择发卡银行</text>
        </list-item>
        <list-item class="todo-itemed" for="{{item in banks}}" @click="getItem(item.bank)">
          <text class="todo-title1-text">{{item.bank}}</text>
        </list-item>
        <list-item class="todo-bottom" @click="forbid">
          <text class="todo-bottom-text">取消</text>
        </list-item>
      </am-list>
    </div>
  </div>
</div>
```

```javascript
export default{
  data: {
    bank: [],
    selectds:false,
    toggle:"隐藏",
    banks:[{index:0,bank:'网商银行'},{index:1,bank:'建设银行'},{index:0,bank:'工商银行'},{index:0,bank:'浦发银行'}],
  },
  onPickerTap() {
    this.selectds = true;
  },
  forbid(){
    this.selectds = false;
  },
  getItem(e){
    this.bank = [e];
    this.selectds = false;
  }
};
```