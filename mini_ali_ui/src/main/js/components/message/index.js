import fmtEvent from '../../common/_util/fmtevent';

//const SUPPORT_COMPONENT2 = my.canIUse('component2');
const SUPPORT_COMPONENT2 = true;
const TYPE_MAP = {
  success: 'check_',
  fail: 'close_',
  cancel: 'close_',
  info: 'help_',
  warn: 'warn_',
  waiting: 'time-5_',
};

export default{
  props: {
    className: {default:''},
    title: {default:''},
    type:{default:'success'},
    subTitle:{default:''},
    mainButton:{default:''},
    subButton:{default:''},
    // message 的 icon 类型（颜色）
    iconColor:{default:'#1677FF'},
    // message 的 icon 图标样式
    iconType:{default:'success'},
  },
  data: {
//    // message 的 icon 图标样式
//    iconType: 'check_',
//    // message 的 icon 类型（颜色）
//    iconType_: 'success',
  },
  onInit() {
    this.iconType = this.type;

  },
  onReady() {
    if (!SUPPORT_COMPONENT2) {
      this.iconType = this.type;
    }
  },
  onShow(prevProps) {
    if (!SUPPORT_COMPONENT2 && this.type !== prevProps.type) {
      this.iconType = this.type;
    }
  },
  deriveDataFromProps(nextProps) {
    if (this.type !== nextProps.type) {
      this.setType(nextProps.type);
    }
  },
  tapMain(e) {
    const event = fmtEvent(this.props, {target:{dataSet:e._detail.dataset}}, this);
    this.$emit("onTapMain",event);
  },
  tapSub(e) {
    const event = fmtEvent(this.props, {target:{dataSet:e._detail.dataset}},this);
    this.$emit("onTapSub",event);
  },
  setType(type) {
    const realType = TYPE_MAP[type] || 'success';
    // 根据 props 中的 type 值选择 icon 的图标以及颜色
    this.iconColor = realType;
    this.iconType = type;
  },
};
