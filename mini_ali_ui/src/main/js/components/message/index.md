## Message 结果页

结果页。

## 截图
![message 信息](../../../../../../gifs/message.gif)

## 属性介绍

| 属性名 |  类型 | 默认值 | 可选项 | 必选 | 描述 |
| ----- | ----- | ----- | ---- | ---- | ---- |
| class-name | String | - | 暂不支持 | - | 自定义的class |
| icon-type | String | success|success、fail、info、warn、waiting | - | 五种状态类型，默认为success |
| title | String  | -|- | yes | 主标题 |
| sub-title | String  | - | - | - | 副标题 |
| main-button | Object<buttonText, disabled> | - | - | - | 主按钮的文本和可用性相关 |
| sub-button | Object<buttonText, disabled>  | - | - | - | 副按钮的文本和可用性相关 |
| @on-tap-main | () => {}  | - | - | - | 主按钮的点击函数 |
| @on-tap-sub | () => {}  | - | - | - | 副按钮的点击函数 |

### slots
| slot |  说明 |
| ----- | ---- |
| tips | 可根据需要插入内容，如拨打客服电话等。当 `subTitle` 为空时才有效。 |

## 示例

```

```hml
<div class="container">
  <message
    class="operating"
    icon-color="{{iconColor}}"
    title="{{title}}"
    sub-title="{{subTitle}}"
    icon-type="{{type}}"
    main-button="{{mainButton}}"
    sub-button="{{subButton}}"
    @on-tap-main="goBack">
    <div slot="tips"><text>这里是通过</text> <text style="color: red;">slot</text> <text>插槽加入的内容，加入更多自定义内容。</text></div>
  </message>
  <div class="controls">
    <div class="radio-group">
      <div class="radio" for="{{item in items}}">
        <input type="radio" checked='{{item.checked}}' name="radioSample" value="{{item.value}}" onchange="radioChange(item.value)"></input>
        <text class="radio-text">{{item.value}}</text>
      </div>
    </div>
    <div style="margin:16px 16px;font-weight: bold;"><text>主标题</text></div>
    <input value="{{title}}" onchange="titleChange" style="width:200px;margin-left:16px;font-size: 18px;border: 1px solid #eee;border-radius: 0px;background-color: white;"/>
    <div style="margin:16px 16px;font-weight: bold;"><text>副标题</text></div>
    <textarea maxlength="140" showcounter="true" value="{{subTitle}}" onchange="subtitleChange" extend="true" style="margin:0px 16px;font-size: 18px;border: 1px solid #eee;background-color: white;" />
    <div class="showBtn">
      <input type="checkbox" onchange='onChange'></input>
      <text class="btnLabel">显示按钮</text>
    </div>
  </div>
<!--  最后面添加高度  如果设成100% 拉起键盘会挤压-->
  <div style="height: 160px;">
  </div>
</div>

```

```javascript
import router from '@system.router';
export default{
  data: {
    title: '操作成功',
    subTitle: '内容详情可折行，建议不超过两内容。也可以通过 slot="tips" 插入更具有功能性的提示。',
    type: 'success',
    iconColor:'#1677FF',
    items: [
      { name: 'success', value: 'success', checked: true ,iconColor:'#1677FF'},
      { name: 'fail', value: 'fail' ,iconColor:'#FF3B30'},
      { name: 'info', value: 'info' ,iconColor:'#1677FF'},
      { name: 'warned', value: 'warned' ,iconColor:'#FF8F1F'},
      { name: 'waiting', value: 'waiting' ,iconColor:'#00B578'},
    ],
    mainButton:null,
    subButton:null,
  },
  onInit() {
  },
  goBack() {
    router.back();
  },
  radioChange(a,e) {
    let targetValue = JSON.stringify(e).split(",")[1].split(":")[1].split('"')[1];
    this.type=targetValue;
    let targetIcon = this.items.filter((item)=>{
      return item.value == targetValue
    });
    this.iconColor = targetIcon[0].iconColor;
  },
  titleChange(e) {
    this.title= e.value;
  },
  subtitleChange(e) {
    this.subTitle = e.text;
  },
  onChange(e) {
    if (JSON.stringify(e).split(",")[0].split(":")[1] == "true") {
      this.mainButton= {buttonText: '主要操作' };
      this.subButton= {buttonText: '辅助操作' };
    } else {
      this.mainButton= null;
      this.subButton= null;
    }
  },
};

```