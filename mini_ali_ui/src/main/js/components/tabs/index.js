import fmtUnit from '../../common/_util/fmtunit';
import display from '@ohos.display';

export default {
  props: {
    className: {default:'',},
    // tabbar激活的 tab 样式 class
    activeCls: {default:'',},
    // tabbar的自定义样式class
    tabBarCls: {default:'',},
    // 选中选项卡下划线颜色 & 胶囊选中背景色
    tabBarUnderlineColor: {default:'#1677FF',},
    // 选中选项卡字体颜色
    tabBarActiveTextColor: {default:'#1677FF',},
    // 胶囊选中选项卡字体颜色
    capsuleTabBarActiveTextColor: {default:'#ffffff',},
    // 未选中选项卡字体颜色
    tabBarInactiveTextColor: {default:'#333333',},
    // 未选中描述字体颜色
    tabBarSubTextColor: {default:'#999999',},
    // 选中描述字体颜色
    tabBarActiveSubTextColor: {default:'#ffffff',},
    // 选项卡背景颜色
    tabBarBackgroundColor: {default:'#ffffff',},
    // 胶囊选项卡未选中的背景色
    capsuleTabBarBackgroundColor: {default:'#e5e5e5',},
    showPlus: {default:false,},
    // tabs 内容区是否可拖动，true 可拖动，内容区固定高度 false 不可拖动，内容区自适应高度
    swipeable: {default:true,},
    // 当前激活tab id
    activeTab: {default:0,},
    animation: {default:true,},
    duration: {default:500,},
    // 是否为胶囊形式 tab
    capsule: {default:false,},
    // 是否有副标题
    hasSubTitle: {default:false,},
    elevator: {default:false,},
    floorNumber: {default:[],},
    elevatorTop:{default:'0px',},
    showBadge: {default:false,},
    // 选中选项卡下划线宽度
    tabBarUnderlineWidth: {default:'',},
    // 选中选项卡下划线高度
    tabBarUnderlineHeight: {default:'',},
    // 电梯组件 tab-content 距离顶部高度
    elevatorContentTop: {default:0,},
    // 通过接收外部传值，动态控制 tab-content 在 swiper 下的高度
    tabContentHeight: {default:'',},
    // plus icon 类型更多的支持
    plusIcon: {default:'add',},
    plusIconSize: {default:16,},
    plusIconColor: {default:'#000',},
    // plus icon 使用 image 的方式
    plusImg: {default:'',},
    plusImgWidth: {default:'',},
    plusImgHeight: {default:'',},
    // tab-bar 是否滚动定位在顶部的判断
    stickyBar: {default:false,},
    bindChange: {default:()=>{}},
    bindTabClick: {default:()=>{},},
    bindPlusClick: {default:()=>{},},
    bindTabFirstShow: {default:()=>{},},
    tabsName: {default:''},
    tabs:{default:{}},
  },
  data: {
    windowWidth: 0,
    tabWidth: 0.25,
    autoplay: false,
    manimation: false,
    showLeftShadow: false,
    showRightShadow: true,
    //version: my.SDKVersion,
    viewScrollLeft: 0,
    tabViewNum: 0,
    hideRightShadow: false,
    boxWidth: 0,
    elWidth: 0,
    tabFontSize15: fmtUnit('15px'),
    tabFontSize13: fmtUnit('13px'),
    mshowPlus: false,
    tabsWidthArr: [],
    amTabsContentWrapHeight: '100%',
  },
  onInit() {
    this.$app.tabContents = this.$app.tabContents || {};
    this.$app.tabContentsHeight = this.$app.tabContentsHeight || {};
    this.$watch('tabs','onTabsChange');
    this.$watch('elevator','onElevatorChange');
    this.$watch('showPlus','onShowPlusChange');
    this.$watch('activeTab','onActiveTabChange');
    this.$watch('tabsName','onTabsNameChange');
    this.$watch('swipeable','onSwipeableChange');
    this.$watch('hasSubTitle','onHasSubTitleChange');
    this.$watch('capsule','onCapsuleChange');
    this.$watch('tabBarCls','onTabBarClsChange');//
    this.$watch('mshowPlus','onMshowPlusChange');
    this.$watch('elevatorHeight','onElevatorHeightChange');
    this.$watch('floorNumber','onFloorNumberChange');
    this.$watch('tabsWidthArr','onTabsWidthArrChange');
    this.$watch('elWidth','onElWidthChange');
    this.$watch('boxWidth','onBoxWidthChange');
    this.$watch('tabViewNum','onTabViewNumChange');
    this.$watch('viewScrollLeft','onViewScrollLeftChange');
  },
  async onPageShow() {
    const self = this;
    const { tabs, animation, hasSubTitle, elevator, showPlus } = this;
    if (tabs.length !== 0 || !tabs) {
      this.mshowPlus = showPlus;
      this.setWindowWidth();

      if (hasSubTitle) {
        this.capsule = true;
      }
      this.tabWidth = tabs.length > 3 ? 0.25 : 1 / tabs.length;
      this.manimation = animation;
      this.autoplay = true;
      if (elevator) {
        this.swipeable = false;
        // 记录电梯组件总高度，并写入 data
        var tabsElevatorContent = this.$element('am-tabs-elevator-content');
        if (tabsElevatorContent) {
          this.elevatorHeight = tabsElevatorContent.getBoundingClientRect().height;
        }
        // 获取电梯组件每个 pane 的 top 值
        //const floorNumber = await this.getElevatorHeight(tabs);
        // 滚动到指定是初始位置
//        my.pageScrollTo({
//          scrollTop: Math.ceil(floorNumber[this.activeTab]),
//          duration: 1,
//        });
      }

      // 初始状态下，如果 activeTab 数值较大，会将后面的 tab 前移
      let boxWidth = 0;
      let elWidth = 0;
      let elLeft = 0;
      var tabsItem = this.$element(`tabs-item-${this.tabsName}-${this.activeTab}`);
      if (tabsItem) {
        elWidth = tabsItem.getBoundingClientRect().width;
        elLeft = tabsItem.getBoundingClientRect().left;
        this.elWidth = elWidth;
        this.elLeft = elLeft;
      }
      var tabsBarContent = this.$element(`am-tabs-bar-${this.tabsName}-content`);
      if (tabsBarContent) {
        boxWidth = tabsBarContent.getBoundingClientRect().width;
        this.boxWidth = boxWidth;
        setTimeout(() => {
          self.viewScrollLeft = Math.floor(this.elWidth + this.elLeft - this.boxWidth);
        }, 300);
      }
        for (var key in this.$app.tabContents) {
          this.$app.tabContentsHeight[key] = this.$app.tabContents[key]?.getBoundingClientRect().height;
        }
        var amTabsContentWrap = this.$element('am-tabs-content-wrap');
        this.amTabsContentWrapHeight = this.$app.tabContentsHeight['tab-content-' + this.activeTab];
        amTabsContentWrap.scrollTo({
          position: amTabsContentWrap.getBoundingClientRect().width * this.activeTab,
          duration: 0,
        });
    }
  },
  onViewScrollLeftChange(newV) {
    var amTabsBarContent = this.$element('am-tabs-bar-' + (this.tabsName?this.tabsName+'-':'')+'content');
    amTabsBarContent.scrollTo({
      position: newV,
      duration: this.manimation ? 300 : 0,
    });
  },
  onTabsChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {tabs: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onElevatorChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {elevator: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onShowPlusChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {showPlus: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onActiveTabChange(newV, oldV) {
    console.info("onActiveTabChange newV = " + newV + ", type = " + typeof newV);
     var prevProps = Object.assign({}, this, {activeTab: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onTabsNameChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {tabsName: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onSwipeableChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {swipeable: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onHasSubTitleChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {hasSubTitle: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onCapsuleChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {capsule: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onTabBarClsChange(newV, oldV) {
     var prevProps = Object.assign({}, this, {tabBarCls: oldV});
     var prevData = Object.assign({}, this);
     this.didUpdate(prevProps, prevData);
  },
  onMshowPlusChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {mshowPlus : oldV});
     this.didUpdate(prevProps, prevData);
  },
  onElevatorHeightChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {elevatorHeight : oldV});
     this.didUpdate(prevProps, prevData);
  },
  onFloorNumberChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {floorNumber : oldV});
     this.didUpdate(prevProps, prevData);
  },
  onTabsWidthArrChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {tabsWidthArr : oldV});
     this.didUpdate(prevProps, prevData);
  },
  onElWidthChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {elWidth : oldV});
     this.didUpdate(prevProps, prevData);
  },
  onBoxWidthChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {boxWidth : oldV});
     this.didUpdate(prevProps, prevData);
  },
  onTabViewNumChange(newV, oldV) {
     var prevProps = Object.assign({}, this);
     var prevData = Object.assign({}, this, {tabViewNum : oldV});
     this.didUpdate(prevProps, prevData);
  },
  didUpdate(prevProps, prevData) {
    const { tabs, elevator, showPlus, activeTab: currentActiveTab, tabsName, swipeable } = this;
    this.mshowPlus = showPlus;
    if (prevProps.tabs.length !== tabs.length) {
      this.tabWidth = tabs.length > 3 ? 0.25 : 1 / tabs.length;
    }
    if (elevator) {
      // 当 didUpdate 时判断电梯组件总高度是否发生变化
      var amTabsElevatorContent = this.$element('am-tabs-elevator-content');
      if (amTabsElevatorContent) {
        var height = amTabsElevatorContent.getBoundingClientRect().height;
        if (height !== this.elevatorHeight) {
//          my.pageScrollTo({
//              scrollTop: 0,
//              success: () => {
//                this.setData({
//                  elevatorHeight: ret[0].height,
//                });
//                // 总高度变化后，重新获取电梯组件每个 panel 的 top 值
//                this.getElevatorHeight(tabs);
//              },
//            });
        }
      }
      this.$parent().floorNumber = this.floorNumber;
      if (this.$parent().getFloorNumber >= 0) {
        this.tabViewNum = this.$parent().getFloorNumber;
        this.prevTabViewNum = prevData.tabViewNum;
      }

      if (currentActiveTab !== prevProps.activeTab) {
        this.tabViewNum=  currentActiveTab;
        this.prevTabViewNum = prevData.tabViewNum;
//        my.pageScrollTo({
//          scrollTop: Math.ceil(this.floorNumber[currentActiveTab]),
//          duration: 1,
//        });
      }
    } else if (currentActiveTab !== prevProps.activeTab) {
      var contentScrollerHeight = this.$app.tabContentsHeight['tab-content-'+currentActiveTab];
      var amTabsContentWrap = this.$element('am-tabs-content-wrap');
      this.amTabsContentWrapHeight = contentScrollerHeight;
      amTabsContentWrap.scrollTo({
        position: amTabsContentWrap.getBoundingClientRect().width * this.activeTab,
        duration: this.duration,
      });
      let boxWidth = 0;
      let elWidth = 0;
      let elLeft = 0;
      var tabsWidthArr = new Array(tabs.length);
      for (var i = 0; i< tabs.length; i++) {
        var element = this.$element(`tabs-item-${tabsName?tabsName+'-':''}${i}`);
        tabsWidthArr[i] = element.getBoundingClientRect().width;
      }
      this.tabsWidthArr = tabsWidthArr;

      var currentTabsItem = this.$element(`tabs-item-${tabsName}-${currentActiveTab}`);
      if (currentTabsItem) {
        elWidth = currentTabsItem.getBoundingClientRect().width;
        elLeft = currentTabsItem.getBoundingClientRect().left;
        this.elWidth = elWidth;
        this.elLeft = elLeft;
      }

      var tabsBarContent = this.$element(`am-tabs-bar-${tabsName}-content`);
      if (tabsBarContent) {
        boxWidth = tabsBarContent.getBoundingClientRect().width;
        this.boxWidth = boxWidth;

        // mock el.offsetLeft
        const elOffeseLeft = this.tabsWidthArr.reduce((prev, cur, index) => {
          if (index < this.activeTab) {
          // eslint-disable-next-line no-param-reassign
            prev += cur;
          }
          return prev;
        }, 0);
        if (this.elWidth > this.boxWidth / 2) {
          setTimeout(() => {
            this.viewScrollLeft = elOffeseLeft - 40;
          }, 300);
        } else {
          setTimeout(() => {
            this.viewScrollLeft = swipeable ? elOffeseLeft : elOffeseLeft - Math.floor(this.boxWidth / 2);
          }, 300);
        }
      }
    }
  },
    setWindowWidth() {
      display.getDefaultDisplay((err, data) => {
        if (typeof data.width === 'string' || typeof data.scaledDensity === 'string') {
          this.windowWidth = 360;
        } else {
          this.windowWidth = Math.round(data.width / data.scaledDensity / 20) * 20;
        }
      });
    },
    getElevatorHeight(tabs) {
      return new Promise((resolve) => {
        for (let i = 0; i < tabs.length; i++) {
          var tabsElevatorPane = this.$app.tabContents[`am-tabs-elevator-pane-${i}`];
          var tabsBarSticky = this.$element("am-tabs-bar-sticky");
          if (tabsElevatorPane && tabsBarSticky) {
            const { elevatorTop, elevatorContentTop } = this;
            let tabContentDistance = 0;
            if (elevatorTop.match(/\d+px/)) {
              tabContentDistance = parseInt(elevatorTop, 10);
            } else {
              tabContentDistance = parseInt(elevatorContentTop, 10);
            }
            var floorNumberI = tabsElevatorPane.getBoundingClientRect().top - tabsBarSticky.getBoundingClientRect().height - tabContentDistance;
            this.floorNumber.splice(i, 1, floorNumberI);
            if (i === tabs.length - 1) {
              resolve(this.floorNumber);
            }
          }
        }
        setTimeout(() => {
          this.$parent().floorNumber = this.floorNumber;
        }, 100);
      });
    },
    onTouchEnd(e) {
      const { tabsname } = e.target.dataSet;
      var amTabsContentWrap = this.$element('am-tabs-content-wrap');
      var scrollOffset = amTabsContentWrap.getScrollOffset().x;
      var amTabsContentWrapWidth = amTabsContentWrap.getBoundingClientRect().width;
      var current = Math.round(scrollOffset / amTabsContentWrapWidth);
      if (this.activeTab == current && (scrollOffset % amTabsContentWrapWidth !== 0)) {
              amTabsContentWrap.scrollTo({
                position: amTabsContentWrap.getBoundingClientRect().width * this.activeTab,
                duration: this.duration,
              });
      }
      this.activeTab = current;
      if (this.bindChange) {
        this.bindChange({ index: current, tabsname });
      }
    },
    handleTabClick(e) {
      const { index, tabsname } = e.target.dataSet;/*floor*/
      if (this.bindTabClick && !this.elevator) {
        this.bindTabClick({ index, tabsname });
      }
      if (this.bindTabClick && this.elevator) {
        this.tabViewNum = this.prevTabViewNum;
        setTimeout(() => {
          this.bindTabClick({ index, tabsname });
        }, 300);
//        my.pageScrollTo({
//          scrollTop: Math.ceil(floor),
//          duration: 1,
//        });// todo
      }
    },
    handlePlusClick() {
      if (this.bindPlusClick) {
        this.bindPlusClick();
      }
    },
  onShowLeftShadow(e) {
      const { scrollLeft, scrollWidth } = e.detail;
      // 判断是否隐藏左边的阴影
      if (scrollLeft > 0) {
        this.showLeftShadow = true;
      } else {
        this.showLeftShadow = false;
      }
      // 判断是否隐藏右边的阴影
      if (scrollLeft + this.boxWidth >= scrollWidth - 8) {
        this.showRightShadow = false;
      } else {
        this.showRightShadow = true;
      }
    },
    onTabFirstShow(e) {
      // SDKversion 最低要求 1.9.4
      const { index, tabsname } = e.target.dataSet;
      if (this.bindTabFirstShow) {
        this.bindTabFirstShow({ index, tabsname });
      }
    },
}
