export default {
  props: {
    tabId: {default:'',} ,
    activeTab: {default:'',},
    elevator: {default:false,},
  },
  onAttached() {
    this.$app.tabContents['tab-content-'+this.tabId] = this.$element('tab-content-'+this.tabId);
  },
  onDetached() {
    delete this.$app.tabContents['tab-content-'+this.tabId];
  },
};
