# Tabs 横向选项卡

## 截图
![tabs](../../../../../../gifs/tabs.gif)

## 属性介绍
tabs 横向选项卡主要是由 `<tabs>` 和 `<tab-content>` 两个标签组成，包含的类型较多，可通过 `<tabs>` 的属性进行配置。

### tabs

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| tabs | Array | - | - | tab数据，其中包括选项标题 `title`，以及副标题（描述）文案 `subTitle`，以及胶囊形式 tab 中的数字 `number`，如需要以 badge 方式展示数字，添加 `showBadge` 并设置为 true 即可 | - | true |
| active-tab | Number | 0 | - | 当前激活的 tab 索引 | - | true |
| active-cls | String | - | - | tabbar激活的 tab 样式 class | 暂不支持 | - |
| tab-bar-cls | String | - | - | tabbar的自定义样式class | 暂不支持 | - |
| tab-bar-underline-color | String | #1677FF | - | 选中选项卡下划线颜色 | - | - |
| tab-bar-active-text-color | String | #1677FF | - | 选中选项卡字体颜色 | - | - |
| capsule-tab-bar-active-text-color | String | #ffffff | - | 胶囊选中选项卡字体颜色 | - | - |
| capsule-tab-bar-background-color | String | #e5e5e5 | - | 胶囊未选中的背景色 | - | - |
| tab-bar-inactive-text-color | String | #333333 | - | 未选中选项卡字体颜色 | - | - |
| tab-bar-sub-text-color | String | #999999 | - | 未选中描述字体颜色 | - | - |
| tab-bar-active-sub-text-color | String | #ffffff | - | 选中描述字体颜色 | - | - |
| tab-bar-background-color | String | #ffffff | - | 选项卡背景颜色 | - | - |
| show-plus | Boolean | false | - | 是否显示 + icon | - | - |
| swipeable | Boolean | true | - | tabs 内容区是否可拖动 | - | - |
| animation | Boolean | true | - | 选项卡切换时滑动动画 | - | - |
| duration | Number | 500 | - | tabs 内容区切换动画时长 | - | - |
| capsule | Boolean | false | - | 是否为胶囊 tab | - | - |
| has-sub-title | Boolean | false | - | 是否有副标题（描述）内容 | - | - |
| elevator | Boolean | false | - | 是否电梯组件 | 暂不支持 | - |
| elevator-top | String | 0px | - | 电梯组件中 tab 置顶时的位置控制 | 暂不支持 | - |
| elevator-content-top | Number | 0 | - | 电梯组件中 tab-content 距离顶部的位置 | 暂不支持 | - |
| bind-plus-click | EventHandle | () => {} | - | + icon 被点击时的回调 | - | - |
| bind-tab-click | EventHandle | (index: Number, tabsName: String) => void | - | tab 被点击的回调 | - | - |
| bind-change | EventHandle | (index: Number, tabsName: String) => void | - | tab 变化时触发 | - | - |
| tabs-name | String | - | - | tab 选项卡的名字，与 `activeTab` 的 key 值相同 | - | true |
| tab-bar-underline-width | String | 100% | - | 设置 tab 选项卡选中态的下划线宽度 | - | - |
| tab-bar-underline-height | String | 2px | - | 设置 tab 选项卡选中态的下划线高度 | - | - |
| bind-tab-first-show | EventHandle | (index: Number, tabsName: String) => {} | - | tab 选项卡首次出现时的回调 | - | - |
| tabContentHeight | String | '' | - | 当 `swipeable` 为 `true` 时，可通过该属性值重设高度强制让 swiper 组件支持“自适应”高度的行为 | - | - |
| plus-icon | String | add | icon 类型 | 改变 icon 类型 | - | - |
| plus-icon-size | Number | 16 | - | 改变 icon 大小 | - | - |
| plus-icon-color | String | '' | - | 改变 icon 颜色 | - | - |
| plus-img | String | '' | - | 使用图片替换 icon | - | - |
| plus-img-width | String | '' | - | 设置替换 icon 后的图片宽度 | - | - |
| plus-img-height | String | '' | - | 设置替换 icon 后的图片高度 | - | - |
| sticky-bar | Boolean | false | - | tabBar 是否在页面滚动的时候定位在顶部的某个位置，可结合 `elevatorTop` 设置距离顶部的位置 | 暂不支持 | - |

### tab-content

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| index | String | - | - | 列表项的唯一索引 | - | - |
| tab-id | Number | {{index}} | - | tab 内容序列索引 | - | - |
| active-tab | Number | {{activeTab}} | - | 选项卡当前激活序列索引 | - | - |
| elevator | Boolean | false | - | 电梯组件时需要指定 | 暂不支持 | - |

## 代码示例

```xml
<element name="am-tabs" src="../../../../../../../mini_ali_ui/src/main/js/components/tabs/index"></element>
<element name="am-tab-content" src="../../../../../../../mini_ali_ui/src/main/js/components/tabs/tab-content/index"></element>
<div style="flex-direction: column;">
  <div style="padding: 12px;">

    <am-tabs tabs="{{tabs}}" tabs-name="activeTab" bind-tab-click="{{handleTabClick}}" bind-change="{{handleTabChange}}" bind-plus-click="{{handlePlusClick}}" active-tab="{{activeTab}}" show-plus="{{hasPlus}}" swipeable="{{isSwipeable}}" capsule="{{typeCapsule}}" has-sub-title="{{typeHasSubTitle}}" tab-bar-underline-width="20px" tab-content-height="{{activeTab === 0 ? '130px' : activeTab === 2 ? '200px' : '50%'}}" sticky-bar="{{true}}">
      <block for="{{tabs}}" if="{{!isSwipeable}}">
        <am-tab-content key="{{$idx}}" tab-id="{{$idx}}" active-tab="{{activeTab}}" if="{{$idx === 0}}">
          <text class="tab-content" style="height: 130px;">高度为 130px {{$item.title}}</text>
        </am-tab-content>
        <am-tab-content key="{{$idx}}" tab-id="{{$idx}}" active-tab="{{activeTab}}" elif="{{$idx === 2}}">
          <text class="tab-content" style="height: 200px;">改变 tab-content 高度为 200px {{$item.title}}</text>
        </am-tab-content>
        <am-tab-content key="{{$idx}}" tab-id="{{$idx}}" active-tab="{{activeTab}}"
                        else>
          <text class="tab-content" style="height: 350px;">content of {{$item.title}}</text>
        </am-tab-content>
      </block>

      <block for="{{tabs}}" if="{{isSwipeable}}">
        <am-tab-content key="{{$idx}}" tab-id="{{$idx}}" active-tab="{{activeTab}}" if="{{$idx === 0}}">
          <text class="tab-content" style="height: 130px;">高度为 130px {{$item.title}}</text>
        </am-tab-content>
        <am-tab-content key="{{$idx}}" tab-id="{{$idx}}" active-tab="{{activeTab}}" elif="{{$idx === 2}}">
          <text class="tab-content" style="height: 200px;">改变 tab-content 高度为 200px {{$item.title}}</text>
        </am-tab-content>
        <am-tab-content key="{{$idx}}" tab-id="{{$idx}}" active-tab="{{activeTab}}"
                        else>
          <text class="tab-content">content of {{$item.title}}</text>
        </am-tab-content>
      </block>
    </am-tabs>
  </div>

  <text class="demo-title">activeTab: {{activeTab}}</text>
  <div class="radio-group">
    <block for="{{ tabs }}">
      <div class="radio-container">
        <input class="radio-input" type="radio" name="tabs" value="{{ $idx }}" onchange="activeTabChange" ></input>
        <text class="radio-text" class="radio-text">{{ $item.title }}</text>
      </div>
    </block>
  </div>
  <text class="demo-title">tab 类型:</text>
  <div class="radio-group">
    <block for="{{ type }}">
      <div class="radio-container">
        <input class="radio-input" type="radio" name="type" value="{{ $item.name }}"
               checked="{{ $item.checked }}" onchange="typeChange"></input>
        <text class="radio-text">{{ $item.value }}</text>
      </div>
    </block>
  </div>

  <text class="demo-title">swipeable:</text>
  <div class="radio-group">
    <block for="{{ swipeable }}">
      <div class="radio-container">
        <input class="radio-input" type="radio" name="swipeable"
               value="{{ $item.name }}" checked="{{ $item.checked }}" onchange="swipeableChange"></input>
        <text class="radio-text">{{ $item.value }}</text>
      </div>
    </block>
  </div>

  <text class="demo-title">是否有 ➕icon:</text>
  <div class="radio-group">
    <block for="{{ plus }}">
      <div class="radio-container">
        <input class="radio-input" type="radio" name="plus" value="{{ $item.name }}"
               checked="{{ $item.checked }}" onchange="plusChange"></input>
        <text class="radio-text">{{ $item.value }}</text>
      </div>
    </block>
  </div>

  <text class="demo-title">tabs选项数量:</text>
  <div class="radio-group">
    <block for="{{ tabsNumber }}">
      <div class="radio-container">
        <input class="radio-input" type="radio" name="tabsNumber"
               value="{{ $item.name }}" checked="{{ $item.checked }}" onchange="tabsNumberChange"></input>
        <text class="radio-text">{{ $item.value }}</text>
      </div>
    </block>
  </div>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
  data: {
    tabs: [
      {
        title: '选项',
        subTitle: '描述文案',
        number: '6',
        showBadge: true,
        badge: {
          arrow: true,
          stroke: true,
        },
      },
      {
        title: '选项二',
        subTitle: '描述文案描述',
        number: '66',
        showBadge: true,
        badge: {
          arrow: false,
          stroke: true,
        },
      },
      {
        title: '3 Tab',
        subTitle: '描述',
        number: '99+',
        showBadge: true,
        badge: {
          arrow: true,
        },
      },
      { title: '4 Tab',
        subTitle: '描述',
        showBadge: true,
        number: 0,
      },
      { title: '5 Tab',
        subTitle: '描述描述',
        number: '99+',
        showBadge: false,
      },
      { title: '3 Tab',
        subTitle: '描述',
        showBadge: false,
      },
      { title: '4 Tab',
        subTitle: '描述',
      },
      { title: '15 Tab',
        subTitle: '描述',
      }],
    activeTab: 0,
    isSwipeable: false,
    type: [
      { name: 'normal', value: '普通', checked: true },
      { name: 'capsule', value: '胶囊', checked: false },
      { name: 'hasSubTitle', value: '带描述', checked: false },
    ],
    typeCapsule: false,
    typeHasSubTitle: false,
    plus: [
      { name: 'has', value: '是', checked: true },
      { name: 'hasnt', value: '否', checked: false },
    ],
    swipeable: [
      { name: '是', value: '是', checked:false },
      { name: '否', value: '否', checked: true },
    ],
    hasPlus: true,
    contentHeight: [
      { name: 'has', value: '是', checked: false },
      { name: 'hasnt', value: '否', checked: true },
    ],
    // hasContentHeight: false,
    tabsNumber: [
      { name: '1', value: '一条', checked: false },
      { name: '2', value: '两条', checked: false },
      { name: '3', value: '三条', checked: false },
      { name: '-1', value: '很多', checked: true },
    ],
    isShowFilter: false,
  },
  tabsNumberChange(e) {
    if (e.value === '1') {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
          },
        ];
    } else if (e.value === '2') {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: true,
            },
          },
          {
            title: '选项二',
            subTitle: '描述文案描述',
            number: '66',
            showBadge: true,
          },
        ];
    } else if (e.value === '3') {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: true,
            },
          },
          {
            title: '选项二',
            subTitle: '描述文案描述',
            number: '66',
            showBadge: true,
          },
          {
            title: 'Tab',
            subTitle: '描述',
            number: '99+',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: false,
            },
          },
        ];
    } else {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: true,
            },
          },
          {
            title: '选项二',
            subTitle: '描述文案描述',
            number: '66',
            showBadge: true,
            badge: {
              arrow: false,
              stroke: true,
            },
          },
          {
            title: '3 Tab',
            subTitle: '描述',
            number: '99+',
            showBadge: true,
            badge: {
              arrow: true,
            },
          },
          { title: '4 Tab',
            subTitle: '描述',
            showBadge: true,
            number: 0,
          },
          { title: '5 Tab',
            subTitle: '描述描述',
            number: '99+',
            showBadge: false,
          },
          { title: '3 Tab',
            subTitle: '描述',
            showBadge: false,
          },
          { title: '4 Tab',
            subTitle: '描述',
          },
          { title: '15 Tab',
            subTitle: '描述',
          },
        ];
    }
  },
  typeChange(e) {
    if (e.value === 'hasSubTitle') {
      this.typeCapsule = true;
      this.typeHasSubTitle = true;
    } else if (e.value === 'capsule') {
      this.typeCapsule = true;
      this.typeHasSubTitle = false;
    } else {
      this.typeCapsule = false;
      this.typeHasSubTitle = false;
    }
  },
  plusChange(e) {
    if (e.value === 'hasnt') {
      this.hasPlus = false;
    } else {
      this.hasPlus = true;
    }
  },
  swipeableChange(e) {
    if (e.value === '是') {
      this.isSwipeable = true;
    } else {
      this.isSwipeable = false;
    }
  },
  handleTabClick({ index, tabsname }) {
    this[tabsname] = index;
  },
  handleTabChange({ index, tabsname }) {
    this[tabsname] = index;
  },
  handlePlusClick() {
    prompt.showDialog({
      message:'plus clicked',
      buttons:[{text:'确定',color:'#1677ff'}],
    });
  },
  activeTabChange(e) {
    this.activeTab = parseInt(e.value);
  },
};

```

```css
.tab-content {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  box-sizing: border-box;
  /* 如果 swipeable="{{true}}"，需要增加 height */
  /* height: 350px; */
  /* 为了体现 stickyBar 的作用而增加的 tab-content 的高度 */
  /*height: 50%;*/
  font-size: 20px;
  text-align: center;
  width: 100%;
}

/*.radio-group {*/
/*  padding: 15px 20px 0;*/
/*}*/

.radio {
  margin-right: 10px;
}

.demo-title {
  font-size: 22px;
  margin: 15px 20px 0;
  padding-bottom: 10px;
  font-weight: bold;
  border-bottom: 1px solid #ccc;
}

.radio-input {
  height:22px;
  width:22px;
  padding: 2px;
  margin-left: 2px;
}

.radio-text {
  font-size: 20px;
}

.radio-container {
  flex-direction : row;
  flex : 0 0;
  margin-right: 5px;
  margin-top: 4px;
}

.radio-group {
  flex-direction : row;
  flex-wrap : wrap;
  padding: 12px;
}
```