# guide 遮罩引导模块

用于引导新用户或者页面内的新功能使用说明。

## 截图
![avatar](../../../../../../gifs/guide.gif)
## 属性介绍
| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| btn_next | String | 下一步 | - | 引导按钮组中的下一步按钮的文案 | - | - |
| btn_jump | String | 跳过 | - | 引导按钮组中可跳过按钮的文案 | - | - |
| btn_over | String | 知道了 | - | 引导按钮组中，当引导内容在最后一页时按钮的文案 | - | - |
| has-jump | Boolean | false | - | 是否显示跳过按钮 | - | - |
| show | Boolean | false | - | 是否显示 guide 遮罩引导模块 | - | - |
| guide-list | Array | [] | - | guide 模块中的内容 | - | true |
| guide-over | function | () => { } | - | 跳过/关闭 guide 遮罩引导按钮的事件 | - | - |
| mask-click | Boolean | false | - | 是否可通过点击遮罩触发事件 | - | - |

## Bug & Tip
* `has-jump` 如为 `false`，guide 引导中的按钮只会显示一个 `btn_next` 按钮；
* `on-guide-over` 事件主要是用于控制当 guide 引导结束后或者当有跳过按钮出现时，点击按钮关闭 guide 引导的，如有必要也可以自行再添加事件；
* `guide-list` 是用于控制显示每页 guide 引导图片的内容、位置以及大小等；
  * 数组中的格式：`[{ url: '', x: '', y: '', width: '', height: '',},]`
  * `url`：guide 引导图的 url；
  * `x`：引导图在可视区域的 x 坐标位置；
  * `y`：引导图在可视区域的 y 坐标位置；
  * `width`：引导图的宽度；
  * `height`：引导图的高度；

## 代码示例

```xml
<element name='guide1' src="../../../../../../../mini_ali_ui/src/main/js/components/guide/index.hml"></element>
<element name='button1' src="../../../../../../../mini_ali_ui/src/main/js/components/button/index.hml"></element>
<div style="width: 100%;height: 100%; flex-direction: column;">
    <guide1
            is-show="{{guideShow}}"
            has-jump="{{guideJump}}"
            guide-list="{{list}}"
            guide-over="{{ closeGuide }}"
            mask-click="{{true}}">
    </guide1>
    <button1 type="primary" @on-tap="onShowJumpGuide">查看可跳过的引导图</button1>
    <button1 type="primary" @on-tap="onShowGuide">查看普通的引导图</button1>
</div>
```
```javascript
export default{
  data: {
    list: [
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ',
        x: '75px',
        y: '100px',
        width: '200px',
        height: '200px',
      },
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ',
        x: '125px',
        y: '75px',
        width: '200px',
        height: '100px',
      },
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ',
        x: '175px',
        y: '150px',
        width: '100px',
        height: '100px',
      },
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ',
        x: '200px',
        y: '200px',
        width: '100px',
        height: '150px',
      },
    ],
    guideShow: false,
    guideJump: true,
  },
  onLoad() {},
  closeGuide() {
    this.guideShow = false
  },
  onShowJumpGuide() {
    this.guideShow = true
    this.guideJump = true
  },
  onShowGuide() {
    this.guideShow = true
    this.guideJump =false
  },
};

```