import getI18n from '../../common/_util/geti18n';

const i18n = getI18n().guide;

export default{

  props:{
    isShow:{default:true},
    btn_next:{default:i18n.nextBtn},
    btn_jump:{default:i18n.jumpBtn},
    btn_over:{default:i18n.overBtn},
    hasJump:{default:false},
    guideList:{default:[]},
    guideOver:{default:() => {}},
    maskClick:{default:false},
  },
  data: {
    guideNumber: 1,
    guideCurrent: 1,
    guideLast: 1,
    showGuideList: [],
  },



  onAttached() {
    this.guideNumber = this.guideList.length
    this.guideCurrent = this.guideList.length
    this.showGuideList = this.guideList[this.guideNumber-this.guideCurrent]
  },
  onLayoutReady() {
    if (this.guideCurrent - 1 >= 0) {
      //this.guideCurrent = this.guideCurrent
      this.showGuideList = this.guideList[this.guideNumber-this.guideCurrent]
    }

  },
  onBtnClick() {
    if (this.guideCurrent > this.guideLast) {
      this.guideCurrent = this.guideCurrent -1
      this.onLayoutReady()
    } else {
      this.isShow = false;
    }

  },
  onGuideClick() {
    this.isShow = false;
    if (typeof this.guideOver === 'function') {
      this.guideOver();
    }
  },
  onMaskTap() {
    if (this.maskClick === true) {
      if (this.guideCurrent > this.guideLast) {
        this.onBtnClick();
      } else {
        this.onGuideClick();
      }
    }
  },
};
