import fmtEvent from '../../common/_util/fmtevent';
import fmtUnit from '../../common/_util/fmtunit';

export default{
  props: {
    className: {default:''},
    inputCls: {default:''},
    last: {default:false},
    value: {default:''},
    name: {default:''},
    type: {default:'text'},
    placeholderClass: {default:''},
    placeholderStyle: {default:''},
    disabled: {default:false},
    maxlength: {default:140},
    focus: {default:true},
    clear: {default:true},// 默认有清除功能
    syncInput: {default:false},
    controlled: {default:true},
    enableNative: {default:false}, // 兼容安卓input的输入bug
    inputMethod:{default:()=>{}},
    confirmMethod:{default:()=>{}},
    clearMethod:{default:()=>{}},
    focusMethod:{default:()=>{}},
    blurMethod:{default:()=>{}},
  },
  data: {
    visible: false,
    iconSizeClose: fmtUnit(18),
    iconSizeEye: fmtUnit(22),
  },
  onBlur(e) {
    this.focus = false
    const event = fmtEvent(this.props, e,this);
    if(this.blurMethod != undefined){
      this.blurMethod(event)
    }
  },
  onConfirm(e) {
    const event = fmtEvent(this.props, e,this);
    if(this.confirmMethod != undefined){
      this.confirmMethod(event)
    }
  },
  onFocus(e) {
    this.focus = true
    const event = fmtEvent(this.props, e,this);
    if(this.focusMethod != undefined){
      this.focusMethod(event)
    }
  },
  onInput(e) {

    this.value = e.value
    const event = fmtEvent(this.props, e,this);
    if(this.inputMethod != undefined){
      this.inputMethod(event)
    }
  },
  onClear(e) {

    this.value = ''
    const event = fmtEvent(this.props, e,this);
    if(this.clearMethod != undefined){
      this.clearMethod(event)
    }
  },
  onSwitchVisible() {
    this.visible = !this.visible
  },
};
