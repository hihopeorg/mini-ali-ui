# LongPassword 长密码框

长密码框  


## 截图
![avatar](../../../../../../gifs/LongPassword.gif)

## 属性介绍
| 属性名           | 类型                | 默认值 | 可选值 | 描述                                    | 最低版本 | 必填 |
| ---------------- | ------------------- | ------ | ------ | --------------------------------------- | -------- | ---- |
| class-name        | String              | ''     | 暂不支持 | 自定义的class                           |          |      |
| input-cls         | String              | ''     | 暂不支持 | 自定义input的class                      |          | -    |
| last             | Boolean             | false  | -      | 是否最后一行                            |          | -    |
| value            | String              | ''     | -      | 初始内容                                |          | -    |
| name             | String              | ''     | -      | 组件名字，用于表单提交获取数据          |          | -    |
| disabled         | Boolean             | false  | -      | 是否禁用                                |          | -    |
| maxlength        | Number              | 140    | -      | 最大长度                                |          | -    |
| focus            | Boolean             | false  | -      | 获取焦点                                |          | -    |
| clear            | Boolean             | true   | -      | 是否带清除功能，仅disabled为false才生效 |          | -    |
| input-method          | function |        | -      | 键盘输入时触发input事件                 |          | -    |
| confirm-method        | function |        | -      | 点击键盘完成时触发                      |          | -    |
| focus-method          | function |        | -      | 聚焦时触发                              |          | -    |
| blur-method           | function |        | -      | 失去焦点时触发                          |          | -    |
| clear-method          | function |        | -      | 点击清除icon时触发                      |          | -    |

## 代码示例



### hml
```
<element name='long-password' src="../../../../../../../mini_ali_ui/src/main/js/components/long-password/index.hml"></element>

<div style="flex-direction: column;width: 100%;">
  <div style="margin-top: 10px;" ></div>
  <text style="padding: 0 10px; font-size: 18px;">长密码框</text>
  <div style="margin-top: 10px;" ></div>
  <long-password
    value="{{longPassword}}" 
    clear="{{true}}" 
    input-method="{{ onInput }}"
    clear-method="{{ onClear }}">
  </long-password>
</div>
```

### js
```javascript
export default{
  data: {
    longPassword: '',
  },
  onInput(e) {
    this.longPassword = e.detail.value
  },
  onClear() {

    this.longPassword = ''

  },
};

```