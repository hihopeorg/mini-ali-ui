# am-radio 单选框

单选框。具体用法和小程序框架中 radio 保持一致，在 radio 基础上做了样式的封装。

## 截图
<img src="../../../../../../gifs/am-radio.gif" />


## 属性介绍
| 属性名 | 类型 | 默认值 | 可选值 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| value | String | - | - | 组件值，选中时 change 事件会携带的 value | - |
| checked | Boolean | false | true,false | 当前是否选中，可用来设置默认选中 | - |
| disabled | Boolean | false | true,false | 是否禁用 | - |
| id | String | - | - | 与label组件的for属性组合使用 | - |

## 示例

```xml
<element name="am-radio" src="../../../../../../../mini_ali_ui/src/main/js/components/am-radio/index"></element>
<div class="page" style="width: 100%;">
    <text class="page-description" style="font-size : 20px;">单选框</text>
    <div class="page-section">
        <div class="section section_gap">
            <div class="page-section-demo">
                <div class="radio" for="{{ items }}" key="label-{{ index }}">
                    <am-radio value="{{ $item.value }}" checked="{{ $item.checked }}" disabled="{{ $item.disabled }}"
                              id="{{ $item.id }}" @on-change="onChange"></am-radio>
                    <text style="display : flex; font-size : 20px;">{{ $item.desc }}</text>
                </div>
            </div>
            <div class="page-section-demo">
                <div class="radio" for="{{ items1 }}" key="label-{{ index }}">
                    <am-radio value="{{ $item.value }}" checked="{{ $item.checked }}" disabled="{{ $item.disabled }}">
                    </am-radio>
                    <text style="display : flex; font-size : 20px;">{{ $item.desc }}</text>
                </div>
            </div>
        </div>
    </div>
</div>
```
```css
.radio {
  display: flex; align-items: center;
}
.page-section-demo {
  padding: 24rpx;
}
```
```javascript
import prompt from '@system.prompt';

export default{
  data: {
    items: [
      { checked: true, disabled: false, value: 'a', desc: '单选框-默认选中', id: 'checkbox1' },
      { checked: false, disabled: false, value: 'b', desc: '单选框-默认未选中', id: 'checkbox2' },
    ],
    items1: [
      { checked: true, disabled: true, value: 'c', desc: '单选框-默认选中disabled', id: 'checkbox3' },
    ],
  },
  onSubmit(e) {
    prompt.showToast({
      message: e.detail.value.lib,
    });
  },
  onReset() {
  },
  radioChange() {
  },
  onChange(e) {
    var checked = e._detail.checked;
    var id = e._detail.target.dataset.id;
    if (id === 'checkbox1') {
      this.items[0].checked = checked;
      this.items[1].checked = !checked;
    } else {
      this.items[1].checked = checked;
      this.items[0].checked = !checked;
    }this.$element()
  }
};


```