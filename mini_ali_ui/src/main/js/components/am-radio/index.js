import fmtEvent from '../../common/_util/fmtevent';

export default {
    props: {
        value: {
            default: '',
        },
        checked: {
            default: false,
        },
        disabled: {
            default: false,
        },
        id: {
            default: '',
        },
    },
    onChange(e) {
        this.checked = e.checked;
        const event = fmtEvent(this.props, e, this);
        this.$emit('onChange',event);
    },
};
