export default{
  props: {
    className: {
      default:'',
    },
    overflowCount: {
      default:99,
    },
    text: {
      default:'',
    },
    dot: {
      default:false,
    },
    withArrow: {
      default:false,
    },
    direction: {
      default:'middle',
    },
    stroke: {
      default:false,
    },
    isWrap: {
      default:false,
    },
  },
};
