# SearchBar 搜索栏

搜索栏。


## 截图
![avatar](../../../../../../gifs/SearchBar.gif)
## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 必选 | 描述 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| value | String | -| - | - | 搜索框的当前值 |
| focus | Boolean | false | true,false |- | 自动获取光标 |
| show-voice | Boolean | false | true,false | - | 是否展示voice图标 |
| input-method | function | - | -| - | 键盘输入时触发 |
| clear-method | function | - | -| - | 点击 clear 图标触发 |
| focus-method | function | - | -| - | 获取焦点时触发 |
| blur-method | function | - | -| - | 失去焦点时触发 |
| cancel-method | function | - | -| - | 点击取消时触发 |
| voice-click | function | - | -| - | 点击voice图标时触发 |
| submit-method | function | - | -| - |点击键盘的 enter 时触发 |
| disabled | Boolean | false |true,false | - | 设置禁用 |
| max-length | Number | - | - | - | 最多允许输入的字符个数 |
| show-cancel-button | Boolean | false | true,false | - | 是否一直显示取消按钮 |
| border-color | String | #1677ff | - | - | 搜索输入框边框色 |


## 示例

```hml
<element name='search-bar' src="../../../../../../../mini_ali_ui/src/main/js/components/search-bar/index.hml"></element>
<div style="flex-direction: column; width: 100%;">
  <div>
    <search-bar
            value="{{value}}"
            focus="{{true}}"
            disabled="{{false}}"
            max-length="{{20}}"
            show-voice="{{showVoice}}"
            placeholder="搜索"
            input-method="{{ handleInput }}"
            clear-method="{{ handleClear }}"
            cancel-method="{{ handleCancel }}"
            submit-method="{{ handleSubmit }}"
            show-cancel-button="{{true}}" >
    </search-bar>
  </div>
  <div style="flex-direction: row;">
    <input on:change='onChange' type="checkbox" id="showVoice"/> 
    <label style="font-size:18px" target="showVoice">是否展示Voice图标</label>
  </div>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
  data: {
    value: '',
    showVoice: false,
  },
  handleInput(value) {

  },
  handleClear() {

  },
  handleCancel() {
  },
  handleSubmit(value) {

    prompt.showDialog({
      message: value,
      buttons:[{text:'确定', color: '#1677ff'}]
    })
  },
  onChange(e) {
    this.showVoice = e.checked
  },
};


```
