import getI18n from '../../common/_util/geti18n';

const i18n = getI18n().searchBar;

export default{
  props: {
    className: {default:''},
    placeholder: {default:''},
    focus: {default:false},
    showVoice: {default:false},
    disabled: {default:false},
    borderColor: {default:'#1677ff'},
    controlled: {default:true},
    showCancelButton: {default:false},
    maxLength:Number,
    enableNative: {default:false}, // false 处理 fixed 定位后输入框内容闪动的问题
    value: {default:''},
    inputMethod:{default:()=>{}},
    clearMethod:{default:()=>{}},
    changeMethod:{default:()=>{}},
    focusMethod:{default:()=>{}},
    blurMethod:{default:()=>{}},
    cancelMethod:{default:()=>{}},
    submitMethod:{default:()=>{}},
    voiceClickMethod:{default:()=>{}},
  },
  data: {
    i18nCancel: i18n.cancel,
  },
  handleInput(e) {
    this.value = e.value
    if(this.inputMethod != undefined){
      this.inputMethod(this.value)
    }
  },
  handleClear() {
    setTimeout(() => {
      this.handleFocus();
    }, 100);

    this.value = ''
    this.doClear();
  },
  doClear() {
    if(this.clearMethod != undefined){
      this.clearMethod('')
    }
    if(this.changeMethod != undefined){
      this.changeMethod('')
    }
  },
  handleFocus() {
    this.focus = true
    if(this.focusMethod != undefined){
      this.focusMethod()
    }
  },
  handleBlur() {
    if(this.blurMethod != undefined){
      this.blurMethod()
    }
  },
  handleCancel() {
    this.value = ''
    if(this.cancelMethod != undefined){
      this.cancelMethod()
    }else{
      this.doClear()
    }
  },
  handleConfirm() {
      if(this.submitMethod != undefined){
        this.submitMethod(this.value)
      }
  },
  handleVoice() {
    if (this.voiceClickMethod != undefined) {
      this.voiceClickMethod();
    }
  },
};
