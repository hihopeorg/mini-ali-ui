export default {
    props: {
        // title component have boder-bottom line
        hasLine: {
            default: false
        },
        // type: arrow、close、more、'';
        type: {
            default: ''
        },
        // if type="arrow", need to write path
        iconUrl: {
            default: ''
        },
        // developer can use class for style
        className: {
            default: ''
        },
    },
    data: {},

    onClick(e) {
        this.$emit('onActionTap', e)
    },
}
