# title 标题模块

页面内每个容器模块中的标题模块。


## 截图
![Image text](../../../../../../gifs/title.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String| - | - | 自定义class | 暂不支持 |
| has-line | Boolean| false | - | 是否有下划线 |- |
| icon-url | String | - | - | 标题旁边的 icon URL | - |
| type | String| - | arrow、close、more、custom | 标题可操作区域类型，包含：`arrow`：箭头；`close`：关闭；`more`：更多；`custom`：自定义内容，需要传递名为 operation 的具名插槽；默认为空。 | - |
| @on-action-tap | EventHandle | () => {} | - | type 属性有具体值时可点击事件 | - |

## Bug & Tip
* `icon-url` 是默认以背景图的方式展示在一个正方形的元素中；
* 如 `type` 为空，`onActionTap` 将无效；

## 代码示例

```xml
<element name='title' src="../../../../../../../mini_ali_ui/src/main/js/components/title/index.hml"></element>

<div class="container">
    <title
            has-Line="true"
            icon-url="https://gw.alipayobjects.com/mdn/miniProgram_mendian/afts/img/A*wiFYTo5I0m8AAAAAAAAAAABjAQAAAQ/original"
            type="arrow"
            @on-Action-tap="titleGo"
            >
        <text>内部标题可跳转</text>
    </title>

    <title
            has-Line="true"
            icon-Url="https://gw.alipayobjects.com/mdn/miniProgram_mendian/afts/img/A*wiFYTo5I0m8AAAAAAAAAAABjAQAAAQ/original"
            @on-Action-tap="titleGo"
            type="custom"
            >
        <text>内部标题可跳转</text>
        <text slot="operation">自定义跳转></text>
    </title>

    <title
            has-Line="true"
            type="more"
            @on-Action-tap="titleMore"
            >
        <text>内部标题无 icon 展开气泡菜单</text>
    </title>

    <title
            has-Line="true"
            icon-Url="https://t.alipayobjects.com/images/T1HHFgXXVeXXXXXXXX.png"
            type="close"
            @on-Action-tap="titleClose"
            >
        <text>内部标题可关闭</text>
    </title>

    <title
            has-Line="true"
            >
        <text style="text-overflow : ellipsis; max-lines : 1;">标题过长后的处理，标题单行展示。标题过长后的处理，标题单行展示。</text>
    </title>

    <title
            has-Line="true"
            class-Name="changeColor"
            icon-Url="https://gw.alipayobjects.com/mdn/miniProgram_mendian/afts/img/A*wiFYTo5I0m8AAAAAAAAAAABjAQAAAQ/original"
            type="arrow"
            @on-Action-tap="titleGo"
            >
        <text style="font-size : 30px; color : #f32600;">使用class修改样式</text>
    </title>
</div>
```

```css
.container {
  background-color: #ffffff;
  flex-direction: column;
  padding-top: 12px;
}
.am-title {
  padding-top: 12px;
}

.changeColor {
  font-size: 30px;
  color: #f32600;
}
```

```javascript
export default{

  data: {},
  onLoad() {},
  titleGo() {
    prompt.showToast({
      message: '点击箭头，可设置跳转',
      duration: 2000
    })
  },
  titleMore() {
    prompt.showToast({
      message: '点击更多，展开气泡菜单',
      duration: 2000
    })
  },
  titleClose() {
    prompt.showToast({
      message: '点击关闭，可设置关闭',
      duration: 2000
    })
  },
}
```