# am-checkBox 复选框

复选框。 当 `ctrlChecked === undefined` (默认)时 am-checkbox 是非受控组件，否则是一个受控组件。

## 截图
<img src="../../../../../../gifs/am-checkbox.gif"/>

## 属性介绍
| 属性名 | 类型 | 默认值 | 可选值 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| value | String | - | - | 组件值，选中时 change 事件会携带的 value | - |
| ctrl-checked | Array | undefined | true,false | 当 ctrlChecked 不等于 undefined 时 am-checkbox 是受控组件。 | - |
| checked | Boolean | false | true,false | 默认是否选中 | - |
| disabled | Boolean | false | true,false | 是否禁用 | - |
| @on-change | (e: Object) => void |  | - | change 事件触发的回调函数 | - |
| id | String | - | - | 与 label 组件的 for 属性组合使用 | - |

## 示例


```xml
<element name="am-checkbox" src="../../../../../../../mini_ali_ui/src/main/js/components/am-checkbox/index"></element>
<element name="am-list" src="../../../../../../../mini_ali_ui/src/main/js/components/list/index"></element>
<element name="am-list-item" src="../../../../../../../mini_ali_ui/src/main/js/components/list/list-item/index"></element>
<!--<element name="button" src="../../../../../../../mini_ali_ui/src/main/js/components/button/index"></element>-->
<div class="page">

    <am-list slot-header="{{true}}">
        <text slot="header">
            列表+复选框（非受控）
        </text>
        <block for="{{ items }}">
            <am-list-item
                    thumb=""
                    arrow="{{ false }}"
                    index="{{ $idx }}"
                    key="items-{{ $idx }}"
                    last="{{ $idx === (items.length - 1) }}"
                    >
                <div style="display : flex; align-items : center;flex-direction: row;">
                    <am-checkbox dataset="{{ {id:$item.id} }}" id="{{ $item.id }}" value="{{ $item.value }}"
                                 disabled="{{ $item.disabled }}" checked="{{ $item.checked }}"></am-checkbox>
                    <label>{{ $item.title }}</label>
                </div>
            </am-list-item>
        </block>
    </am-list>

    <div style="padding : 12px 0 0 12px;flex-direction: row;">
        <button class="button" onclick="checkedON" style="margin-left : 10px;">全选</button>
        <button class="button" onclick="checkedOFF" >不全选</button>
    </div>


    <am-list slot-header="{{ true }}">
        <text slot="header">
            列表+复选框（受控）
        </text>
        <block for="{{ items1 }}">
            <am-list-item
                    thumb=""
                    arrow="{{ false }}"
                    index="{{ $idx }}"
                    key="items-{{ $idx }}"
                    last="{{ $idx === (items.length - 1) }}"
                    >
                <div style="display : flex; align-items : center;flex-direction: row;">
                    <am-checkbox dataset="{{ {id:$item.id} }}" id="{{ $item.id }}" value="{{ $item.value }}"
                                 disabled="{{ $item.disabled }}" ctrl-checked="{{ $item.ctrlChecked }}"
                                 @on-change="{{onChange}}"></am-checkbox>
                    <label>{{ $item.title }}</label>
                </div>
            </am-list-item>
        </block>
    </am-list>
</div>
```

```javascript
export default{
  data: {
    items: [
      { value: 'a', title: '复选框-默认未选中', id: 'checkbox1' },
      { checked: true, value: 'b', title: '复选框-默认选中', id: 'checkbox2' },
      { checked: true, disabled: true, value: 'c', title: '复选框-默认选中disabled', id: 'checkbox3' },
    ],
    items1: [
      { ctrlChecked: false, disabled: false, value: 'd', title: '复选框-默认未选中', id: 'checkbox4' },
      { ctrlChecked: true, disabled: true, value: 'e', title: '复选框-默认未选中disabled', id: 'checkbox5' },
      { ctrlChecked: true, value: 'f', title: '复选框-默认选中', id: 'checkbox6' },
    ],
  },
  onChange(e) {
    const { id } = e._detail.currentTarget.dataset;
    const value = e._detail.currentTarget.dataset;
    const length  = this.items1.length;
    for (let index = 0; index < length; index += 1) {
      if (this.items1[index].id === id) {
        this.items1[index].ctrlChecked = value.ctrlChecked;
        break;
      }
    }
  },
  // 全选
  checkedON() {
    this.checkedAll(true);
  },
  // 全不选
  checkedOFF() {
    this.checkedAll(false);
  },
  checkedAll(status) {
    const items1 = this.items1.map((element) => ({
      ...element,
      ctrlChecked: status,
    }));
    this.items1 = items1;
  },
};
```