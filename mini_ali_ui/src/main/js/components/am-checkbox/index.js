import fmtEvent from '../../common/_util/fmtevent';

/**
 * 对齐 ant design checkbox 的设计，增加 ctrlChecked 属性
 * 当 props 中有 checked 传入时，am-checkbox 是非受控组件
 * 当 props 中不传入 checked 而使用 ctrlChecked 时，am-checkbox 是受控组件
 */
export default{
  props: {
    value: {
      default: '',
    },
    checked: {
      default: false,
    },
    ctrlChecked: {
      default: undefined,
    },
    disabled: {
      default: false,
    },
    id: {
      default: '',
    },
    dataset: {
      default: {},
    }
  },
  data: {
    // 组件内维护的 chackbox 勾选状态
    mchecked: false,
  },
  onInit() {
    const checked = this.checked;
    this.mchecked = checked;
    this.$watch('ctrlChecked', 'deriveDataFromProps');
  },
  onPageShow() {
    const checked = this.checked;
    this.mchecked = checked;
    this.deriveDataFromProps(this.ctrlChecked, this.ctrlChecked);
  },
  // props 改变时
  deriveDataFromProps(newV, oldV) {
    const mchecked = this.mchecked;
    const oldChecked = oldV;
    const ctrlChecked = newV;
    // oldChecked===undefined 说明未传入 checked 属性，am-checkbox 将成为不受控组件
    // oldChecked 有传入值 mchecked 受外部 checked 属性控制
    if (mchecked !== ctrlChecked && oldChecked !== undefined) {
      this.mchecked = ctrlChecked;
    }
  },
  onChange(e) {
    if (this.ctrlChecked !== undefined) {
      this.ctrlChecked = e.checked;
    }
    this.mchecked = e.checked;
    const event = fmtEvent(this.props, e, this);
    this.$emit('onChange', event);
  },
};
