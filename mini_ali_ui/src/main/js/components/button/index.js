import fmtClass from '../../common/_util/fmtclass';
import fmtEvent from '../../common/_util/fmtEvent';

const prefixCls = 'am-button';
export default{
  mixins: [],
  data: {
    baseClass: prefixCls,
    mHoverClass: undefined,
    touching: false,
  },
  props: {
    className: {
      default:'',
    },
    type: {
      default:'',
    },
    dataName: {
      default:'',
    },
    disabled: {
      default:false,
    },
    shape: {
      default:'',
    },
    subtitle: {
      default:''
    },
    capsuleSize: {
      default: 'medium',
    },
    capsuleMinWidth: {
      default:false,
    },
    showLoading: {
      default:false,
    },
    hoverClass: {
      default:undefined,
    },
    value: {
      default: '',
    },
    dataset: {
      default: {},
    }
  },
  onInit() {
    if (!this.hoverClass) {
      this.hoverClass = 'am-button-active';
      if (this.type === 'text') {
        this.hoverClass = 'am-button-active-text';
      }
    }
    this.baseClass = this.wrapBaseCls();
    this.mHoverClass = this.hoverClass;
    this.$watch('type', 'deriveDataFromProps');
    this.$watch('disabled', 'deriveDataFromProps');
    this.$watch('subtitle', 'deriveDataFromProps');
    this.$watch('shape', 'deriveDataFromProps');
    this.$watch('capsuleSize', 'deriveDataFromProps');
    this.$watch('capsuleMinWidth', 'deriveDataFromProps');
  },
  deriveDataFromProps() {
      this.baseClass =this.wrapBaseCls();
  },
  onPageShow() {
      if (!this.hoverClass) {
        this.hoverClass = 'am-button-active';
        if (this.type === 'text') {
          this.hoverClass = 'am-button-active-text';
        }
      }
      this.baseClass = this.wrapBaseCls();
      this.mHoverClass = this.hoverClass;
  },

    wrapBaseCls() {
      const type = this.type;
      const disabled = this.disabled;
      const subtitle = this.subtitle;
      const shape = this.shape;
      const capsuleSize = this.capsuleSize ? this.capsuleSize : 'medium';
      const capsuleMinWidth = this.capsuleMinWidth;
      let capsuleMinWidthCls = '';
      if (capsuleMinWidth) {
        capsuleMinWidthCls = `${prefixCls}-capsule-${capsuleSize}-minwidth`;
      }

      const ret = fmtClass({
        [`${prefixCls}`]: true,
        [`${prefixCls}-primary`]: type === 'primary',
        [`${prefixCls}-ghost`]: type === 'ghost',
        [`${prefixCls}-warn`]: type === 'warn',
        [`${prefixCls}-warn-ghost`]: type === 'warn-ghost',
        [`${prefixCls}-text`]: type === 'text',
        [`${prefixCls}-light`]: type === 'light',
        [`${prefixCls}-capsule ${prefixCls}-capsule-${capsuleSize} ${capsuleMinWidthCls}`]:
          shape === 'capsule',
        [`${prefixCls}-disabled`]: disabled,
        [`${prefixCls}-subtitle`]: subtitle,
      });
      console.debug("baseCls = " + ret);
      return ret;
    },
    onButtonTap(e) {
      const event = fmtEvent(this.props, e, this);
      this.$emit('onTap', event);
    },
    onGetAuthorize(e) {
      const event = fmtEvent(this.props, e, this);
      this.$emit('onGetAuthorize', event);
    },
    onError(e) {
      const event = fmtEvent(this.props, e, this);
      this.$emit('onError', event);
    },
};
