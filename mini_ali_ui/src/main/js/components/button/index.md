# Button 按钮

按钮，用户只需单击一下即可执行操作并做出选择。常用于表单提交、界面跳转、模块引导点击。具体用法和小程序框架中 button 保持一致，在 button 基础上做了样式的封装。


## 截图
<img src="../../../../../../gifs/button.gif" />


## 属性介绍
| 属性 | 类型 | 默认值 | 可选值 | 描述 | 必填 |
| :--- | :--- | :--- | :--- | :--- | :--- |
| type | String | default | default, primary, ghost, warn,  warn-ghost, text, light | 按钮样式 | - |
| subtitle | String | - | - | 子标题 | - |
| shape | String | default | default, capsule | 按钮形状 | - |
| capsule-size | String | medium | large, medium, small | 胶囊按钮大小 | - |
| capsule-min-width | Boolean | false | true, false | 是否启用胶囊按钮最小宽度 | - |
| disabled | Boolean | false | true, false | 是否禁用 | - |
| show-loading | Boolean | false | true, false | 按钮文字前是否带 loading 图标 | - |
| hover-class | String | button-hover | - |按钮按下去的样式类。`button-hover` 默认为 `{background-color: rgba(0, 0, 0, 0.1); opacity: 0.7;}`，`hover-class="none"` 时表示没有点击态效果 | - |
| hover-start-time | Number | 20 | - | 按住后多少事件后出现点击状态，单位毫秒 | 暂不支持  |
| hover-stay-time | Number | 70 | - | 手指松开后点击状态保留时间，单位毫秒 | 暂不支持 |
| hover-stop-propagation | Boolean | false | true, false | 是否阻止当前元素的祖先元素出现点击态 | 暂不支持 |
| form-type | String | - | - | 有效值：submit, reset，用于 `<form />` 组件，点击分别会触发 submit/reset 事件 | 暂不支持 |
| @on-tap | EventHandle | - | - | 点击 | - |


## 示例

```xml
<element name="am-button" src="../../../../../../../mini_ali_ui/src/main/js/components/button/index"></element>
<element name="am-radio" src="../../../../../../../mini_ali_ui/src/main/js/components/am-radio/index"></element>
<element name="am-checkbox" src="../../../../../../../mini_ali_ui/src/main/js/components/am-checkbox/index"></element>
<div class="container page">
    <am-button value="{{ title }}" @on-tap="onTest" show-loading="{{ showLoading }}" data-name="{{ dataName }}"
               type="{{ type }}" subtitle="{{ subtitle }}" disabled="{{ disabled }}" shape="{{ shape }}"
               capsule-size="{{ capsuleSize }}" capsule-min-width="{{ capsuleMinWidth }}" style="min-width:{{shape === 'capsule' || type === 'text' ? '0' : '100%'}}">
        {{ title }}
    </am-button>
    <text class="title">主标题</text>
    <input value="{{ title }}" onchange="titleChange" class="inputItem"/>
    <text class="title">副标题</text>
    <input type="text" value="{{ subtitle }}" onchange="subtitleChange" class="inputItem"/>
    <text class="title">按钮类型</text>
    <div style="flex-wrap : wrap; flex-direction : row;">
        <block for="{{ types }}">
            <div style="flex-direction : row;" id="{{ $idx }}" onclick="typeChange">
                <am-radio checked="{{ $item.checked }}" disabled="true"></am-radio>
                <text class="label">{{ $item.value }}</text>
            </div>
        </block>
    </div>
    <text class="title">形状</text>
    <div style="flex-wrap : wrap; flex-direction : row;">
        <block for="{{ shapes }}">
            <div style="flex-direction : row;" id="{{ $idx }}" onclick="shapeChange">
                <am-radio checked="{{ $item.checked }}" disabled="{{ true }}"></am-radio>
                <text class="label">{{ $item.value }}</text>
            </div>
        </block>
    </div>
    <text class="title">胶囊按钮大小</text>
    <div style="flex-wrap : wrap; flex-direction : row;">
        <block for="{{ capsuleSizes }}">
            <div style="flex-direction : row;" id="{{ $idx }}" onclick="sizeChange">
                <am-radio checked="{{ $item.checked }}" disabled="true"></am-radio>
                <text class="label">{{ $item.value }}</text>
            </div>
        </block>
    </div>
  <div class="changeItem" style="flex-direction: row;">
    <am-checkbox @on-change="onDisableChange" id="disabled__"></am-checkbox>
    <text class="label" style="margin-left: 4px;">是否禁用</text>
  </div>
    <div class="changeItem" style="flex-direction: row;">
        <am-checkbox @on-change="onMinWidthChange" id="miniBtn__"></am-checkbox>
        <text class="label" style="margin-left: 4px;">是否限制胶囊按钮最小宽度</text>
    </div>
    <div class="changeItem" style="flex-direction: row;">
        <am-checkbox @on-change="onLoadingChange" id="loading__"></am-checkbox>
        <text class="label" style="margin-left: 4px;">是否显示 loading</text>
    </div>

</div>
```
```css
@import '../../app.css';
.container {
  padding: 20px;
  align-items: flex-start;
}

.container button {
  margin-bottom: 24px;
}

.title {
  padding-top: 24px;
  font-weight: bold;
}
.inputItem {
  margin: 8px 0 12px;
  padding: 4px 8px;
  font-size: 15px;
  border: 0.5px solid #eee;
  border-radius: 0px;
  background-color: white;
}
.radio-group {
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 24px;
}
.radio {
  display: flex;
  align-items: center;
  margin-right: 16px;
}
.changeItem {
  display: flex;
  align-items: center;
  margin-top: 12px;
}
.title {
  font-size:20px;
}
.label{
  font-size: 18px;
}
```
```javascript
export default{
  data: {
    title: '按钮操作 Normal',
    subtitle: '',
    disabled: false,
    dataName: '1',
    type: '',
    shape: 'default',
    capsuleSize: 'medium',
    capsuleMinWidth: false,
    showLoading: false,
    types: [
      { name: 'default', value: 'default', checked: true },
      { name: 'primary', value: 'primary', checked: false },
      { name: 'ghost', value: 'ghost', checked: false },
      { name: 'text', value: 'text', checked: false },
      { name: 'warn', value: 'warn', checked: false },
      { name: 'warn-ghost', value: 'warn-ghost', checked: false },
      { name: 'light', value: 'light', checked: false },
    ],
    shapes: [
      { name: 'default', value: 'default', checked: true },
      { name: 'capsule', value: 'capsule', checked:false },
    ],
    capsuleSizes: [
      { name: 'small', value: 'small' ,checked:false},
      { name: 'medium', value: 'medium', checked: true },
      { name: 'large', value: 'large' ,checked:false},
    ],
  },
  onLoad() {
  },
  typeChange(e) {
    var idx = e.target.attr.id;
    var oldStatus = this.types[idx].checked;
    if (oldStatus) {
      return;
    }
    for (var i = 0; i < this.types.length; i++) {
      this.types[i].checked = (i == idx) ? true : false;
    }
    this.type = this.types[idx].value;
  },
  shapeChange(e) {
    var idx = e.target.attr.id;
    var oldStatus = this.shapes[idx].checked;
    if (oldStatus) {
      return;
    }
    for (var i = 0; i < this.shapes.length; i++) {
      this.shapes[i].checked = (i == idx) ? true : false;
    }
    this.shape = this.shapes[idx].value;
  },
  sizeChange(e) {
    var idx = e.target.attr.id;
    var oldStatus = this.capsuleSizes[idx].checked;
    if (oldStatus) {
      return;
    }
    for (var i = 0; i < this.capsuleSizes.length; i++) {
      this.capsuleSizes[i].checked = (i == idx) ? true : false;
    }
    this.capsuleSize = this.capsuleSizes[idx].value;
  },
  titleChange(e) {
    this.title = e.value;
  },
  subtitleChange(e) {
    this.subtitle= e.value;
  },
  onDisableChange(e) {
    this.disabled=e._detail.checked;
  },
  onMinWidthChange(e) {
    this.capsuleMinWidth= e._detail.checked;
  },
  onTest() {
    // e.target.dataset.name
  },
  onLoadingChange(e) {
    this.showLoading= e._detail.checked;
  },
};


```