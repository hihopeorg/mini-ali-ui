## Avatar 头像

头像。

## 截图
<img src="../../../../../../gifs/avatar.gif" />

## 属性介绍
| 属性 | 类型 | 默认值 | 可选值 | 描述 | 最低版本 | 必填 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| class-name | String | - | - | 自定义class | 暂不支持 | - |
| src | String | 默认蓝底头像 | - | 头像图片资源地址 | - | - |
| size | String | md | lg, md, sm, xs | 头像尺寸大小 | - | - |
| shape | String | circle | standard, circle, square | 头像形状 | - | - |
| name | String | - | - | 设置用户名 | - | - |
| desc | String | - | - | 设置摘要信息 | - | - |
| name | String | - | - | 设置用户名 | - | - |
| @on-error | EventHandle | (e: Object) => void | - | 图片资源加载失败回调 | 暂不支持 | - |

## 示例

```xml
<div>
  <!--普通头像组件-->
  <avatar src="xxxx" shape="standard"/>
  <!--带用户名和摘要描述头像组件-->
  <avatar src="xxxx" size="lg" name="用户名" desc="摘要描述" shape="standard" />
</div>
```
