import getI18n from '../../common/_util/geti18n';

const i18n = getI18n().avatar;

export default {
    props: {
        className: {
            defalt: '',
        },
        shape: {
            default: 'circle',
        },
        size: {
            default: 'md',
        },
        src: {
            default: 'common/images/avatar.png',
        },
        name: {
            default: '',
        },
        desc: {
            default: '',
        },
        lazyLoad: {
            default: false,
        }
    },
    onPageShow() {
        const name = this.name;
        const desc = this.desc;
        if (!name && desc) {
            console.error(i18n.error);
        }
    },
    // 图片加载失败
    _onError(e) {
        this.$emit('onError',
            e
        );
    },
};
