import fmtUnit from '../../common/_util/fmtunit';

export default{
  props: {
    type: {default:'primary',},
    iconType:{default: '',},
    className:{default: '',},
    size: {default:'lg',},
    ghost: {default:false},
  },
  data: {
    bgClass: {
      primary: 'am-tag-bg-primary',
      warning: 'am-tag-bg-warning',
      success: 'am-tag-bg-success',
      danger: 'am-tag-bg-danger',
    },
    iconClass: {
      primary: 'am-tag-icon-primary',
      warning: 'am-tag-icon-warning',
      success: 'am-tag-icon-success',
      danger: 'am-tag-icon-danger',
    },
    ghostClass: {
      primary: 'am-tag-ghost-primary',
      warning: 'am-tag-ghost-warning',
      success: 'am-tag-ghost-success',
      danger: 'am-tag-ghost-danger',
    },
    iconSizeSm: fmtUnit(10),
    iconSize: fmtUnit(12),
  },
  onInit(){
    console.info('tag iconSizeSm = ' + this.iconSizeSm + ", iconSize = " + this.iconSize);
  },
  getIconColor() {
    switch (this.type) {
      case "primary":
        return "#1677ff";
      case "warning":
        return "#ff8f1f"
      case "danger":
        return "#ff6010";
      default:
        return "#00B578";
    }
  }
};
