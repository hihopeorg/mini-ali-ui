# Tag 标签

突出利益点、以及属性说明的标签

## 预览

<img src="../../../../../../gifs/tag.gif">


## 属性

| 属性名    | 类型   | 默认值    | 可选项                                        | 描述     | 最低版本 | 必填 |
| --------- | ------ | --------- | --------------------------------------------- | -------- | -------- | ---- |
| class-name | String | ''        | -                                             | 类名称   | 暂不支持      | -    |
| type      | String | 'primary' | primary<br />success<br />warning<br />danger | 标签类型 | -        | -    |
| icon-type  | String | ''        |                                               | 图标类型 | -        | -    |
| size      | String | 'lg'      | lg<br />sm                                    | 标签大小 | -        | -    |

## slots

| slotName | 说明         |
| -------- | ------------ |
| ''       | 标签内部文案 |

## 示例
```xml
<element name="tag" src="../../../../../../../mini_ali_ui/src/main/js/components/tag/index"></element>
<element name="am-list-item" src="../../../../../../../mini_ali_ui/src/main/js/components/list/list-item/index"></element>
<element name="am-switch" src="../../../../../../../mini_ali_ui/src/main/js/components/am-switch/index"></element>
<div style="padding: 12px;background-color: #f5f5f5;flex-direction: column;">
  <div style="width:100%;justify-content: space-around;">
    <tag size="lg" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="primary"><text>标签</text></tag>
    <tag size="lg" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="warning"><text>标签</text></tag>
    <tag size="lg" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="danger"><text>标签</text></tag>
    <tag size="lg" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="success"><text>标签</text></tag>
  </div>
  <div style="width:100%;justify-content: space-around; margin-top: 20px;">
    <tag size="sm" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="primary"><text>标签</text></tag>
    <tag size="sm" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="warning"><text>标签</text></tag>
    <tag size="sm" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="danger"><text>标签</text></tag>
    <tag size="sm" icon-type="{{useIcon ? 'qr' : ''}}" ghost="{{ghost}}" type="success"><text>标签</text></tag>
  </div>
  <div style="padding: 20px 10px;flex-direction: column;">
    <am-list-item slot-extra="{{true}}">
      <text >图标</text>
      <am-switch slot="extra" @on-change="setInfoIcon" checked="{{useIcon}}"></am-switch>
    </am-list-item>
    <am-list-item slot-extra="{{true}}">
      <text>线框样式</text>
      <am-switch slot="extra" @on-change="setInfoGhost" checked="{{ghost}}"></am-switch>
    </am-list-item>
  </div>
</div>
```

```javascript
export default {
  data: {
    useIcon:false,
    ghost:false,
  },
  setInfoIcon(e) {
    this.useIcon = e._detail.checked;
  },
  setInfoGhost(e) {
   this.ghost = e._detail.checked;
  }
};
```
