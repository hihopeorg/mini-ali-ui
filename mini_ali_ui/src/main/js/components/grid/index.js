import fmtUnit from '../../common/_util/fmtunit';

export default{
  data: {
    getColumnBorderIndex: 0,
    iconSize: fmtUnit(28),
  },
  props: {
    columnNum: {default:3},
    circular: {default:false},
    list: {default:[]},
    gridItemClick: {default:()=>{}},
    hasLine: {default:true},
    infinite: {default:false},
    multiLine: {default:false},
    infiniteHeight: {default:fmtUnit('90px')},
    gridName: {default:''},
  },
  onAttached() {
    this.clearBorder();
    this.createGridName();
  },
  onGridItemClick(e) {
    if(this.gridItemClick != undefined){
      this.gridItemClick({
        detail: {
          index: e.target.dataSet.index,
        },
      });
    }


  },
  clearBorder() {
    if (this.columnNum === 3) {
      const rows = this.list.length % this.columnNum
      this.getColumnBorderIndex = rows === 0 ? 3 : rows
    }
  },
  createGridName() {
    if (this.infinite) {
      if (this.gridName === '' && !this.gridName) {
        this.gridName = `grid${Math.floor(Math.random() * 100000)}`;
      }
    }
  },
};
