# Grid 宫格

按照不同的业务场景，可选择不同列数的宫格，包含了 2、3、4、5 列四种列数的宫格。

## 截图
![avatar](../../../../../../gifs/Grid.gif)
## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| column-num | Number | 3 | 2/3/4/5 | 设置宫格的列数 | - | true |
| circular | Boolean | false | - | item 图是否为圆形 | - | - |
| list | Array | - | - | 宫格数据 | - | true |
| grid-item-click | function | (index: Number) => void | - | 点击宫格项回调 | - | - |
| has-line | Boolean | true | - | 3 列宫格时才有的间隔线 | - | - |
| infinite | Boolean | false | - | 5 列宫格时是否为无限滚动模式 | - | - |
| multi-line | Boolean | false | - | 5 列宫格时是否以多行形式展示 | - | - |
| infinite-height | String | 90px | - | 无限滚动模式时的宫格整体高度 | - | - |
| grid-name | String | - | - | 无限滚动宫格的名称 | - | - |

## Bug & Tip
* `has-line` 仅在 3 列宫格中才有效果；
* `circular` 圆角仅在 4/5 列宫格中才有效果；
* `infinite` 无限滚动模式的宫格仅在 5 列宫格，且列数超过 5 条之后才会有效果；
* `multi-line` 为 `true` 时，且未设置 `infinite` 的话，宫格最终会以 5 列多行的形式展示数据；
* 如在一个页面中有多个无限滚动的 5 列宫格，建议增加使用 `grid-name` 属性，避免分页符表现错误；
* 当使用 5 列的无限滚动时，需要同时引入 **pagination** 组件；

## 代码示例

```xml
<element name='grid' src="../../../../../../../mini_ali_ui/src/main/js/components/grid/index.hml"></element>
<div style="flex-direction: column;">
    <div style="height: 10px;background-color: gainsboro;"></div>
    <grid grid-item-click="{{ onItemClick }}" column-num="{{5}}" list="{{list55}}" infinite="{{true}}"></grid>
    <div style="height: 10px;background-color: gainsboro;"></div>
    <text style="color: red;font-size: 20px;">下面这个分页的高度是重置过的……</text>
    <grid grid-item-click="{{ onItemClick }}" column-num="{{5}}" list="{{list55}}" infinite="{{true}}" grid-name="newGridName" circular="{{true}}" infinite-height="120px"></grid>

    <div style="height: 10px;background-color: gainsboro;"></div>
    <grid grid-item-click="{{ onItemClick }}" column-num="{{5}}" list="{{list5}}"></grid>

    <div style="height: 10px;background-color: gainsboro;"></div>
    <grid grid-item-click="{{ onItemClick }}" column-num="{{5}}" list="{{list55}}" ></grid>

    <div style="height: 10px;background-color: gainsboro;"></div>
    <grid grid-item-click="{{ onItemClick }}" column-num="{{5}}" list="{{list55}}" multi-line="{{true}}"></grid>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
  data: {
    list5: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
    ],
    list55: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '6标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
    ],
    list56: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },

    ],
  },
  onItemClick(ev) {
    let message = String(ev.detail.index)
    prompt.showDialog({
      title: '',
      message: message,
      buttons:[{text:'确定', color: '#1677ff'}]
    })
  },
};

```