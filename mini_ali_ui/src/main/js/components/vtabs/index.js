import fmtUnit from '../../common/_util/fmtunit';

export default {
  data: {
    tabTop: 0,
    wrapScrollTop: { _v: 0 },
    besideRadius: fmtUnit('8px'),
    currentBefore: 0,
    currentAfter: 0,
    current: 0,
    anchorMap: {},
    indexMap: {},
    indexTop: {},
    wrapHeight: 0,
    scrollWrapHeight: 0,
  },
  props: {
    activeTab: {default:0,},
    className: {default:'',},
    tabs: {default:[],},
    animated: {default:false,},
    swipeable: {default:true,},
    tabBarActiveTextColor: {default:'#1677FF',},
    tabBarInactiveTextColor: {default:'#333333',},
    tabBarActiveBgColor: {default:'#ffffff',},
    tabBarInactiveBgColor: {default:'#f5f5f5',},
    tabBarlineColor: {default:'#1677FF',},
    sameFontSize: {default:true,},
    tabBarlineShow: {default:true,},
    bindTabClick: {default:() => { },},
    bindScrollBar: {default:() => { },},
    bindChange: {default:() => { },},
  },
  async onPageShow() {
    console.info('vtabs onPageShow');
    this.isScrolling = false;
    this.onlyChangeTab = false;
    this.timerId = null;
    await this.calcHeight();
    this.wrapScrollTop ={ _v: this.anchorMap[this.tabs[this.activeTab].anchor] };
  },
  onInit() {
    console.info('vtabs onInit');
    this.$app.vtabContents = this.$app.vtabContents || {};
    //    this.$watch('activeTab', 'onActiveTabChange');
    this.$watch('tabs', 'onTabsChange');
    this.$watch('wrapScrollTop','onWrapScrollTopChange');
  },
  onTabsChange(newV, oldV) {
    if (newV.length != oldV.length) {
      this.calcHeight();
    }
  },
  onWrapScrollTopChange() {
    var amVtabsSliders = this.$element('am-vtabs-slides');
    var dy = this.wrapScrollTop._v - amVtabsSliders.getScrollOffset().y;
    amVtabsSliders.scrollBy({
      dy: dy,
      smooth:this.animated,
    });
  },
  onPageHide() {
    if (this.timerId) {
      clearTimeout(this.timerId);
      this.timerId = null;
    }
  },
  async onWrapTouch() {
    await this.calcHeight();
  },
  async calcHeight() {
    const activeTab = this.activeTab;
    this.anchorMap = {};
    this.indexMap = {};
    this.indexTop = {};
    this.wrapHeight = 0;
    this.scrollWrapHeight = 0;

    this.currentBefore = activeTab - 1;
    this.currentAfter = activeTab + 1;

    await new Promise((resolve) => {
      let height = this.$element('am-vtabs-slides'). getBoundingClientRect().height;
      this.wrapHeight = height;
      resolve();
    });
    const tabs = this.tabs || [];
    var self = this;
    const rects = await new Promise((resolve) => {
      const res = tabs.map(tab => self.$app.vtabContents[`am-vtab-slide-${tab.anchor}`].getBoundingClientRect());
      if (res && res[0]) {
        resolve(res.sort((a, b) => a.top - b.top));
      }
    });

    let prevHeight = 0;
    for (let i = 0; i < tabs.length; i += 1) {
      const { height } = rects[i];
      this.anchorMap[tabs[i].anchor] = prevHeight;
      this.indexMap[i] = height;

      if (i === 0) {
        this.indexTop[0] = 0;
      } else {
        this.indexTop[i] = this.indexTop[i - 1] + Math.floor((rects[i - 1])?.height);
      }

      prevHeight += Math.floor(height);
      this.scrollWrapHeight = prevHeight;
    }
  },
  handleTabClick(e) {
    const { index } = e.target.dataSet;
    if (!this.isScrolling || !this.swipeable || this.onlyChangeTab) {
      if (this.activeTab !== index && this.bindTabClick) {
        this.bindTabClick(index);
      }
      this.wrapScrollTop = { _v: this.indexTop[index] };
      this.moveScrollBar(index);
    }
  },
  moveScrollBar(current) {
    let tabTop;
    // tabTop �������Ʋ�� tab �� scroll-view ����λ��
    if (current < 6) {
      tabTop = 0;
    } else {
      tabTop = (current - 5) * 55;
    }
    // tab-content ����ʱ���Բ�� tab ��Ӱ��
    if (this.activeTab !== current) {
      if (this.bindChange) {
        this.bindChange(current);
      } else {
        if (this.bindScrollBar) {
          this.bindScrollBar(current);
        }
      }
    }
    this.tabTop = tabTop;
    this.$element('am-vtabs-tabs').scrollTo({
      position: this.tabTop,
    });
    this.current = current;
    this.currentBefore = current - 1;
    this.currentAfter = current + 1;
  },
  onScroll() {
    var amVtabsSlides = this.$element('am-vtabs-slides');
    const scrollTop = amVtabsSlides.getScrollOffset().y;
    const keys = Object.keys(this.anchorMap);

    if (this.timerId) {
      clearTimeout(this.timerId);
      this.timerId = null;
    }

    this.timerId = setTimeout(() => {
      this.isScrolling = false;
    }, 300);

    const anchorLength = keys.length;
    for (let i = 0; i < anchorLength; i++) {
      if (i === anchorLength - 1) {
        // ��������һ��ֻ������scrollTop���ڵ�ǰvtab-content�ĸ߶�
        if (scrollTop >= this.anchorMap[keys[i]]) {
          this.moveScrollBar(i);
          break;
        }
      }
      if (scrollTop >= this.anchorMap[keys[i]] &&
      scrollTop < this.anchorMap[keys[i + 1]]) {
        // ���ÿ��vtab-content�߶�С��scroll-view�߶ȣ�����ײ���Ͳ���Ҫ����scrollTop��ȥ�ж�����ѡ����
        if (scrollTop + this.wrapHeight < this.scrollWrapHeight) {
          this.moveScrollBar(i);
        }
        break;
      }
    }
  },
  onWrapTouchMove() {
    if (this.swipeable) {
      this.isScrolling = true;
      this.onlyChangeTab = true;
    }
  },
  grabTouchMove() {

  }
};
