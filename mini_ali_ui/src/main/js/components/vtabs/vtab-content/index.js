export default{
    props: {
        anchor: {
            default:'',
        }
    },
    onAttached() {
        console.info('vtab-content onAttached');
        var element = this.$element("am-vtab-slide-"+this.anchor);
        this.$app.vtabContents["am-vtab-slide-"+this.anchor] = element;
    },
    onDetached() {
        console.info('vtab-content onDetached');
        delete this.$app.vtabContents["am-vtab-slide-"+this.anchor];
    },
    onPageShow() {
        console.info('vtab-content onPageShow');
    },
    onInit() {
        console.info('vtab-content onInit');
    }
};
