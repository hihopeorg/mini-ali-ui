# vtabs 纵向选项卡

用于让用户在不同的视图中进行切换。

## 截图
![vtabs 纵向选项卡](../../../../../../gifs/vtabs.gif)

## 属性介绍
vtabs 纵向选项卡包含了 `<vtabs>` 和 `<vtab-content>` 两部分。

### vtabs

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| active-tab | Number | 0 | - | 当前激活 Tab 索引 | - | - |
| tabs | Array | - | - | tab 数据 | - | true |
| animated | Boolean | false | - | 是否开启动画 | - | - |
| swipeable | Boolean | true | - | 是否可滑动切换 | - | - |
| tab-bar-active-text-color | String | #1677FF | - | tabBar 激活状态文字颜色 | - | - |
| tab-bar-active-bg-color | String | #ffffff | - | tabBar 激活状态背景色 | - | - |
| tab-bar-inactive-text-color | String | #333333 | - | tabBar 非激活状态文字颜色 | - | - |
| tab-bar-inactive-bg-color | String | #f5f5f5 | - | tabBar 非激活状态背景色 | - | - |
| tabBarlineColor | String | #1677FF | - | tabBar 激活状态边线 | - | - |
| on-tab-click | EventHandle | (index: Number) => void | - | tab 被点击时的回调 | - | - |
| on-change | EventHandle | (index: Number) => void | - | vtab-content变化时触发 | - | - |
| same-font-size | Boolean | true | - | tab 选项卡的文字是否保持相同，如为 false，激活态的文字会大一点 | - | - |
| tab-barline-show| Boolean | true | - | tab 选项卡激活态侧边竖线是否显示 | - | - |
| bind-tab-first-show | EventHandle | (index: Number, anchor: String) => {} | - | - | - |

### vtab-content

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| anchor | String | - | - | 列表唯一锚点值 | - | true |

## Bug & Tip
* `tabs` 的数组中需要包含 **vtab-content** 中的 `anchor`；
* `tabs` 数组的数据格式：`[{ title: '', anchor: '', number: '99+',},]`；

## 代码示例

```xml
<element name="vtabs" src="../../../../../../../mini_ali_ui/src/main/js/components/vtabs/index"></element>
<element name="vtab-content" src="../../../../../../../mini_ali_ui/src/main/js/components/vtabs/vtab-content/index"></element>
<element name="am-button" src="../../../../../../../mini_ali_ui/src/main/js/components/button/index"></element>
<div style="width: 100%;height: 100%;">
<vtabs tabs="{{tabs}}"
  bind-tab-click="{{handleChange}}"
  bind-change="{{onChange}}"
  active-tab="{{activeTab}}"
  same-font-size="{{false}}"
  swipeable="{{swipeable}}"
  tab-barline-show="{{false}}">
  <block for="{{tabs}}">
    <block if="{{$item.anchor === 'b'}}">
      <vtab-content anchor="{{$item.anchor}}">
        <div onclick="changeHeight"
          style="height: {{tabItemHeight * 7.8}};width:100%;background-color: #ccc;">
          <text style="font-size:16px">content of {{$item.title}}</text>
        </div>
      </vtab-content>
    </block>
    <block elif="{{$item.anchor === 'a'}}">
      <vtab-content anchor="{{$item.anchor}}">
        <div onclick="changeHeight"
          style="height: 780px;">
          <am-button size="default" type="primary" @on-tap="changeSwipeable" style="width: 100%;">{{swipeable? 'swipeable: true' : ' swipeable:false'}}</am-button>
        </div>
      </vtab-content>
    </block>
    <block else>
      <vtab-content anchor="{{$item.anchor}}">
        <div style="height: 780px;">
          <text style="font-size:16px">content of {{$item.title}}</text>
        </div>
      </vtab-content>
    </block>
  </block>
</vtabs>
</div>
```

```javascript
export default{
  data: {
    activeTab: 2,
    swipeable: true,
    tabs: [
      { title: '选项二', anchor: 'a', number: '6' },
      { title: '选项', anchor: 'b', number: '66' },
      { title: '不超过五字', anchor: 'c', number: '99+' },
      { title: '选项四选项四选项四选项四', anchor: 'd' },
      { title: '选项五', anchor: 'e' },
      { title: '选项六', anchor: 'f' },
    ],
    tabItemHeight: 50,
  },
  handleChange(index) {
    this.activeTab = index;
  },
  onChange(index) {
    this.activeTab = index;
  },
  changeHeight() {
    this.tabItemHeight += 5;
  },
  changeSwipeable() {
    this.swipeable = ! this.swipeable;
  },
};
```
