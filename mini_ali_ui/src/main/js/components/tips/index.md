# Tips 小提示

小提示。分`tips-dialog`和`tips-plain`两种类型。


## tips-dialog

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| --- | --- | --- | --- | --- | --- | --- |
| class-name | String |  |  | 自定义class |  | false |
| showed | Boolean | true |  | 控制组件是否展示 |  | false |
| type | String | dialog |  | `dialog`表示对话框的样式类型，`rectangle`表示矩形的样式类型。 |  | false |
| @on-close-tap | () => void |  |  | 当`type`值为`rectangle`时，组件点击关闭icon的回调 |  | false |
| icon-url | String |  |  | 展示icon的url地址 |  | false |
| arrow-position | String | bottom-left | `bottom-left`, `bottom-center`, `bottom-right`, `top-left`, `top-center`, `top-right`, `left`, `right` | 控制 tips 中的箭头位置 |  | false |

### slots

| slotName | 说明 |
| ---- | ---- |
| content | 用于渲染提示的正文内容 |
| operation | 用于渲染右侧操作区域 |

## tips-plain

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| --- | --- | --- | --- | --- | --- | --- |
| class-name | String |  |  | 自定义class |  | false |
| time | Number | 5000(ms) |  | 自动关闭时间(单位毫秒) |  | false |
| @on-close | () => void |  |  | 回调并关闭提示框 |  | false |
| @on-time-out | () => void |  |  | 倒计时结束时关闭回调 | | false |


## 示例

### tips-dialog

```hml
<div style="display: flex;flex-direction: column;">
    <tips-dialog icon-url="{{useIcon ? 'https://gw.alipayobjects.com/zos/rmsportal/AzRAgQXlnNbEwQRvEwiu.png' : ''}}"
                 icon-size="36"
                 style="width: 94%;margin-left: 3%;"
                 type="rectangle"
                 class-name="rectangle"
                 @on-close-tap="onCloseTap"
                 showed="{{showRectangle}}"
                 arrow-position="{{textvalue}}">
        <text class="content"
              slot="content">
            箭头有8个方向。字数字数字数字数字两行
        </text>
        <button if="{{useButton}}"
                   slot="operation"
                   style="width: 115px;height:26px;background-color: white;text-color: #333;font-size:14px;border-radius: 4px;"
                   @on-tap="onRectangleTap">立即添加</button>
    </tips-dialog>
    <div style="padding: 20px 10px;flex-direction: column;">
        <div class="row">
            <text class="row-title">箭头位置</text>
            <text class="row-extra">当前选择：</text>
            <picker id="picker_text" type="text" value="{{textvalue}}" selected="{{textselect}}" range="{{arrowPositions}}" onchange="textonchange"
                    oncancel="textoncancel" class="pickertext"></picker>
            <am-icon type="right"></am-icon>
        </div>
        <am-list>
            <am-list-item>
                <text>左侧图标</text>
                <switch class="am-list-item-switch" checked="{{useIcon}}" @change="setInfo1"></switch>
            </am-list-item>
            <am-list-item>
                <text>右侧按钮</text>
                <switch class="am-list-item-switch" checked="{{useButton}}" @change="setInfo2"></switch>
            </am-list-item>
        </am-list>
    </div>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
    data: {
        showRectangle: true,
        showDialog: true,
        arrowPositions: [
            'top-left',
            'top-center',
            'top-right',
            'left',
            'right',
            'bottom-left',
            'bottom-center',
            'bottom-right',
        ],
        useIcon:false,
        arrowPosIndex: 3,
        useButton: true,
        textvalue:'top-left',
    },
    onCloseTap() {
        this.showRectangle= false;
    },
    onRectangleTap() {
        prompt.showDialog({title:'do something'});
    },
    onDialogTap() {
        this.showDialog=false;
    },
    textonchange(e) {
        this.textvalue = e.newValue;
    },

    setInfo1(e) {
        if(e.checked){
            this.useIcon = true;
        }else{
            this.useIcon = false;
        }
    },
    setInfo2(e) {
        if(e.checked){
            this.useButton = true;
        }else{
            this.useButton = false;
        }
    },
};
```

```css
.rectangle {
  padding: 0 14px;
  margin-top: 20px;
}

.dialog {
  position: fixed;
  bottom: 18px;
}

.content {
  color: #fff;
}

.opt-button {
  width: 51px;
  height: 27px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  font-size: 12px;
  border: 1px solid #68BAF7;
}

.add-home {
  width: 72px;
  height: 27px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #56ADEB;
  color: #fff;
  font-size: 14px;
}

.am-tips-dialog .am-button {
  border: none;
}

.row {
  display: flex;
  align-items: center;
  padding: 0 10px;
  background-color: white;
}

.row-title {
  flex: 1;
  padding-top: 28px;
  padding-bottom: 28px;
  font-size: 18px;
  color: #000;
}

.row-extra {
  font-size: 18px;
  color: #888;
  margin-right: 12px;
}
.am-list-item-switch{
  margin-left: 60%;
}
```

### tips-plain

```hml
<div class="container">
    <tips-plain @on-close="onClose" @on-time-out="onTimeOut" time="{{time}}"><text>{{content}}</text></tips-plain>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
    data: {
        content: '我知道了(2秒后消失)',
        time: 2000,
    },
    onClose() {
        prompt.showDialog({title:'主动关闭 tips'});
    },
    onTimeOut() {
        prompt.showDialog({title:'时间到了，关闭 tips'});
    },
};
```