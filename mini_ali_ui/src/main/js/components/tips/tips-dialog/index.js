import fmtEvent from '../../../common/_util/fmtevent';
import fmtUnit from '../../../common/_util/fmtunit';

export default({
  props: {
    iconUrl:{default:''},
    showed: {default:true},
    className:{default:''},
    type: {default:'dialog'},
    iconSize: {default:20},
    arrowPosition: {default:'bottom-left'},
  },
  data: {
    arrowColor: 'rgba(40, 40, 40, 1)',
    iconSizeClose: fmtUnit(18),
  },
  onCloseTap(e) {

    const { onCloseTap } = this;
    const event = fmtEvent(this.props, e,this);

    if (onCloseTap) {
      this.$emit("onCloseTap",event);
    }
  },
});
