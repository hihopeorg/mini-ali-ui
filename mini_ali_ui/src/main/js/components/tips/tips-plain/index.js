import fmtEvent from '../../../common/_util/fmtevent';

export default{
  data: {
    showed: true,
    timer:'',
  },
  props: {
    className: {default:''},
    time: {default:5000},
    showed:{default:true},
  },
  onReady() {
    const { time } = this;
    this.timer = setTimeout(() => {
      this.showed= false;
      this.$emit("onTimeOut");
    }, time);
  },
  onDestroy() {
    clearTimeout(this.timer);
  },
  onClose(e) {
    const event = fmtEvent(this.props, e ,this);
    this.showed= false;
    clearTimeout(this.timer);
    this.$emit("onClose",event);
  },
  onTimeOut(e) {
    const event = fmtEvent(this.props, e ,this);
    this.$emit("onTimeOut",event);
  },
};
