# Popup 弹出菜单

弹出菜单。


## 属性介绍


| 属性名 | 描述 | 类型 | 默认值 | 必选 |
| ---- | ---- | ---- | ---- | ---- |
| class-name | 自定义class | String | | 暂不支持 |
| showed | 是否显示菜单 | Boolean | false | - |
| animation | 是否开启动画 | Boolean | true | - |
| mask | 是否显示mask，不显示时点击外部不会触发onClose | Boolean| true | true |
| position | 控制从什么方向弹出菜单，bottom表示底部，left表示左侧，top表示顶部，right表示右侧 | String | bottom | true |
| disableScroll | 不支持||||
| zIndex | 不支持||||

### slots

可以在popup组件中定义要展示部分，具体可参看下面代码示例。

## 代码示例

```hml
<div class="container">
  <div class="btn-container">
    <am-button @on-tap="onTopBtnTap">上</am-button>
  </div>
  <am-popup showed="{{showTop}}" position="top" @on-close="onPopupClose">
    <div slot="tip" class="box top"><text>hello world</text></div>
  </am-popup>
</div>
```

```javascript
export default{
    data: {
        showTop: false,
    },
    onTopBtnTap() {
        this.showTop=true;
    },
    onPopupClose() {
        this.showLeft= false;
        this.showRight= false;
        this.showTop= false;
        this.showBottom= false;
    },
};
```
