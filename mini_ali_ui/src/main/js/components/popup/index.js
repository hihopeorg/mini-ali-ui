export default{
  props: {
    className: {default:''},
    showed: {default:false},
    position:{default:'bottom'},
    mask: {default:true},
    animation: {default:true},
    disableScroll: {default:true},
  },
  onMaskTap() {
    const { animation } = this;
    if (animation) {
      this.$emit("onClose");
    } else {
      setTimeout(() => {
        this.$emit("onClose");
      }, 200);
    }
  },
};
