## Modal 对话框

对话框，当应用中需要比较明显的对用户当前的操作行为进行警示或提醒时，可以使用对话框。用户需要针对对话框进行操作后方可结束。


## 截图
![model 弹窗](../../../../../../gifs/model.gif)

## 属性介绍
| 属性 | 类型 | 默认值 | 可选值 | 描述 | 最低版本 | 必填 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| class-name | String | - | - | 自定义class | 暂不支持 | - |
| showed | Boolean | false | true, false | 是否展示`modal` | - | - |
| show-close | Boolean | false | true, false | 是否渲染`关闭` | - | - |
| mask | Boolean | true | true, false | 是否展示蒙层 | - | - |
| close-type | String | 0 | 0, 1 | 关闭图表类型 0：灰色图标 1：白色图标 | - | - |
| @on-modal-tap | EventHandle | () => void | - | 选择区间时的回调 | - | - |
| @on-modal-close | EventHandle | () => void | - | 点击`关闭`的回调, `showClose`为false时无需设置 | - | - |
| top-image | String | - | - | 顶部图片 | - | - |
| top-image-size | String | md | lg, md, sm | 顶部图片规则 | - | - |
| buttons | Array\<Object\> | md | - | 底部自定义多按钮, 详情见buttons配置 | - | - |
| onButtonTap | EventHandle | (e: Object) => void | - | 点击`buttons`部分的回调 | - | - |
| buttons-layout | String | horizontal | horizontal, vertical | 设置`buttons`的对齐方式 | - | - |
| advice | Boolean | false | true, false | 是否是运营类弹窗 | - | - |
| @on-mask-tap | EventHandle | () => void | - | 点击遮罩层时的回调  | - ||

## buttons
提供按钮组配置，每一项表示一个按钮，每一项的属性为
| 属性名 | 描述 | 类型 | 默认值 |
| ---- | ---- | ---- | ---- |
| text| 按钮的文本 | String| |
| ext-class | 按钮自定义Class，可用户定制按钮样式 | String | 暂不支持 |

## slots

| slotName | 说明 |
| ---- | ---- |
| header | 可选，modal头部 |
| footer | 可选，modal尾部 |


## 代码示例

```

```hml
<div>
   	<modal
   		showed="{{modalOpened}}"
        @on-mask-tap="onMaskClick"
   		@on-modal-tap="onModalClick"
   		@on-modal-close="onModalClose"
   		header="header"
   		footer="footer"
   		top-image="https://gw.alipayobjects.com/zos/rmsportal/yFeFExbGpDxvDYnKHcrs.png"
   	>
   		<text slot="header" style="font-size: 24px;">带图弹窗</text>
   		<text slot="between" style="font-size: 18px;">说明当前状态、提示用户解决方案，最好不要超过两行。</text>
   		<text slot="footer">我知道了</text>
   	</modal>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
  data: {
    modalOpened: false,
  },
  /* 通用modal */
  openModal() {
    this.modalOpened = true;
  },
  onMaskClick(e){
  
  },
  onModalClick(e){
  
  },
  onModalClose(e){
  
  }, 
};

```
