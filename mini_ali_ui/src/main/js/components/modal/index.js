import fmtUnit from '../../common/_util/fmtunit';

export default{
  data: {
    buttonsLayout: '',
    adviceClose: fmtUnit(26),
    normalClose: fmtUnit(18),
  },
  props: {
    className: {default:''},
    topImageSize: {default:'md'},
    showClose:{default:false},
    closeType: {default:'0'},
    mask: {default:true},
    buttonsLayout: {default:'horizontal'},
    disableScroll:  {default:true},
    maskClick: {default:false},
    showed:{default:false},
    topImage:{default:''},
    advice:{default:false},
    header:{default:''},
    footer:{default:''},
    buttons:{default:[]}
  },
  onReady() {
    const { buttons, buttonsLayout } = this;
    // button数目大于 2 个，则强制使用竖排结构
    if (buttons && buttons.length > 2) {
      this.buttonsLayout= 'vertical';
    } else {
      this.buttonsLayout= buttonsLayout;
    }
  },
    // footer点击
    onModalClick() {
      this.$emit("onModalTap");
    },
    // buttons点击
    onButtonClick(e) {
      this.$emit("onButtonTap",e);
    },
    // 关闭按钮点击
    onModalClose() {
      this.$emit("onModalClose");
    },
    // mask 遮罩层点击
    onMaskTap() {
      this.$emit("onMaskTap");
    },
};
