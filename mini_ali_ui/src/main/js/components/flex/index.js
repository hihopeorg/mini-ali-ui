export default{
  props: {
    className: {
      default:''
    },
    direction: {
      default:'row'
    },
    wrap:  {
      default:'nowrap'
    },
    justify:  {
      default:'start'
    },
    align:  {
      default:'center'
    },
    alignContent:  {
      default:'stretch'
    },
  },
  onInit(){
    if(this.align == 'start'){
      this.align = 'flex-start'
    }else if(this.align=='end'){
      this.align = 'flex-end'
    }

    if(this.justify == 'start'){
      this.justify = 'flex-start'
    } else if(this.justify =='end'){
      this.justify = 'flex-end'
    } else if(this.justify =='between'){
      this.justify = 'space-between'
    } else if(this.justify =='around'){
      this.justify = 'space-around'
    }

    if(this.alignContent == 'start'){
      this.alignContent = 'flex-start'
    } else if(this.alignContent =='end'){
      this.alignContent = 'flex-end'
    } else if(this.alignContent =='between'){
      this.alignContent = 'space-between'
    } else if(this.alignContent =='around'){
      this.alignContent = 'space-around'
    }
  }

};