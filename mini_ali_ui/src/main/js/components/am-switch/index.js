import fmtEvent from '../../common/_util/fmtevent';

export default {
    props: {
        color: {
            default:'#1677ff',
        },
        checked: {
            default: false,
        },
        disabled: {
            default: false,
        },
        id: {
            default: '',
        },
        name: {
            default: '',
        },
        controlled: {
            default: false,
        },
        dataset: {
            default: {},
        }
    },
    data: {
        checkedCls: '',
        currentChecked: false,
        userTrigger: false,
    },
    onInit() {
        this.currentChecked = this.checked;
        this.$watch('checked','checkedChange');
    },
    onChange(e) {
        this.refreshAndNotify(e);
    },
    checkedChange(newV) {
        this.refreshAndNotify({checked:newV,
        target: {}});
    },
    refreshAndNotify(e) {
        this.userTrigger = true;
        this.currentChecked = e.checked;
        e.value = e.checked;
        const event = fmtEvent(this.props, e, this);
        this.$emit('onChange',
            event
        );
    }
};
