# am-switch 开关

开关。具体用法和小程序框架中 switch 保持一致，在 switch 基础上做了样式的封装。


## 截图
<img src="../../../../../../gifs/am-switch.gif" />

## 属性介绍
| 属性名 | 类型 | 默认值 | 可选值 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| name | String | - | - | 组件名字，用于表单提交获取数据 | - |
| checked | Boolean | false | false, true | 当前是否选中，可用来设置默认选中 | - |
| disabled | Boolean | false | false, true | 是否禁用 | - |
| @on-change | (e: Object) => void | - | - | change 事件触发的回调函数 | - |
| color | String | - | 同 CSS 色值 | 组件颜色 | - |
| controlled | Boolean | false | false, true| 是否为受控组件，为 true 时，checked 会完全受 setData 控制	| - |

## tips
原库在不同手机系统上有不同表现，ohos版本按圆形风格移植。

## 示例

```xml
<element name="am-switch" src="../../../../../../../mini_ali_ui/src/main/js/components/am-switch/index"></element>

<div class="page">
  <text class="page-description">开关</text>
  <div class="page-section">
    <div class="page-section-demo switch-list">
      <div class="switch-item">
        <am-switch checked="true" @on-change="switch1Change"></am-switch>
      </div>
      <div class="switch-item">
        <am-switch color="red" checked="true" ></am-switch>
      </div>
    </div>
  </div>
</div>
```
```javascript
export default {
    switch1Change(e) {
        console.log('switch1 发生 change 事件，携带值为' + e._detail.checked);
    },
};
```