import fmtEvent from '../../../common/_util/fmtevent';
import fmtUnit from '../../../common/_util/fmtunit';
export default{
    props: {
        classname: {default:''},
        iconURL: {default:''},
        icontype: {default:''}
    },
    data: {
        iconSize: fmtUnit(22)
    },
    onInit(){
    },
    onItemClick(e) {
        var event = fmtEvent(this.props, e, this);
        this.$emit("onItemTap", event);
    }
};