export default{
    props: {
        showed: {default:false},
        className: {default:''},
        showMask: {default:true},
        position: {default:'bottomRight'},
        fixMaskFull: {default:false},
    },
    onMaskClick() {
        this.$emit("onMaskClick")
    },
};