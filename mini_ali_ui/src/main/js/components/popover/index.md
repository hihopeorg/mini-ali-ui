# Popover 气泡

气泡。

## 预览
![popover 气泡](../../../../../../gifs/popover.gif)


## popover 属性

| 属性名    | 类型    | 默认值      | 可选项                                                       | 描述           | 最低版本 | 必填 |
| --------- | ------- | ----------- | ------------------------------------------------------------ | -------------- | -------- | ---- |
| class-name | String  |             | -                                                            | 最外层覆盖样式 | -        | -    |
| showed      | Boolean | false       | -                                                            | 气泡是否展示   | -        | true |
| showmask  | Boolean | true        | -                                                            | 蒙层是否展示   | -        | -    |
| position  | String  | bottomRight | `top`<br/>`topRight`<br/>`topLeft`<br/> `bottom`<br/>`bottomLeft`<br/>`bottomRight`<br/>`right`<br/>`rightTop`<br/>`rightBottom`<br/>`left`<br/>`leftBottom`<br/> `leftTop` | 气泡位置       | -        | -    |
| fix-mask-full | Boolean | false | - | 用以解决遮罩层受到 `transform` 影响而显示不全的问题 | | - |

## popover-item 属性

| 属性名      | 类型       | 默认值 | 可选项 | 描述         | 最低版本 | 必填 |
| ----------- | ---------- | ------ | ------ | ------------ | -------- | ---- |
| class-name   | String     | -      | -      | 单项样式     | -        | -    |
| onItemClick | () => void | -      | -      | 单项点击事件 | -        | -    |
| icontype | String | - | 参考 icon 组件 | 所有的 type 值均来自 icon 组件 | - | - |
| iconurl | String | - | - |


## 示例


### hml
```hml
<div class="container">
  <div class="demo-popover-test-btns">
    <am-button value="下个位置" class="demo-popover-test-btn" @on-tap="onNextPositionTap"></am-button>
    <am-button value="蒙层{{showMask ? '隐藏' : '显示'}}" class="demo-popover-test-btn" @on-tap="onMaskChangeTap"></am-button>
    <am-button value="显示/隐藏图标" class="demo-popover-test-btn" @on-tap="onIconChangeTap"></am-button>
  </div>
  <div class="demo-popover">
    <popover
            class="popover-div"
            position="{{position}}"
            showed="{{showed}}"
            showmask="{{showMask}}"
            @on-mask="onMaskClick"
            >
      <div class="demo-popover-btn" grab:touchstart.capture="onShowPopoverTap"><text class="btn-text">点击{{showed ? '隐藏' : '显示'}}</text></div>
      <div slot="items" class="demo-popover-item">
        <popover-item shrink="{{shrink}}" @on-item-tap="itemTap1" iconurl="{{showIcon ? 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ' : ''}}" data-direction="{{position}}" dataindex="{{1}}">
          <text style="font-size: {{shrink?'10':'12'}}px;">{{position}}</text>
        </popover-item>
        <popover-item shrink="{{shrink}}" @on-item-tap="itemTap2" icontype="{{showIcon ? 'qr' : ''}}" data-direction="{{position}}" dataindex="{{2}}">
          <text style="font-size: {{shrink?'10':'16'}}px;">line2</text>
        </popover-item>
      </div>
    </popover>
  </div>
</div>
```

### js
```javascript
const position = ['top', 'topRight', 'rightTop', 'right', 'rightBottom', 'bottomRight', 'bottom', 'bottomLeft', 'leftBottom', 'left', 'leftTop', 'topLeft'];
import prompt from '@system.prompt';
export default{
  data: {
    position: position[0],
    showed:true,
    showMask: false,
    showIcon: true,
    shrink:false,
  },
  onShowPopoverTap() {
    this.showed=!this.showed;
  },
  relieve(){
    this.showed=false;
  },
  onNextPositionTap() {
    let index = position.indexOf(this.position);
    index = index >= position.length - 1 ? 0 : index + 1;
    this.showed=true;
    this.position=position[index];
    if(index ==8 || index ==9 || index ==10 ){
      this.shrink = true;
    }else{
      this.shrink = false;
    }
  },
  onMaskChangeTap() {
    this.showMask=!this.showMask;
  },
  onIconChangeTap() {
    this.showIcon=!this.showIcon;
  },
  onMaskClick() {
    this.showed=false;
  },
  itemTap1(e){
    prompt.showToast(
      {message:JSON.stringify(`点击_${e.detail.currentTarget.dataset.index}`),
      }
    );
  },
  closeTip(){
    this.$element('simpledialog').close();
  },
  itemTap2(e) {
    prompt.showToast(
      {message:JSON.stringify(`点击_${e.detail.currentTarget.dataset.index}`),
      }
    );
  },
};
```

### css
```css
.demo-popover {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
}
.popover-div{
  position: absolute;
  top: 150px;
  left: 140px;
}
.demo-popover-btn {
  width: 100px;
  height: 100px;
  background-color: #fff;
  border: 1px solid #dddddd;
  border-radius: 2px;
  position: relative;
}
.demo-popover-test-btns {
  display: flex;
  justify-content: space-around;
  position: absolute;
  top: 400px;
}
.demo-popover-test-btn {
  width: 45%;
}
.container{
  display: flex;
  flex-direction: column;
  background-color: #e0dede;
  height: 100%;
  position: relative;
}
.btn-text{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  font-size: 18px;
}
.demo-popover-item{
  position: relative;
  flex-direction: column;
  z-index: 200;
}
.dialog-main{
  height: 150px;
  width: 250px;
  margin-bottom: 350px;
}
.dialog-div{
  display: flex;
  flex-direction: column;
}
.txt{
  width: 100%;
  text-align: center;
  font-size: 18px;
}
.btn-txt{
  border-top: 1px solid #999696;
  padding-top: 20px;
  margin-top: 20px;
  width: 100%;
  color: blue;
  font-size: 18px;
  text-align: center;
}
```