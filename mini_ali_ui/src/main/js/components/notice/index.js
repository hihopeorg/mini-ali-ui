
//const noop = () => {
//};
//const canIUseTransitionEnd = my.canIUse('view.onTransitionEnd');

export default {
    props: {
        className: {
            default: ''
        },
        mode: {
            default: ''
        }, // closable,link
        action: {
            default: ''
        }, // 文本按钮
        actionLeft: {
            default: ''
        }, // 文本按钮
        isShow: {
            default: true
        }, // 是否显示
        enableMarquee: {
            default: false
        }, // 是否开启marquee
        marqueeProps: {
            loop: false,
            leading: 500,
            trailing: 800,
            fps: 40,
        },
        capsuleItem: {
            default: []
        },
        showIcon: {
            default: true
        },
        type: {
            default: 'normal'
        }, // 通告栏类型： normal/error/active
        capsule: {
            default: false
        }, // 是否为胶囊型通告栏
        transparent: {
            default: false
        },
        title: {
            default: ''
        },
        iconColor: {
            default: '#ff6010'
        },
        loop: {
            default: -1
        }
    },
    data: {
        animatedWidth: 0,
        overflowWidth: 0,
        duration: 0,
        marqueeStyle: '',
//        canIUseTransitionEnd,
        showShadow: true,
        //    _i18nDetail: i18n.detail,
    },
    bounce() {
        if(this.loop === 1) {
            this.enableMarquee = false
        }
    },
    onAttached() {
        if (this.enableMarquee) {
//            if (!canIUseTransitionEnd) {
//                this._measureText();
//                this._startAnimation();
//            } else {
//                this._measureText(this.startMarquee.bind(this));
//            }
        }
        if (this.type === 'active' && this.transparent) {
            this.showShadow = false;
        } else {
            this.showShadow = true;
        }
    },

    onPageShow() {
        if (this.type === 'active' && this.transparent && this.showShadow === true) {
            this.showShadow = false;
        } else if (this.showShadow === false) {
            this.showShadow = true;
        }
        // 这里更新处理的原因是防止notice内容在动画过程中发生改变。
//        if (!canIUseTransitionEnd) {
//            this._measureText();
//        }
//        if (this.enableMarquee && !this._marqueeTimer){// && !canIUseTransitionEnd) {
//            this._measureText();
//            this._startAnimation();
//        } else {
//            // 当通过脚本切换 show 的值时（true or false），导致跑马灯动画效果失效的 bug 处理
//            if (!this.show && this.marqueeStyle !== '') {
//                this.marqueeStyle = ''
//            }
//            this._measureText(this.startMarquee.bind(this));
//        }
    },

    onDetached() {
        if (this._marqueeTimer) {
            clearTimeout(this._marqueeTimer);
            this._marqueeTimer = null;
        }
    },
    resetMarquee() {
        const marqueeStyle = 'transform: translateX(0px); transition: 0s all linear;';
        this.marqueeStyle = marqueeStyle;
    },

    startMarquee() {
        const { leading = 500 } = this.marqueeProps;
        const { duration, overflowWidth } = this;
        const marqueeStyle = `transform: translateX(${-overflowWidth}px); transition: ${duration}s all linear ${typeof leading === 'number' ? `${leading / 1000}s` : '0s'};`;
        if (this.marqueeStyle !== marqueeStyle) {
            this.marqueeStyle = marqueeStyle;
        }
    },

    onTransitionEnd() {
        const { loop = false, trailing = 800 } = this.marqueeProps;
        if (loop) {
            setTimeout(() => {
                this.resetMarquee();
                this._measureText(this.startMarquee.bind(this));
            }, typeof trailing === 'number' ? trailing : 0);
        }
    },

    _measureText(/*callback = noop*/) {
        //const { fps = 40 } = this.marqueeProps;
        // 计算文本所占据的宽度，计算需要滚动的宽度
        setTimeout(() => {
            //        my.createSelectorQuery()
            //          .select(`.am-notice-marquee-${this.$id}`)
            //          .boundingClientRect()
            //          .select(`.am-notice-content-${this.$id}`)
            //          .boundingClientRect()
            //          .exec((ret) => {
            //            const overflowWidth = (ret && ret[0] && ret[1] && (ret[0].width - ret[1].width)) || 0;
            //            if (overflowWidth > 0) {
            //              this.overflowWidth = overflowWidth;
            //              this.duration = overflowWidth / fps;
            //              callback();
            //            }
            //          });
        }, 0);
    },

    _startAnimation() {
        if (this._marqueeTimer) {
            clearTimeout(this._marqueeTimer);
        }

        const {
            loop = false,
            leading = 500,
            trailing = 800,
            fps = 40,
        } = this.marqueeProps;
        const TIMEOUT = 1 / fps * 1000;
        const isLeading = this.animatedWidth === 0;
        const timeout = isLeading ? leading : TIMEOUT;

        const animate = () => {
            const { overflowWidth } = this;
            let animatedWidth = this.animatedWidth + 1;
            const isRoundOver = animatedWidth > overflowWidth;

            if (isRoundOver) {
                if (loop) {
                    animatedWidth = 0;
                } else {
                    return;
                }
            }

            if (isRoundOver && trailing) {
                this._marqueeTimer = setTimeout(() => {
                    this.animatedWidth = animatedWidth;

                    this._marqueeTimer = setTimeout(animate, TIMEOUT);
                }, trailing);
            } else {
                this.animatedWidth = animatedWidth;

                this._marqueeTimer = setTimeout(animate, TIMEOUT);
            }
        };

        if (this.overflowWidth !== 0) {
            this._marqueeTimer = setTimeout(animate, timeout);
        }
    },

    onNoticeTap(e) {
        const { capsule, mode, action, actionLeft } = this;
        if (capsule || (mode === 'link' && actionLeft === '' && action === '')) {
            this.$emit('onTap', e)
        }
    },

    onOperationTap(e) {
        const { mode, action } = this;
        if (mode || action !== '') {
            this.$emit('onTap', e)
        }
    },

    onActionLeftTap(e) {
        const { actionLeft } = this;
        if (actionLeft !== '') {
            this.$emit('onTapLeft', e)
        }
    },
}
