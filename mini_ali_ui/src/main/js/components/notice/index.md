# Notice 通告栏组件

当应用有重要公告或者由于用户的刷新操作产生提示反馈时可以使用通告栏系统。通告栏不会对用户浏览当前页面内容产生影响，但又能明显的引起用户的注意。公告内容不超过一行。 说明：

* 仅用于 UI 展示没有对应的业务逻辑功能。

## 截图
![Image text](../../../../../../gifs/Notice.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| mode | String | - | closable、link | 右侧 icon 类型：关闭、箭头 | - |
| action | String | - | - | 右侧文本按钮文案 | - |
| action-left | String | - | - | 右侧第二个按钮文案 | - |
| @on-tap | EventHandle | () => {} | - | 点击右侧按钮回调 | - |
| @on-tap-left | EventHandle | () => {} | - | 点击右侧第二个按钮回调 | - |
| is-show | Boolean | true | - | 是否显示 notice | - |
| enable-marquee | Boolean | false | - | 是否开启动画 | - |
| show-icon | Boolean | true | - | 是否显示 icon | - |
| capsule-item | Array | - | - | 胶囊通告栏的业务 logo url | - |
| capsule | Boolean | false | - | 是否为胶囊通告栏 | - |
| type | String | normal | normal、error、active、transparent | 通告栏类型 | - |

## Bug & Tip
* 如果 `action` 没有任何值，那么 `actionLeft` 将不会显示；
* `@on-tap`、`@on-tap-left` 相对应于 `action` 和 `action-left`；
* 当 `mode` 的值为 `link`，显示为一个箭头 icon 时，整条通告栏是可点击的；
* 当 `action` 有值时，将会代替 `mode` 中的 `closable` 和 `link`，只会显示文字；
* `capsule-item` 在胶囊通告栏中只会显示 3 个，超过部分仅统计个数，但不会显示 logo；
* 当 `type` 的值为 `transparent` 时，展示的是带有透明度，内容色为白色的通告栏；

## 代码示例
```xml
<element name='notice' src="../../../../../../../mini_ali_ui/src/main/js/components/notice/index.hml"></element>
    <div class="demo-item">
        <notice
                marquee-Props="{{ marqueeProps }}"
                enable-Marquee="{{ true }}"
                is-show="{{ closeShow }}"
                type="{{ noticeType }}"
                mode="{{ noticeMode }}"
                action="{{ actionText }}"
                @on-Tap="actionClick"
                action-Left="{{ actionLeftText }}"
                icon-Color="{{ iconColor }}"
                @on-Tap-Left="linkActionClick"
                title="无限循环滚动的通告栏展示情况。文字不够继续添加文字凑数。">
        </notice>
    </div>

    <div class="demo-item">
        <notice
                enable-Marquee="{{ true }}"
                loop="{{ 1 }}"
                is-show="{{ closeShow }}"
                show-Icon="{{ false }}"
                type="{{ noticeType }}"
                mode="{{ noticeMode }}"
                action="{{ actionText }}"
                @on-Tap="actionClick"
                action-Left="{{ actionLeftText }}"
                icon-Color="{{ iconColor }}"
                @on-Tap-Left="linkActionClick"
                title='滚动一次的通告栏展示情况。文字不够继续添加文字凑数。无 icon'>
        </notice>
    </div>
    <div class="demo-item">
        <notice class="demo-notice"
                enable-Marquee="{{ false }}"
                is-show="{{ closeShow }}"
                type="{{ noticeType }}"
                mode="{{ noticeMode }}"
                action="{{ actionText }}"
                @on-Tap="actionClick"
                action-Left="{{ actionLeftText }}"
                icon-Color="{{ iconColor }}"
                @on-Tap-Left="linkActionClick"
                title="不滚动的通告栏展示情况。">
        </notice>
    </div>
    <div class="demo-item" style="justify-content: center;">
        <notice
                is-show="{{ closeShow }}"
                type="{{ noticeType }}"
                mode="{{ noticeMode }}"
                action="{{ actionText }}"
                @on-Tap="actionClick"
                action-Left="{{ actionLeftText }}"
                @on-Tap-Left="linkActionClick"
                capsule="{{ true }}"
                capsule-item="{{ capsuleItem }}">
            <text>{{ capsuleItem.length > 0 ? capsuleItem.length + ' 个优惠信息推荐' : '暂无优惠信息推荐' }}</text>
        </notice>
    </div>
```

```javascript
export default {
  data:{
    marqueeProps: {
      loop: true,
      leading: 1000,
      trailing: 800,
      fps: 40,
    },
  },
  linkActionClick() {
    prompt.showToast({
      message: '左侧操作区被点击了',
      duration: 1000,
    })
  },
  actionClick() {
    prompt.showToast({
      message: '你点击了右侧操作区',
      duration: 1000,
    })
  },
}
```
