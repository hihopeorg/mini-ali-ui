import fmtEvent from '../../common/_util/fmtevent';
export default{
    props: {
		positioned:{default:false},
        maskZindex: {default:0},
        // product: 产品弹窗蒙层；market：营销弹窗蒙层；
        type: {default:'product'},
//        onMaskTap: function onMaskTap() {},
        showed: {default:true},
        fixmaskfull: {default:false}
    },
    onMaskClick(e) {
        this.$emit("onMaskTap",fmtEvent(this.props, e, this));
    },
};