# Mask 遮罩蒙层

可用于需要遮罩蒙层的弹层元素。

## 截图
![mask 遮罩蒙层](../../../../../../gifs/mask.gif)


## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| type | String | product | product、market | 显示不同透明度的蒙层 | - | true |
| showed | Boolean | true | - | 是否显示蒙层 | - | - |
| @on-mask-tap | EventHandle | () => { } | - | 蒙层点击事件 | - | - |
| fix-mask-full | Boolean | false | - | 用以解决遮罩层受到 `transform` 影响而显示不全的问题 | | - |

## 代码示例

```xml
<am-mask type="{{type}}" showed="{{showed}}" @on-mask-tap="maskClick"></am-mask>
```

```javascript
export default{
    data: {
        type: 'market',
        showed:true,
    },
    maskClick() {
        if (this.type === 'market') {
            this.type= 'product';
        } else {
            this.type= '';
            this.showed= false;
        }
    },
};
```