import fmtEvent from '../../common/_util/fmtevent';
import fmtUnit from '../../common/_util/fmtunit';
import getI18n from '../../common/_util/getI18n';

const i18n = getI18n().amountInput;

export default{
  props: {
    type: {
      default:'number',
    },
    className:{
      default:'',
    } ,
    focus:{
      default:false,
    }  ,
    placeholder:{
      default: '',
    },
    value:{
      default:'',
    }  ,
    controlled:{
      default:true,
    }  ,
    showClear:{
      default:false,
    }  ,
    focusAfterClear:{
      default:true,
    }  ,
    title:{
      default:'',
    },
    extra: {
      default:'',
    },
    btnText: {
      default:'',
    },
    maxLength: {
      default:10,
    }
  },
  data: {
    mfocus: false,
    munit: '',
    iconSize: fmtUnit(22),
  },
  onInit() {
    this.didUpdate(this.focus, this.focus);
    this.$watch('focus', 'didUpdate');
    this.$watch('value', 'didUpdate');
  },
  onPageShow() {
    this.getMoneyUnit(this.value);
    this.mfocus = this.focus;
  },
  didUpdate(nowFocus, prevFocus) {
    if (prevFocus !== nowFocus) {
      this.mfocus = nowFocus;
    }
    this.getMoneyUnit(this.value);
  },
    onInput(e) {
      const event = fmtEvent(this.props, e, this);
      this.$emit('onInput', event);
      this.getMoneyUnit(e.value);
    },
    onConfirm(e) {
      const event = fmtEvent(this.props, e, this);
      this.$emit('onConfirm', event);
    },
    onButtonClick() {
      this.$emit('onButtonTap', {});
    },
    onFocus(e) {
      this.mfocus = true;
      const event = fmtEvent(this.props, e);
      this.$emit('onFocus', event);
      this.getMoneyUnit(e.value);
    },
    onBlur(e) {
      this.mfocus = false;
      const event = fmtEvent(this.props, e);
      this.$emit('onBlur', event);
      this.getMoneyUnit(e.value);
    },
    onClearTap() {
      if (this.focusAfterClear) {
        this.mfocus = true;
      }
      this.$emit('onClear', {});
    },
    getMoneyUnit(inputValue) {
      const value = Math.floor(inputValue);
      if (value > 999.99 && value < 10000) {
        this.munit= i18n.thousand;
      } else if (value > 9999.99 && value < 100000) {
        this.munit= i18n.tenThousand;
      } else if (value > 99999.99 && value < 1000000) {
        this.munit= i18n.hundredThousand;
      } else if (value > 999999.99 && value < 10000000) {
        this.munit= i18n.million;
      } else if (value > 9999999.99 && value < 100000000) {
        this.munit= i18n.tenMillion;
      } else if (value > 99999999.99 && value < 1000000000) {
        this.munit= i18n.hundredMillion;
      } else if (value > 999999999.99 && value < 10000000000) {
        this.munit= i18n.billion;
      } else {
        this.munit= '';
      }
    },
};
