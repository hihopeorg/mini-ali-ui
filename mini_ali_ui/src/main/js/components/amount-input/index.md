# amount-input 金额输入

金额输入框。

## 扫码体验 

![image](../../../../../../gifs/amount-input.gif)

## 属性介绍


| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| --- | --- | --- | --- | --- | --- | --- |
| type | String | number |  | input 的类型，有效值：digit、number |  | false |
| title | String |  |  | 左上角标题 |  | false |
| extra | String |  |  | 左下角说明 |  | false |
| value | String |  |  | 输入框当前值 |  | false |
| btn-text | String |  |  | 右下角按钮文案 |  | false |
| placeholder | String |  |  | placeholder |  | false |
| focus | Boolean | false |  | 自动获取光标 |  | false |
| @on-input | (e: Object) => void |  |  | 键盘输入时触发 |  | false |
| @on-focus | (e: Object) => void |  |  | 获取焦点时触发 |  | false |
| @on-blur | (e: Object) => void |  |  | 失去焦点时触发 |  | false |
| @on-confirm | (e: Object) => void |  |  | 点击键盘完成时触发 |  | false |
| @on-clear | () => void |  |  | 点击 clear 图标触发 |  | false |
| @on-button-click | () => void |  |  | 点击右下角按钮时触发 |  | false |
| max-length | Number |  |  | 最多允许输入的字符个数 |  | false |
| controlled | Boolean | false |  | 是否为受控组件。为 true时，value内容会完全受setData控制 |  | false |
| show-clear | Boolean | false |  | 是否一直显示清除 icon |  |  |
| focus-after-clear | Boolean | true |  | 清除 icon 触发后，输入框是否获得焦点 |  |  |


## 代码示例

```xml
<element name="amount-input" src="../../../../../../../mini_ali_ui/src/main/js/components/amount-input/index"></element>
<div style="flex-direction: column;">
  <amount-input type="digit"
    title="转入金额"
    extra="建议转入¥100以上金额"
    placeholder="输入金额"
    value="{{value}}"
    max-length="10"
    focus="{{true}}"
    btn-text="全部提现"
    @on-clear="onInputClear"
    @on-input="onInput"
    @on-button-tap="onButtonClick"
    @on-confirm="onInputConfirm"
    @on-blur="onInputBlur"
    @on-focus="onInputFocus"></amount-input>
</div>
```

```javascript
import prompt from '@system.prompt';
export default{
  data: {
    value: 200,
  },
  onInputClear() {
    this.value = '';
  },
  onInputConfirm(e) {
    console.log(e);
    prompt.showToast({
      message:'confirmed',
      duration:1500,
    });
  },
  onInput(e) {
    console.log(e);
    const { value } = e.detail;
    this.value = value;
  },
  onButtonClick() {
    prompt.showToast({
      message:'button clicked',
      duration:1500,
    });
  },
  onInputFocus() {
  },
  onInputBlur() {
  },
};

```
