# Calendar 日历

单日历组件。

## 预览
![Calendar 日历](../../../../../../gifs/calendar.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String | - | - | 自定义 class | 暂不支持 | - |
| type | String | single | single、range | 日期选择模式 | - | - |
| have-year | Boolean | false | - | 是否展示年份控制箭头 | - | - |
| prev-month-disable | Boolean | false | - | 前一个月份箭头禁用 | - | - |
| prev-year-disable | Boolean | false | - | 前一个年份箭头禁用 | - | - |
| nextv-month-disable | Boolean | false | - | 后一个月份箭头禁用 | - | - |
| next-year-disable | Boolean | false | - | 后一个年份箭头禁用 | - | - |
| tag-data | Array | - | `[{ date: '日期', tag: '标签', tagColor: 1, disable: true,},]`，可设置多个不同日期的标签内容，颜色以及是否禁用。| - | - ||
| handle-select | EventHandle | ([startDate, endDate]) => void | - | 选择区间时的回调 | - | - |
| month-change | EventHandle | (currentMonth, prevMonth) => void | - | 点击切换月份时回调，带两个参数currentMonth切换后月份和prevMonth切换前月份 | - | - |
| year-change | EventHandle | (currentYear, prevYear) => void | - | 点击切换年份时回调，带两个参数currentYear切换后年份和prevYear切换前年份 | - | - |
| handle-change | EventHandle | (current, prev) => void | - | 年/月变化时回调，带两个对象，每个均携带year和month信息 | - | - |
| select-has-disable-date | EventHandle | (currentMonth, prevMonth) => void | - | 选择区间包含不可用的日期 | - | - |

## Bug & Tip
* `tagColor` 共有 5 中颜色：
  * `1`: #ff6010,
  * `2`: #00b578,
  * `3`: #ff8f1f,
  * `4`: #1677ff,
  * `5`: #999,
* `prev-month-disable`、`prev-year-disable`、`nextv-month-disable` 以及 `next-year-disable` 四个主要控制日历上的箭头是否可点击使用，可根据实际业务场景来使用；
* `tag-data` 中的 `disable` 是可选项，如某日期需要提示禁用不可点时才需要增加，当不可用时，`tag` 以及 `tag-color` 将不会展示；
* 月份计数从 0 开始，即 0 代表 1 月份，以此类推，月份返回值 11 代表 12 月份；

## 代码示例

```hml
<element name="am-calendar" src="../../../../../../../mini_ali_ui/src/main/js/components/calendar/index.hml"></element>

<div class="am-calendar">
  <am-calendar
          type="single"
          have-year="{{ true }}"
          tag-data="{{ tagData }}"
          handle-select="{{ handleSelectTap }}"
          month-change="{{ monthChangeTap }}"
          year-change="{{ yearChangeTap }}"
          handle-change="{{ handleChangeTap }}"
          select-has-disable-date="{{ selectHasDisableDateTap }}">
  </am-calendar>
</div>
```

```css
.am-calendar{

}
```

```javascript
import prompt from '@system.prompt';

export default {
  data: {
    tagData: [
      {date: '2020-02-14', tag: '颜色 1', tagColor: 1},
      {date: '2020-02-28', tag: '公积金', tagColor: 2},
      {date: '2020-02-24', tag: '颜色 3', tagColor: 3},
      {date: '2020-02-18', tag: '颜色 4', tagColor: 4},
      {date: '2020-02-4', tag: '还房贷', tagColor: 5},
      {date: '2020-02-10', tag: '公积金', disable: true},
    ],
  },
  onInit() {
    const getDate = new Date();
    const getYear = getDate.getFullYear();
    const getMonth = getDate.getMonth();
    let m = getMonth + 1;
    if (m.toString().length === 1) {
      m = '0' + m;
    }
    this.tagData = [
      {date: getYear + '-' + m + '-14', tag: '颜色 1', tagColor: 1},
      {date: getYear + '-' + m + '-28', tag: '公积金', tagColor: 2},
      {date: getYear + '-' + m + '-24', tag: '颜色 3', tagColor: 3},
      {date: getYear + '-' + m + '-18', tag: '颜色 4', tagColor: 4},
      {date: getYear + '-' + m + '-4', tag: '还房贷', tagColor: 5},
      {date: getYear + '-' + m + '-10', tag: '公积金', disable: true},
    ];
  },
  handleSelectTap() { //选择区间时的回调
  },
  monthChangeTap() { //点击切换月份时的回调
  },
  yearChangeTap() { //点击切换年份时的回调
  },
  handleChangeTap() { //年/月变化时的回调
  },
  selectHasDisableDateTap() { //选择了区间包含不可用的日期
    prompt.showToast({
      message: "SelectHasDisableDate",
    });
  },
};
```