# Coupon 票券

票券组件可应用于各种优惠券、红包、票等可兑换利益的虚拟资产。

## 预览

![Image text](../../../../../../gifs/Coupon.gif)

## 属性

| 属性名        | 类型     | 默认值 | 可选值 | 描述                   | 必选 |
| ------------- | -------- | ------ | ------ | ---------------------- | ---- |
| thumb         | String   | -      | -      | Coupon缩略图地址       | -    |
| title         | String   | -      | -      | Coupon标题             | true |
| sub-title      | String   | -      | -      | Coupon副标题           | -    |
| @on-coupon | Function | -      | -      | Coupon点击时的事件回调 | -    |
| extra | Boolean | true | - | 票券是否展示左侧扩展信息 | - |
| more-btn | String | 规则详情 | - | - |
| more-hide | Boolean | true | - | - |
| used | Boolean | false | - | - |

## Slot

| slot name | 描述           |
| --------- | -------------- |
| action    | 票券右侧的插槽 |
| date      | 票券到期时间的插槽 |
| detail    | 票券规则详情的插槽 |
| category  | 票券左侧票券类别的插槽 |

## 示例

```xml
<element name='coupon' src="../../../../../../../mini_ali_ui/src/main/js/components/coupon/index.hml"></element>
<element name='ali_button' src="../../../../../../../mini_ali_ui/src/main/js/components/button/index.hml"></element>
<element name='am-checkbox' src="../../../../../../../mini_ali_ui/src/main/js/components/am-checkbox/index.hml"></element>
<element name='ali-stepper' src="../../../../../../../mini_ali_ui/src/main/js/components/stepper/index.hml"></element>
<div style="width : 100%; min-height : 100%; background-color : #F5F5F5; flex-direction : column; overflow : scroll;">
    <coupon title="券标题1"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}"
            style="margin-top : 10px">
    </coupon>

    <coupon title="券标题2"
            subtitle="券副标题"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}">
    </coupon>

    <coupon title="券标题3"
            subtitle="券副标题"
            used="{{ true }}"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}"
            detail="true">
        <text slot="date">有效期：2020.02.14-2020.02.29</text>
        <div slot="detail" class="coupon_rule">
            <text>1、详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明；</text>
            <text>2、详细规则说明详细规则说明规则说明详细规则说明详细规则说明；</text>
        </div>
    </coupon>

    <coupon title="券标题4"
            subtitle="券副标题"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}"
            category="true"
            detail="true">
        <div slot="category" class="categoryDemo">
            <text class="price">50</text>
            <text class="unit">元</text>
            <text class="type">满减券</text>
        </div>
        <ali_button shape="capsule" slot="action" @on-Tap="onButtonTap" type="ghost">立即使用</ali_button>
        <text slot="date">有效期：2020.02.14-2020.02.29</text>
        <div slot="detail" class="coupon_rule">
            <text>1、详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明；</text>
            <text>2、详细规则说明详细规则说明规则说明详细规则说明详细规则说明；</text>
        </div>
    </coupon>

    <coupon title="券标题5"
            subtitle="券副标题"
            @on-Coupon="onCouponClick"
            extra="{{ false }}"
            detail="true">
        <ali_button shape="capsule" slot="action" @on-Tap="onButtonTap" type="ghost">立即使用</ali_button>
        <text slot="date">有效期：2020.02.14-2020.02.29</text>
        <div slot="detail" class="coupon_rule">
            <text>1、详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明；</text>
            <text>2、详细规则说明详细规则说明规则说明详细规则说明详细规则说明；</text>
        </div>
    </coupon>

    <coupon title="券标题6"
            subtitle="券副标题"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}"
            >
        <ali_button shape="capsule" slot="action" @on-Tap="onButtonTap" type="ghost">立即使用</ali_button>
    </coupon>

    <coupon title="券标题7"
            subtitle="券副标题"
            more-Btn="查看更多"
            more-Hide="{{ false }}"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}"
            detail="true">
        <ali_button shape="capsule" slot="action" @on-Tap="onButtonTap" type="primary">立即使用</ali_button>
        <text slot="date">有效期：2020.02.14-2020.02.29</text>
        <div slot="detail" class="coupon_rule">
            <text>1、详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明详细规则说明；</text>
            <text>2、详细规则说明详细规则说明规则说明详细规则说明详细规则说明；</text>
        </div>
    </coupon>

    <coupon title="券标题8"
            subtitle="券副标题"
            @on-Coupon="onCouponClick"
            thumb="{{ thumb }}">
        <am-checkbox slot="action" onTap="onButtonTap"></am-checkbox>
    </coupon>

    <div>
        <coupon title="券标题9"
                subtitle="券副标题"
                @on-Coupon="onCouponClick"
                thumb="{{ thumb }}">
            <ali-stepper
                    slot="action"
                    step="{{ 1 }}"
                    show-Number="{{true}}"
                    min="{{2}}"
                    input-Width="90"
                    ></ali-stepper>
        </coupon>
    </div>
</div>
```

```css
.container {
  padding-bottom: 50px;
}
.coupon_rule {
  flex-direction: column;
  display: flex;
  margin-bottom: 4px;
}

/* 左侧权益内容的样式 slot="category" */
.categoryDemo {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-content: center;
  align-items: baseline;
  align-self: flex-start;
}
.categoryDemo .price {
  font-size: 30px;
  color: #FF6010;
}
.categoryDemo .unit {
  padding-left: 2px;
  font-weight: bold;
  font-size: 13px;
  color: #FF6010;
}
.categoryDemo .type {
  text-align: center;
  font-size: 11px;
  color: #999999;
}
```

### js
```javascript
export default{
  data: {
    thumb: 'common/images/head.jpg',
  },

  onCouponClick(data) {
    if (data.detail.used) {
      return false;
    } else {
      prompt.showDialog({
        message: "可用票券，票券点击事件",
        buttons: [{text:'确定', color: '#1677ff'}]
      })
    }
  },
  onButtonTap() {
    prompt.showDialog({
      message: "胶囊按钮点击事件",
      buttons: [{text:'确定', color: '#1677ff'}]
    })
  },
}
```