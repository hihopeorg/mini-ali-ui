
export default {
    props: {
        title: {
            default: ''
        },
        subtitle: {
            default: ''
        },
        used: {
            default: false
        },
        // 票券的扩展类型
        extra: {
            default: true
        },
        moreBtn: {
            default: "规则详情"
        },
        moreHide: {
            default: true
        },
        thumb: {
            default: ''
        },
        category: {
            default: false
        },
        detail: {
            default: false
        }
    },

    onInit() {
    },
    onCouponClick() {
        this.$emit("onCoupon", {used:this.used})
    },
    catchActionTap(e) {
        this.$emit("onTap", e)
    },
    changeMoreState() {
        this.moreHide = !this.moreHide;
    },
}
