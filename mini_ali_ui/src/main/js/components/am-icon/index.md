# am-icon icon 图标

图标。

## 预览
![am-icon icon 图标](../../../../../../gifs/am-icon.gif)

## 属性介绍

| 属性名 | 类型 | 默认值 | 可选项 | 描述 | 最低版本 | 必填 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| class-name | String| - | - | 自定义 class | 暂不支持 | - |
| size | String | 30 | - | 设置 icon 尺寸大小 | - | - |
| color | String | #333333 | - | 设置 icon 的颜色 | - | - |
| type | String | - | - | 选择使用 icon 的类型 | - | true |

### type 有效值列表
| 图标类型 | type 值列表 |
| ---- | ---- |
| 线条型 | `add`、 `add-message`、 `add-square`、 `alipay`、 `ant`、 `appx`、 `barcode`、 `bill`、 `bill-note`、 `capslock`、 `chat`、 `check`、 `circle`、 `close`、 `close-circle`、 `collect`、 `contacts`、 `content`、 `down`、 `down-circle`、 `download`、 `eye`、 `eye-close`、 `file`、 `gift`、 `good`、 `help`、 `home`、 `koubei`、 `left`、 `like`、 `limit`、 `link`、 `location`、 `logo-alipays`、 `minus-square`、 `money`、 `money-circle`、 `more`、 `more-1`、 `net`、 `people`、 `person`、 `person-add`、 `person-setting`、 `picture`、 `play`、 `qr`、 `question`、 `receipt`、 `right`、 `sad`、 `scan`、 `scan-code`、 `search`、 `selected`、 `setting`、 `share`、 `text`、 `time-5`、 `trash`、 `up`、 `voice`、 `voice-limit`、 `wallet`、 `warn`、 `zoom-in`、 `zoom-out` |
| 实心型 | `address-book_`、 `ant_`、 `apps_`、 `certified-check_`、 `certified-warn_`、 `chat_`、 `check_`、 `close_`、 `delete_`、 `delete-person_`、 `down_`、 `edit_`、 `eye_`、 `eye-limit_`、 `forbid_`、 `help_`、 `key_`、 `koubei_`、 `like_`、 `location_`、 `lock_`、 `logo-alipay_`、 `mail_`、 `microphone_`、 `pen_`、 `people_`、 `person-circle_`、 `person-delete_`、 `phone_`、 `phone-book_`、 `question_`、 `sad_`、 `star_`、 `time-3_`、 `time-5_`、 `voice-limit_`、 `wait_`、 `warn_` |

## Bug & Tip
* `size` 只需要填写纯数字，默认增加 `px` 单位，比如 `size="45"`，最终得到的就是 `45px` 的结果；若不填写默认继承父级元素的尺寸大小。
* `color` 默认为空，可继承父级元素的颜色值；

## 代码示例

```hml
<element name="am-icon" src="../../../../../../../mini_ali_ui/src/main/js/components/am-icon/index"></element>

<div class="icon-list">
    <input class="searchInput" id="sid" onchange="searchIcon" placeholder="查找 icon"/>
    <block if="{{ searchTemp.length > 0 }}">
        <div class="icon-list-view">
            <block for="{{ searchTemp }}">
                <div class="icon-list-view-item">
                    <am-icon type="{{ $item }}" size="45" color="" class-name=""></am-icon>
                    <text class="icon-list-view-item-desc">{{ $item }}</text>
                </div>
            </block>
        </div>
    </block>
    <block else>
        <div class="icon-list-view">
            <block for="{{ iconTypes }}">
                <div class="icon-list-view-item">
                    <am-icon type="{{ $item }}" size="45" color="" class-name=""></am-icon>
                    <text class="icon-list-view-item-desc">{{ $item }}</text>
                </div>
            </block>
        </div>
    </block>
</div>
```

```css
.icon-list {
    display: flex;
    flex-direction: column;
    padding-bottom: 10px;
    background-color: #fff;
}
.icon-list-view {
    flex-wrap: wrap;
}
.icon-list-view-item {
    width: 33.33%;
    height: 111px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}
.icon-list-view-item-desc {
    margin-top: 10px;
    text-align: center;
    text-overflow: ellipsis;
    prefer-font-sizes: 15px 17px 19px;
}
.searchInput {
    width: 100%;
    margin-top: 5px;
    color: #f32600;
    background-color: #f5f5f5;
    border-bottom: 3px solid #eee;
}
```

```javascript
import prompt from '@system.prompt';

export default {
    data: {
        iconTypes: [
            'qr',
            'share',
            'picture',
            'add-square',
            'file',
            'text',
            'minus-square',
            'barcode',
            'wallet',
            'scan-code',
            'receipt',
            'down-circle',
            'bill-note',
            'trash',
            'bill',
            'scan',
            'content',
            'circle',
            'play',
            'limit',
            'money',
            'link',
            'zoom-in',
            'koubei',
            'location',
            'capslock',
            'time-5',
            'warn',
            'help',
            'close-circle',
            'selected',
            'search',
            'net',
            'chat',
            'contacts',
            'appx',
            'question',
            'person-setting',
            'setting',
            'like',
            'ant',
            'add',
            'more',
            'more-1',
            'zoom-out',
            'money-circle',
            'collect',
            'voice',
            'good',
            'voice-limit',
            'people',
            'person-add',
            'download',
            'sad',
            'left',
            'right',
            'eye-close',
            'eye',
            'koubei_',
            'star_',
            'check',
            'chat_',
            'help_',
            'key_',
            'lock_',
            'people_',
            'voice-limit_',
            'location_',
            'phone_',
            'logo-alipay_',
            'person-delete_',
            'wait_',
            'apps_',
            'microphone_',
            'pen_',
            'close_',
            'question_',
            'down_',
            'certified-check_',
            'certified-warn_',
            'sad_',
            'ant_',
            'time-5_',
            'warn_',
            'person-circle_',
            'time-3_',
            'check_',
            'logo-alipays',
            'like_',
            'home',
            'eye_',
            'edit_',
            'mail_',
            'forbid_',
            'eye-limit_',
            'delete-person_',
            'close',
            'address-book_',
            'person',
            'gift',
            'add-message',
            'alipay',
            'phone-book_',
            'delete_',
            'down',
            'up',
        ],
        searchTemp: [],
    },
    searchIcon(e) {
        let inputValue = e.value;
        let mythis = this;
        let searchTemp = [];
        this.iconTypes.forEach((eValue) => {
            if (eValue.match(inputValue)) {
                searchTemp.push(eValue);
                mythis.searchTemp = searchTemp;
            }
            if (searchTemp.length == 0) {
                prompt.showToast({
                    message: "查无此am-icon"
                })
            }
        });
    },
};
```
