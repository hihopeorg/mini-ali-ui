import I18n_zhCN from '../_lang/zh-cn';
import I18n_enUS from '../_lang/en-us';

export default function getI18n() {
  try {
    /* global getApp */
    /* eslint no-undef: "error" */
    const appMiniAliUI = getApp() || null;
    if (appMiniAliUI) {
      if (appMiniAliUI.globalData?.miniAliUiLang === 'en-US') {
        return I18n_enUS;
      } else {
        return I18n_zhCN;
      }
    } else {
      return I18n_zhCN;
    }
  } catch (error) {
    return I18n_zhCN;
  }
}
