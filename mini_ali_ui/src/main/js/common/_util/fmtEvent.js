export default function fmtEvent(props, e, component) {
    const dataset = {};
    var targetDataSet = e.target.dataSet;
    if (targetDataSet) {
        for (const key in targetDataSet) {
            dataset[key] = targetDataSet[key];
        }
    }
    var attr = e.target.attr;
    if (attr) {
        for (const key in attr) {
            dataset[key] = attr[key];
        }
    }
    if (component !== undefined) {
        for (const idx in props) {
            let key = props[idx];
            if ('dataset' !== key) {
                if ((/data/gi).test(key)) {
                    var newKey = key.replace(/data/gi, '');
                    newKey = newKey.substring(0, 1).toLowerCase() + newKey.substring(1, newKey.length);
                    dataset[newKey] = component[key];
                } else {
                    dataset[key] = component[key];
                }
            }
        }
        if (component.dataset) {
            Object.assign(dataset, component.dataset);
        }
    }
    return Object.assign({}, e, {
        currentTarget: {
            dataset
        },
        target: {
            dataset, targetDataset: dataset
        },
    });
}
