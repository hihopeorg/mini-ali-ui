# mini-ali-ui

>mini-ali-ui是一个实用的三方UI组件的扩展库,覆盖了大量常用自定义组件场景

#### 效果展示
![gif](gifs/am-switch.gif)

![gif](gifs/calendar.gif)

由于组件较多，可查看每个组件目录下的index.md文件

## 下载安装
#### 安装教程
1. 下载mini-ali-ui源码。
2. 拷贝mini-ali-ui源码中的 mini_ali_ui/src/main/js/ 文件夹到自己的工程文件夹下。
3. 在自己项目中引用拷贝的文件。

#### 使用说明：

第一步： 在hml文件中引入组件，src相对路径根据拷贝位置可能存在改动；
```html
<element name="am-radio" src="../../../../../../../mini_ali_ui/src/main/js/components/am-radio/index"></element>
```

第二步：创建组件节点：
```html
                    <am-radio value="{{ $item.value }}" checked="{{ $item.checked }}" disabled="{{ $item.disabled }}"
                              id="{{ $item.id }}" @on-change="onChange"></am-radio>
```

- 注意:导入请求入口js文件时需要根据自己的项目路径;具体详细使用方法可查看项目的案例。

#### 接口说明
详见mini-ali-ui/src/main/js/components/下各个子文件夹下的index.md文件

#### 兼容性
支持 OpenHarmony API version 8 及以上版本。


#### 目录结构
````
├─entry
│  └─src
│      └─main
│          ├─js
│          │  └─MainAbility
│          │      ├─i18n
│          │      └─pages
│          │          ├─alphabet
│          │          ├─……
│          └─resources
├─mini_ali_ui
│  └─src
│      └─main
│          ├─js
│          │  ├─common
│          │  └─components
│          │     ├─am-checkbox
│          │     ├─……
│          └─resources
├─README.MD
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/mini-ali-ui/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/mini-ali-ui/pulls) 。

## 开源协议
本项目基于 [MIT LICENSE](https://gitee.com/hihopeorg/mini-ali-ui/blob/master/LICENSE) ，请自由地享受和参与开源。