import router from '@system.router';
export default{
  data: {
    title: '操作成功',
    subTitle: '内容详情可折行，建议不超过两内容。也可以通过 slot="tips" 插入更具有功能性的提示。',
    type: 'success',
    iconColor:'#1677FF',
    items: [
      { name: 'success', value: 'success', checked: true ,iconColor:'#1677FF'},
      { name: 'fail', value: 'fail' ,iconColor:'#FF3B30'},
      { name: 'info', value: 'info' ,iconColor:'#1677FF'},
      { name: 'warn', value: 'warn' ,iconColor:'#FF8F1F'},
      { name: 'waiting', value: 'waiting' ,iconColor:'#00B578'},
    ],
    mainButton:null,
    subButton:null,
  },
  onInit() {
  },
  goBack() {
    router.back();
  },
  radioChange(a,e) {
    let targetValue = JSON.stringify(e).split(",")[1].split(":")[1].split('"')[1];
    this.type=targetValue;
    let targetIcon = this.items.filter((item)=>{
      return item.value == targetValue
    });
    this.iconColor = targetIcon[0].iconColor;
  },
  titleChange(e) {
    this.title= e.value;
  },
  subtitleChange(e) {
    this.subTitle = e.text;
  },
  onChange(e) {
    if (JSON.stringify(e).split(",")[0].split(":")[1] == "true") {
      this.mainButton= {buttonText: '主要操作' };
      this.subButton= {buttonText: '辅助操作' };
    } else {
      this.mainButton= null;
      this.subButton= null;
    }
  },
};
