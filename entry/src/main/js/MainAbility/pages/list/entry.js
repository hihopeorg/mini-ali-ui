import router from '@system.router';
import ability_featureAbility from '@ohos.ability.featureAbility';
export default {
  data: {
    componentList: [
      {
        components: [
          {
            type: '布局导航',
            list: [
              {
                name: 'Container',
                nameZN: '容器',
                path: 'pages/container/index',
                ability: 'ContainerAbility'
              },
              {
                name: 'Title',
                nameZN: '模块标题',
                path: 'pages/title/index',
                ability: 'MainAbility'
              },
              {
                name: 'List',
                nameZN: '列表',
                path: 'pages/list/index',
                ability: 'ListAbility'
              },
              {
                name: 'List-item',
                nameZN: '列表元素',
                path: 'pages/new-list/index',
                ability: 'NewListAbility'
              },
              {
                name: 'Tabs',
                nameZN: 'Tabs',
                path: 'pages/tabs/index',
                ability: 'TabsAbility'
              },
              {
                name: 'VTabs',
                nameZN: 'VTabs',
                path: 'pages/vtabs/index',
                ability: 'MainAbility'
              },
//              {
//                name: 'Tabs elevator',
//                nameZN: 'Tabs 电梯组件',
//                path: 'pages/tabs/elevator/elevator',
//              },
              {
                name: 'Card',
                nameZN: '卡片',
                path: 'pages/card/index',
                ability: 'MainAbility'
              },
              {
                name: 'Coupon',
                nameZN: '票券',
                path: 'pages/coupon/index',
                ability: 'MainAbility'
              },
              {
                name: 'Grid',
                nameZN: '宫格',
                path: 'pages/grid/index',
                ability: 'MainAbility'
              },
              {
                name: 'Steps',
                nameZN: '步骤进度条',
                path: 'pages/steps/index',
                ability: 'MainAbility'
              },
              {
                name: 'Footer',
                nameZN: '页脚',
                path: 'pages/footer/index',
                ability: 'MainAbility'
              },
              {
                name: 'Terms',
                nameZN: '协议',
                path: 'pages/terms/index',
                ability: 'TermsAbility'
              },
              {
                name: 'Flex',
                nameZN: 'Flex 布局',
                path: 'pages/flex/index',
                ability: 'MainAbility'
              },
              {
                name: 'Collapse',
                nameZN: '折叠面板',
                path: 'pages/collapse/index',
                ability: 'MainAbility'
              },
            ],
          },
        ],
      },
      {
        components: [
          {
            type: '操作浮层',
            list: [
              {
                name: 'Popover',
                nameZN: '气泡菜单',
                path: 'pages/popover/index',
                ability: 'MainAbility'
              },
              {
                name: 'Filter',
                nameZN: '筛选',
                path: 'pages/filter/index',
                ability: 'MainAbility'
              },
              {
                name: 'Modal',
                nameZN: 'Modal',
                path: 'pages/modal/index',
                ability: 'MainAbility'
              },
              {
                name: 'PopUp',
                nameZN: '弹出菜单',
                path: 'pages/popup/index',
                ability: 'MainAbility'
              },
            ],
          },
        ],
      },
      {
        components: [
          {
            type: '结果类',
            list: [
              {
                name: 'Page-result',
                nameZN: '结果页',
                path: 'pages/page-result/index',
                ability: 'MainAbility'
              },
              {
                name: 'Message',
                nameZN: '信息状态、操作结果',
                path: 'pages/message/index',
                ability: 'MessageAbility'
              },
            ],
          },
        ],
      },
      {
        components: [
          {
            type: '提示引导',
            list: [
              {
                name: 'Tips',
                nameZN: '小提示、向导提示',
                path: 'pages/tips/index',
                ability: 'TipsAbility'
              },
              {
                name: 'Notice',
                nameZN: '公告',
                path: 'pages/notice/index',
                ability: 'MainAbility'
              },
              {
                name: 'Badge',
                nameZN: '红点，徽标',
                path: 'pages/badge/index',
                ability: 'MainAbility'
              },
              {
                name: 'Tag',
                nameZN: ' 标签',
                path: 'pages/tag/index',
                ability: 'TagAbility'
              },
              {
                name: 'Mask',
                nameZN: '背景蒙层',
                path: 'pages/mask/mask',
                ability: 'MainAbility'
              },
              {
                name: 'Guide',
                nameZN: '遮罩引导',
                path: 'pages/guide/guide',
                ability: 'GuideAbility'
              },
              {
                name: 'Avatar',
                nameZN: '头像',
                path: 'pages/avatar/index',
                ability: 'MainAbility'
              },
            ],
          },
        ],
      },
      {
        components: [
          {
            type: '表单类',
            list: [
              {
                name: 'Form',
                nameZN: '表单',
                path: 'pages/form/index',
                ability: 'FormAbility'
              },
              {
                name: 'Input-item',
                nameZN: '输入框',
                path: 'pages/input-item/index',
                ability: 'MainAbility'
              },
              {
                name: 'Picker-item',
                nameZN: '选择框',
                path: 'pages/picker-item/index',
                ability: 'PickerItemAbility'
              },
              {
                name: 'Verify-code',
                nameZN: '验证码框',
                path: 'pages/verify-code/index',
                ability: 'MainAbility'
              },
              {
                name: 'Long-password',
                nameZN: '长密码框',
                path: 'pages/long-password/index',
                ability: 'MainAbility'
              },
              {
                name: 'Multi-liner',
                nameZN: '多行输入',
                path: 'pages/multi-liner/index',
                ability: 'MainAbility'
              },
              {
                name: 'Amount-input',
                nameZN: '金额输入',
                path: 'pages/amount-input/index',
                ability: 'AmountInputAbility'
              },
              {
                name: 'Button',
                nameZN: '按钮',
                path: 'pages/button/index',
                ability: 'ButtonAbility'
              },
              {
                name: 'Am-switch',
                nameZN: '开关',
                path: 'pages/am-switch/index',
                ability: 'MainAbility'
              },
              {
                name: 'Am-checkbox',
                nameZN: '复选框',
                path: 'pages/am-checkbox/index',
                ability: 'AmCheckboxAbility'
              },
              {
                name: 'Am-radio',
                nameZN: '单选框',
                path: 'pages/am-radio/index',
                ability: 'MainAbility',
              },
              {
                name: 'Search-bar',
                nameZN: '搜索框',
                path: 'pages/search-bar/index',
                ability: 'MainAbility'
              },
            ],
          },
        ],
      },
      {
        components: [
          {
            type: '手势类',
            list: [
              {
                name: 'Swipe-action',
                nameZN: '滑动删除',
                path: 'pages/swipe-action/index',
                ability: 'SwipeActionAbility'
              },
            ],
          },
        ],
      },
      {
        components: [
          {
            type: '其他',
            list: [
              {
                name: 'Pagination',
                nameZN: '分页',
                path: 'pages/pagination/index',
                ability: 'PaginationAbility'
              },
              {
                name: 'Alphabet',
                nameZN: '字母检索表',
                path: 'pages/alphabet/index',
                ability: 'MainAbility'
              },
              {
                name: 'Stepper',
                nameZN: 'Stepper',
                path: 'pages/stepper/index',
                ability: 'StepperAbility'
              },
              {
                name: 'Calendar',
                nameZN: '日历',
                path: 'pages/calendar/index',
                ability: 'MainAbility'
              },
              {
                name: 'Am-icon',
                nameZN: '图标',
                path: 'pages/am-icon/index',
                ability: 'MainAbility'
              },
              {
                name: 'Loading',
                nameZN: 'loading 加载',
                path: 'pages/loading/loading',
                ability: 'MainAbility'
              },
            ],
          },
        ],
      },
    ],
  },
  goToPage(item) {
    if (item.ability != 'MainAbility') {
      ability_featureAbility.startAbility({
        want:{
          bundleName: 'cn.openharmony.mini_ali_ui',
          abilityName: 'com.example.entry.' + item.ability
        }
      })
    } else {
      router.push({
        uri: item.path
      });
    }
  }
};
