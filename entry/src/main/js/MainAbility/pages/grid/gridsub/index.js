import prompt from '@system.prompt';
import router from '@system.router';
export default{
  data: {
    gridType: '5',
    list1: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述文字最多一行描述文字最多一行描述文字最多一行描述文字最多一行',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字标题文字',
        desc: '描述文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字标题文字',
        desc: '描述文字最多一行描述文字最多一行描述文字最多一行描述文字最多一行描述文字最多一行',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述文字最多一行',
      },
    ],
    list2: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字标题文字标题文字标题文字标题文字标题文字',
      },
    ],
    list31: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字标题文字标题文字标题文字标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
    ],
    list3: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字标题文字标题文字标题文字',
        desc: '描述信息',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述信息描述信息描述信息描述信息描述信息描述信息描述信息',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述信息',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述信息',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述信息',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述信息',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
        desc: '描述信息',
      },
    ],
    list4: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
    ],
    list42: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字标题文',
      },
    ],
    list5: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
    ],
    list55: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '6标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
    ],
    list56: [
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },
      {
        icon: '/common/images/VBqNBOiGYkCjqocXjdUj.png',
        text: '标题文字',
      },

    ],
  },
  onInit() {
    let params = router.getParams();
    if (params) {
      this.gridType = router.getParams()['gridType']
    }
  },
  onItemClick(ev) {
    let message = String(ev.detail.index)
    prompt.showDialog({
      title: '',
      message: message,
      buttons:[{text:'确定', color: '#1677ff'}]
    })
  },
};
