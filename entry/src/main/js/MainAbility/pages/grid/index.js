import router from '@system.router';
export default{
  openPage(e) {
    router.push({
      uri: 'pages/grid/gridsub/index',
      params: { gridType: e.target.attr.dataset.url}
    })
  },
};
