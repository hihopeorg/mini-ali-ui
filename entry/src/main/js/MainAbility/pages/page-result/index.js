import router from '@system.router';
export default{
  data: {
    components: [
      {
        name: 'network',
        nameZN: '网络',
        path: 'network',
      },
      {
        name: 'local-network',
        nameZN: '局部网络',
        path: 'local-network',
      },
      {
        name: 'busy',
        nameZN: '服务繁忙',
        path: 'busy',
      },
      {
        name: 'local-busy',
        nameZN: '局部服务繁忙',
        path: 'local-busy',
      },
      {
        name: 'error',
        nameZN: '系统错误',
        path: 'error',
      },
      {
        name: 'local-error',
        nameZN: '局部系统错误',
        path: 'local-error',
      },
      {
        name: 'logoff',
        nameZN: '用户已注销',
        path: 'log-off',
      },
      {
        name: 'local-logoff',
        nameZN: '局部用户已注销',
        path: 'local-logoff',
      },
      {
        name: 'empty',
        nameZN: '页面空状态',
        path: 'empty',
      },
      {
        name: 'local-empty',
        nameZN: '局部空状态',
        path: 'local-empty',
      },
      {
        name: 'payment',
        nameZN: '付款失败',
        path: 'payment',
      },
      {
        name: 'local-payment',
        nameZN: '局部付款失败',
        path: 'local-payment',
      },
      {
        name: 'redpacket',
        nameZN: '红包领空',
        path: 'redpacket',
      },
      {
        name: 'local-redpacket',
        nameZN: '局部红包领空',
        path: 'local-redpacket',
      },
    ],
  },
  openPage(e) {
    router.push({
      uri: 'pages/page-result/pageresultsub/index',
      params: {
        pageType: e.target.dataSet.url
      }
    });
  },
};
