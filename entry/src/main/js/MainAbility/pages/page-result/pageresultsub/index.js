import router from '@system.router';
export default{
  data: {
    pageType: 'empty',
    footer_busy: [{
      text: '刷新',
    }],
    footer_error: [{ text: '刷新' }],
    footer_local_payment: [{
               text: '返回',
    }],
    footer_network: [{
               text: '修复',
             }, {
               text: '刷新',
    }],
    footer_payment: [{
               text: '返回',
    }],
    footer_redpacket: [{
               text: '操作1',
             }, {
               text: '操作2',
    }],
  },
  onTapLeft(e) {
    console.log(e, 'onTapLeft');
  },
  onTapRight(e) {
    console.log(e, 'onTapRight');
  },
  onBack(e) {
    router.back();
  },
  onInit() {
    let params = router.getParams();
    if (params) {
      this.pageType = params['pageType']
    }
  }
};
