export default {
  data: {
    activeIndex: 1,
    failIndex: 0,
    classNamed:true,
    size:20,
    items: [{
              index:0,
              title: '步骤1',
            }, {index:1}, {
      index:2,
              title: '步骤3',
            }],
    items2: [{
               index:0,
               title: '步骤1 如果标题足够长的话也会换行的',
               description: '这是步骤1的描述文档，文字够多的时候会换行，设置了成功的icon',
               activeIcon: 'https://i.alipayobjects.com/common/favicon/favicon.ico',
               size: 20,
             }, {
               index:1,
               description: '这是步骤2，同时设置了两种状态的icon',
               icon: 'https://gw.alipayobjects.com/mdn/miniProgram_mendian/afts/img/A*lVojToO-qZIAAAAAAAAAAABjAQAAAQ/original',
               activeIcon: 'https://i.alipayobjects.com/common/favicon/favicon.ico',
             }, {
               index:2,
               title: '步骤3',
               description: '这是步骤3',
             }, {
               index:3,
               title: '步骤4',
             }],
    showNumberSteps: true,
  },
  nextStep() {
          this.activeIndex=this.activeIndex + 1;
  },
  preStep() {
      if(this.activeIndex>=0){
          this.activeIndex=this.activeIndex - 1;
      }
  },
  setFailIndex() {
    this.failIndex= 3;
  },
  cancelFailIndex() {
      this.failIndex= -1;
  },
  setIconSizeAdd() {
    this.size= this.size < 30 && this.size > 14 ? this.size + 1 : 15;
  },
  setIconSizeReduce() {
    this.size= this.size > 15 ? this.size - 1 : 15;
  },
  showNumberList() {
    this.showNumberSteps=!this.showNumberSteps;
  },
};
