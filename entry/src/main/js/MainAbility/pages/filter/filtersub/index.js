import prompt from '@system.prompt';
import router from '@system.router';

export default{
  data: {
    filterType: 'alternative',
    isShow: true,
    items_a: [
      { id: 1, value: '衣服', selected: true },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣架' },
      { id: 3, value: '数码产品' },
      { id: 4, value: '防盗门' },
      { id: 5, value: '椅子' },
      { id: 7, value: '显示器' },
      { id: 6, value: '某最新款电子产品' },
      { id: 8, value: '某某某某某牌电视游戏底座' },
    ],
    items_s: [
      { id: 1, value: '衣服', selected: true },
      { id: 1, value: '橱柜' },
    ],
    items_s1: [
      { id: 1, value: '衣服啊', selected: true },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣服' },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣服' },
      { id: 1, value: '橱柜' },
      { id: 1, value: '衣服' },
      { id: 1, value: '橱柜' },
      { id: 1, value: '橱柜' },
    ],
    items_s2: [
      {
        id: 1, value: '衣服', subtitle: '子标题', selected: true
      },
      {
        id: 1, value: '橱柜', subtitle: '子标题'
      },
    ],
  },
  onInit() {
    let params = router.getParams();
    if (params) {
      this.filterType = params['filterType']
    }
  },
  handleCallBack(data) {
    prompt.showDialog({
      message: JSON.stringify(data.detail.results),
      buttons: [{text:'确定', color: '#1677ff'}]
    })
  },

  toggleFilter() {
    this.isShow = !this.isShow
  },
}
