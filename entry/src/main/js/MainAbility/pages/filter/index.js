import router from '@system.router';

export default{

  openPage(e) {
    router.push({
      uri: 'pages/filter/filtersub/index',
      params: { filterType:e.target.dataSet.url }
    })
  },
}
