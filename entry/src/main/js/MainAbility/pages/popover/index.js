const position = ['top', 'topRight', 'rightTop', 'right', 'rightBottom', 'bottomRight', 'bottom', 'bottomLeft', 'leftBottom', 'left', 'leftTop', 'topLeft'];
import prompt from '@system.prompt';
export default{
  data: {
    position: position[0],
    showed:false,
    showMask: false,
    showIcon: true
  },
  onShowPopoverTap() {
    console.info('onShowPopoverTap in')
    this.showed=!this.showed;
  },
  relieve(){
    this.showed=false;
  },
  onNextPositionTap() {
    let index = position.indexOf(this.position);
    index = index >= position.length - 1 ? 0 : index + 1;
    this.showed=true;
    this.position=position[index];
  },
  onMaskChangeTap() {
    this.showMask=!this.showMask;
  },
  onIconChangeTap() {
    this.showIcon=!this.showIcon;
  },
  onMaskClick() {
    this.showed=false;
  },
  itemTap1(e){
    prompt.showToast(
      {message:JSON.stringify(`点击_${e.detail.currentTarget.dataset.index}`),
      }
    );
  },
  itemTap2(e) {
    prompt.showToast(
      {message:JSON.stringify(`点击_${e.detail.currentTarget.dataset.index}`),
      }
    );
  },
};