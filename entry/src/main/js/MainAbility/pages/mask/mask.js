export default{
    data: {
        type: 'market',
        showed:true,
    },
    maskClick() {
        if (this.type === 'market') {
            this.type= 'product';
        } else {
            this.type= '';
            this.showed= false;
        }
    },
};