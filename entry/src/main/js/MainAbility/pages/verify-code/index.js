import prompt from '@system.prompt';
export default{
  data: {
    verifyCode: '',
  },
  onSend() {
    prompt.showDialog({
      message: 'verify code sent',
      buttons : [{text:'确认', color:'#1677ff'},],
    });
  },
  onInput(e) {
    this.verifyCode = e.text;
  },
  onClear() {
    this.verifyCode = '';
  },
};
