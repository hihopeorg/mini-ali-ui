import prompt from '@system.prompt';

export default{
  data: {
    thumb: 'common/images/head.jpg',
    expand3rd: false,
  },
  onCardClick(ev) {
    prompt.showToast({
      message: ev.detail.info,
      duration: 2000,
    });
  },
  onActionClick() {
    prompt.showToast({
      message: "action clicked",
      duration: 2000,
    });
  },
  onExtraActionClick() {
    prompt.showToast({
      message: "extra action clicked",
      duration: 2000,
    });
  },
  toggle() {
    this.expand3rd = !this.expand3rd
  },
}
