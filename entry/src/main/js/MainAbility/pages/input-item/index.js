import prompt from '@system.prompt';
export default{
  data: {
    cardNo: '1234****',
    name: '',
    layerShow1: '',
    layerShow2: '垂直输入框的布局',
    layerShow3: 'disabled 状态的 input',
  },
  onExtraTap() {
    prompt.showToast({
      message: 'extra tapped',
      duration: 2000,
      bottom: 400
    })
  },
  onItemInput() {},
  onItemFocus() {},
  onItemBlur() {},
  onItemConfirm() {},
  onClear(e) {
    e.target.dataSet.field = ''
  },
  onSend() {
    prompt.showToast({
      message: 'verify code sent',
      duration: 2000,
      bottom: 400
    })
  },
};
