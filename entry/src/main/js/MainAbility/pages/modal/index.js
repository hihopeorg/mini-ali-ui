import prompt from '@system.prompt';
export default{
  data: {
    modalOpened: false,
    modalOpened2: false,
    modalOpened21: false,
    modalOpened22: false,
    modalOpened222: false,
    modalOpened23: false,
    modalOpened3: false,
    modalOpened4: false,
    modalOpened5: false,
    modalOpened6: false,
    modalOpened7: false,
    modalOpened8: false,
    modalOpened9: false,
    buttons5: [
      { text: '取消' },
      { text: '主操作', extClass: 'buttonBold' },
    ],
    buttons6: [
      { text: '主操作', extClass: 'buttonBold' },
      { text: '取消' },
    ],
    buttons7: [
      { text: '取消', extClass: 'cancelBtn' },
      { text: '删除', extClass: 'deleteBtn' },
    ],
    buttons8: [
      { text: '主操作', extClass: 'buttonBold' },
      { text: '更多' },
      { text: '取消' },
    ],
  },
  /* 通用modal */
  openModal() {
    this.modalOpened = true;
  },
  onModalClick() {
    this.modalOpened = false;
  },
  onModalClose() {
    this.modalOpened = false;
  },
  /* 带图弹窗-大图 */
  openModal2() {
    this.modalOpened2 = true;
  },
  onModalClick2() {
    this.modalOpened2 = false;
  },
  onModalClose2() {
    this.modalOpened2 = false;
  },
  /* 带图弹窗 */
  openModal21() {
    this.modalOpened21 = true;
  },
  onModalClick21() {
    this.modalOpened21 = false;
  },
  onModalClose21() {
    this.modalOpened21 = false;
  },
  onMaskClick21() {
    this.modalOpened21 = false;
  },
  /* 带图弹窗-小图 */
  openModal22() {
    this.modalOpened22 = true;
  },
  onModalClick22() {
    this.modalOpened22 = false;
  },
  onModalClose22() {
    this.modalOpened22 = false;
  },
  /* 带图弹窗-中图 */
  openModal222() {
    this.modalOpened222 = true;
  },
  onModalClick222() {
    this.modalOpened222 = false;
  },
  onModalClose222() {
    this.modalOpened222 = false;
  },
  /* 带图弹框-关闭icon */
  openModal23() {
    this.modalOpened23 = true;
  },
  onModalClick23() {
    this.modalOpened23 = false;
  },
  onModalClose23() {
    this.modalOpened23=false;
  },
  /* 运营活动弹框-大 */
  openModal3() {
    this.modalOpened3= true;
  },
  onModalClick3() {
    this.modalOpened3= false;
  },
  /* 运营活动弹框-小 */
  openModal4() {
    this.modalOpened4= true;
  },
  onModalClick4() {
    this.modalOpened4= false;
  },
  /* 双按钮弹框 */
  openModal5() {
    this.modalOpened5= true;
  },
  onButtonClick5(e) {
    const  detail  = e._detail;
    this.modalOpened5= false;
    prompt.showDialog(
      {
        message:JSON.stringify(`点击了：${JSON.stringify(detail)}`)
      }
    );
  },
  /* 双按钮弹框-竖排 */
  openModal6() {
    this.modalOpened6= true;
  },
  onButtonClick6(e) {
    const  detail  = e._detail;
    this.modalOpened6= false;
    prompt.showDialog(
      {
        message:JSON.stringify(`点击了：${JSON.stringify(detail)}`)
      }
    );
  },
  /* 弹框自定义按钮样式 */
  openModal7() {
    this.modalOpened7= true;
  },
  onModalClose7() {
    this.modalOpened7= false;
  },
  onButtonClick7(e) {
    const  detail  = e._detail;
    this.modalOpened7= false;
    prompt.showDialog(
      {
        message:JSON.stringify(`点击了：${JSON.stringify(detail)}`)
      }
    );
  },
  /* 三按钮弹框 */
  openModal8() {
    this.modalOpened8= true;
  },
  onButtonClick8(e) {
    const  detail  = e._detail;
    this.modalOpened8= false;
    prompt.showDialog(
      {
        message:JSON.stringify(`点击了：${JSON.stringify(detail)}`)
      }
    );
  },
  /* 自定义弹框内容 */
  openModal9() {
    this.modalOpened9= true;
  },
  onModalClose9() {
    this.modalOpened9= false;
  },
};
