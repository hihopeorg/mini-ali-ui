export default{
    data: {
        showLeft: false,
        showRight: false,
        showTop: false,
        showBottom: false,
    },
    onTopBtnTap() {
        this.showTop=true;
    },
    onRightBtnTap() {
        this.showRight=true;
    },
    onLeftBtnTap() {
        this.showLeft=true;
    },
    onButtomBtnTap() {
        this.showBottom= true;
    },
    onPopupClose() {
        this.showLeft= false;
        this.showRight= false;
        this.showTop= false;
        this.showBottom= false;
    },
};
