import prompt from '@system.prompt';

export default{
  data: {
    closeShow: true,
    closeActionShow: true,
    marqueeProps: {
      loop: true,
      leading: 1000,
      trailing: 800,
      fps: 40,
    },
    capsuleItem: [],
    capsuleItemList: [
      {
        name: 'https://img.alicdn.com/tfs/TB1yTvnfQY2gK0jSZFgXXc5OFXa-145-145.png',
        value: 'https://img.alicdn.com/tfs/TB1yTvnfQY2gK0jSZFgXXc5OFXa-145-145.png',
      },
      {
        name: 'https://img.alicdn.com/tfs/TB1egTmfNz1gK0jSZSgXXavwpXa-145-145.png',
        value: 'https://img.alicdn.com/tfs/TB1egTmfNz1gK0jSZSgXXavwpXa-145-145.png',
      },
      {
        name: 'https://img.alicdn.com/tfs/TB1l36mfQP2gK0jSZPxXXacQpXa-145-145.png',
        value: 'https://img.alicdn.com/tfs/TB1l36mfQP2gK0jSZPxXXacQpXa-145-145.png',
      },
      {
        name: 'https://img.alicdn.com/tfs/TB1uCUdfND1gK0jSZFyXXciOVXa-151-164.png',
        value: 'https://img.alicdn.com/tfs/TB1uCUdfND1gK0jSZFyXXciOVXa-151-164.png',
      },
    ],
    type: [
      { name: 'normal', value: 'normal', checked: true },
      { name: 'error', value: 'error' },
      { name: 'active', value: 'active' },
      { name: 'transparent', value: 'transparent' },
    ],
    noticeType: 'normal',
    mode: [
      { name: '', value: '无', checked: true },
      { name: 'link', value: '箭头' },
      { name: 'closable', value: '关闭' },
    ],
    noticeMode: '',
    actionText: '',
    actionLeftText: '',
    iconColor: '#ff6010'
  },
  capsuleItemChange(e) {
    const index = e.target.dataSet.index;
    if (e.checked) {
      this.capsuleItem.push(this.capsuleItemList[index].name)
    } else {
      for(var i = 0; i < this.capsuleItem.length; i++) {
        if (this.capsuleItem[i] === this.capsuleItemList[index].name){
          this.capsuleItem.splice(i, 1)
        }
      }
    }
  },
  getContent(e) {
    this.setData({
      noticeContent: e.detail.value,
    });
  },
  typeChange(e) {
    const noticeType = e.value;
    if (noticeType === 'normal') {
      this.iconColor = '#ff6010'
    } else if(noticeType === 'error' || noticeType === 'transparent') {
      this.iconColor = '#ffffff'
    } else if(noticeType === 'active') {
      this.iconColor = '#1677ff'
    }
    this.noticeType = noticeType
  },
  modeChange(e) {
    this.noticeMode = e.value;
    if (e.value !== '') {
      this.actionText = '';
    } else {
      this.actionText = '';
      this.actionLeftText = '';
    }
  },
  getRightText(e) {
    this.actionText = e.value;
  },
  getLeftText(e) {
    this.actionLeftText = e.value;
  },
  linkActionClick() {
    prompt.showToast({
      message: '左侧操作区被点击了',
      duration: 1000,
    })
  },
  actionClick() {
    prompt.showToast({
      message: '你点击了右侧操作区',
      duration: 1000,
    })
  },
}
