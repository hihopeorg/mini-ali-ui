import prompt from '@system.prompt';

export default{

  data: {},
  onLoad() {},
  titleGo() {
    prompt.showToast({
      message: '点击箭头，可设置跳转',
      duration: 2000
    })
  },
  titleMore() {
    prompt.showToast({
      message: '点击更多，展开气泡菜单',
      duration: 2000
    })
  },
  titleClose() {
    prompt.showToast({
      message: '点击关闭，可设置关闭',
      duration: 2000
    })
  },
}
