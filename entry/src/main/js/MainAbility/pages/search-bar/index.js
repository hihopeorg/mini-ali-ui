import prompt from '@system.prompt';
export default{
  data: {
    value: '',
    showVoice: false,
  },
  handleInput() {

  },
  handleClear() {

  },
  handleCancel() {
  },
  handleSubmit(value) {

    prompt.showDialog({
      message: value,
      buttons:[{text:'确定', color: '#1677ff'}]
    })
  },
  onChange(e) {
    this.showVoice = e.checked
  },
};
