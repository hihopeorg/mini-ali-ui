import prompt from '@system.prompt';

export default{
  data: {
    items: [
      { checked: true, disabled: false, value: 'a', desc: '单选框-默认选中', id: 'checkbox1' },
      { checked: false, disabled: false, value: 'b', desc: '单选框-默认未选中', id: 'checkbox2' },
    ],
    items1: [
      { checked: true, disabled: true, value: 'c', desc: '单选框-默认选中disabled', id: 'checkbox3' },
    ],
  },
  onSubmit(e) {
    prompt.showToast({
      message: e.detail.value.lib,
    });
  },
  onReset() {
  },
  radioChange() {
  },
  onChange(e) {
    var checked = e._detail.checked;
    var id = e._detail.target.dataset.id;
    if (id === 'checkbox1') {
      this.items[0].checked = checked;
      this.items[1].checked = !checked;
    } else {
      this.items[1].checked = checked;
      this.items[0].checked = !checked;
    }
  }
};
