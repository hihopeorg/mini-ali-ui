import prompt from '@system.prompt';

export default{
  data: {
    thumb: 'common/images/head.jpg',
  },

  onCouponClick(data) {
    if (data.detail.used) {
      return false;
    } else {
      prompt.showDialog({
        message: "可用票券，票券点击事件",
        buttons: [{text:'确定', color: '#1677ff'}]
      })
    }
  },
  onButtonTap() {
    prompt.showDialog({
      message: "胶囊按钮点击事件",
      buttons: [{text:'确定', color: '#1677ff'}]
    })
  },
}
