export default{
  data: {
    longPassword: '',
  },
  onInput(e) {
    this.longPassword = e.detail.value
  },
  onClear() {

    this.longPassword = ''

  },
};
