import prompt from '@system.prompt';
export default {
  data: {
    alphabet: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
  },
  onAlphabetClick(ev) {
    prompt.showToast({
      message: JSON.stringify(ev._detail.data)
    });
  },
};
