import prompt from '@system.prompt';
export default{
  data: {
    value: 200,
  },
  onInputClear() {
    this.value = '';
  },
  onInputConfirm(e) {
    console.log(e);
    prompt.showToast({
      message:'confirmed',
      duration:1500,
    });
  },
  onInput(e) {
    console.log(e);
    const { value } = e.detail;
    this.value = value;
  },
  onButtonClick() {
    prompt.showToast({
      message:'button clicked',
      duration:1500,
    });
  },
  onInputFocus() {
  },
  onInputBlur() {
  },
};
