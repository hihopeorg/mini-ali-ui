import prompt from '@system.prompt';
export default{
  data: {
    tabs: [
      {
        title: '选项',
        subTitle: '描述文案',
        number: '6',
        showBadge: true,
        badge: {
          arrow: true,
          stroke: true,
        },
      },
      {
        title: '选项二',
        subTitle: '描述文案描述',
        number: '66',
        showBadge: true,
        badge: {
          arrow: false,
          stroke: true,
        },
      },
      {
        title: '3 Tab',
        subTitle: '描述',
        number: '99+',
        showBadge: true,
        badge: {
          arrow: true,
        },
      },
      { title: '4 Tab',
        subTitle: '描述',
        showBadge: true,
        number: 0,
      },
      { title: '5 Tab',
        subTitle: '描述描述',
        number: '99+',
        showBadge: false,
      },
      { title: '3 Tab',
        subTitle: '描述',
        showBadge: false,
      },
      { title: '4 Tab',
        subTitle: '描述',
      },
      { title: '15 Tab',
        subTitle: '描述',
      }],
    activeTab: 0,
    isSwipeable: false,
    type: [
      { name: 'normal', value: '普通', checked: true },
      { name: 'capsule', value: '胶囊', checked: false },
      { name: 'hasSubTitle', value: '带描述', checked: false },
    ],
    typeCapsule: false,
    typeHasSubTitle: false,
    plus: [
      { name: 'has', value: '是', checked: true },
      { name: 'hasnt', value: '否', checked: false },
    ],
    swipeable: [
      { name: '是', value: '是', checked:false },
      { name: '否', value: '否', checked: true },
    ],
    hasPlus: true,
    contentHeight: [
      { name: 'has', value: '是', checked: false },
      { name: 'hasnt', value: '否', checked: true },
    ],
    // hasContentHeight: false,
    tabsNumber: [
      { name: '1', value: '一条', checked: false },
      { name: '2', value: '两条', checked: false },
      { name: '3', value: '三条', checked: false },
      { name: '-1', value: '很多', checked: true },
    ],
    isShowFilter: false,
  },
  tabsNumberChange(e) {
    if (e.value === '1') {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
          },
        ];
    } else if (e.value === '2') {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: true,
            },
          },
          {
            title: '选项二',
            subTitle: '描述文案描述',
            number: '66',
            showBadge: true,
          },
        ];
    } else if (e.value === '3') {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: true,
            },
          },
          {
            title: '选项二',
            subTitle: '描述文案描述',
            number: '66',
            showBadge: true,
          },
          {
            title: 'Tab',
            subTitle: '描述',
            number: '99+',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: false,
            },
          },
        ];
    } else {
      this.tabs = [
          {
            title: '选项',
            subTitle: '描述文案',
            number: '6',
            showBadge: true,
            badge: {
              arrow: true,
              stroke: true,
            },
          },
          {
            title: '选项二',
            subTitle: '描述文案描述',
            number: '66',
            showBadge: true,
            badge: {
              arrow: false,
              stroke: true,
            },
          },
          {
            title: '3 Tab',
            subTitle: '描述',
            number: '99+',
            showBadge: true,
            badge: {
              arrow: true,
            },
          },
          { title: '4 Tab',
            subTitle: '描述',
            showBadge: true,
            number: 0,
          },
          { title: '5 Tab',
            subTitle: '描述描述',
            number: '99+',
            showBadge: false,
          },
          { title: '3 Tab',
            subTitle: '描述',
            showBadge: false,
          },
          { title: '4 Tab',
            subTitle: '描述',
          },
          { title: '15 Tab',
            subTitle: '描述',
          },
        ];
    }
  },
  typeChange(e) {
    if (e.value === 'hasSubTitle') {
      this.typeCapsule = true;
      this.typeHasSubTitle = true;
    } else if (e.value === 'capsule') {
      this.typeCapsule = true;
      this.typeHasSubTitle = false;
    } else {
      this.typeCapsule = false;
      this.typeHasSubTitle = false;
    }
  },
  plusChange(e) {
    if (e.value === 'hasnt') {
      this.hasPlus = false;
    } else {
      this.hasPlus = true;
    }
  },
  swipeableChange(e) {
    if (e.value === '是') {
      this.isSwipeable = true;
    } else {
      this.isSwipeable = false;
    }
  },
  handleTabClick({ index, tabsname }) {
    this[tabsname] = index;
  },
  handleTabChange({ index, tabsname }) {
    this[tabsname] = index;
  },
  handlePlusClick() {
    prompt.showDialog({
      message:'plus clicked',
      buttons:[{text:'确定',color:'#1677ff'}],
    });
  },
  activeTabChange(e) {
    this.activeTab = parseInt(e.value);
  },
};
