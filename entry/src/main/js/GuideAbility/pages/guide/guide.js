export default{
  data: {
    list: [
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ',
        x: '75px',
        y: '100px',
        width: '200px',
        height: '200px',
      },
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ',
        x: '125px',
        y: '75px',
        width: '200px',
        height: '100px',
      },
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*XMCgSYx3f50AAAAAAAAAAABkARQnAQ',
        x: '175px',
        y: '150px',
        width: '100px',
        height: '100px',
      },
      {
        url: 'https://gw.alipayobjects.com/mdn/rms_ce4c6f/afts/img/A*gWo-TLFGp38AAAAAAAAAAABkARQnAQ',
        x: '200px',
        y: '200px',
        width: '100px',
        height: '150px',
      },
    ],
    guideShow: false,
    guideJump: true,
  },
  onLoad() {},
  closeGuide() {
    this.guideShow = false
  },
  onShowJumpGuide() {
    this.guideShow = true
    this.guideJump = true
  },
  onShowGuide() {
    this.guideShow = true
    this.guideJump =false
  },
};
