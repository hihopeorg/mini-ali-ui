import prompt from '@system.prompt';

export default{
  data: {},
  titleClick() {
    prompt.showDialog({
      title: 'onActionTap 回调',
      message: "标题后面操作区域点击",
      buttons:[{text:'确定', color: '#1677ff'}]
    });
  },
}
