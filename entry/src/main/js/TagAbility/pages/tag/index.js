export default {
  data: {
    useIcon:false,
    ghost:false,
  },
  setInfoIcon(e) {
    this.useIcon = e._detail.checked;
  },
  setInfoGhost(e) {
   this.ghost = e._detail.checked;
  }
};
