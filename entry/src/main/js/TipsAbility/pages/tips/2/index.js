import prompt from '@system.prompt';
export default{
    data: {
        content: '我知道了(2秒后消失)',
        time: 2000,
    },
    onClose() {
        prompt.showDialog({title:'主动关闭 tips'});
    },
    onTimeOut() {
        prompt.showDialog({title:'时间到了，关闭 tips'});
    },
};