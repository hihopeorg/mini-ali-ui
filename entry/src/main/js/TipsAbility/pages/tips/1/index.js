import prompt from '@system.prompt';
export default{
    data: {
        showRectangle: true,
        showDialog: true,
        arrowPositions: [
            'top-left',
            'top-center',
            'top-right',
            'left',
            'right',
            'bottom-left',
            'bottom-center',
            'bottom-right',
        ],
        useIcon:false,
        arrowPosIndex: 3,
        useButton: true,
        textvalue:'top-left',
    },
    onCloseTap() {
        this.showRectangle= false;
    },
    onRectangleTap() {
        prompt.showDialog({title:'do something'});
    },
    onDialogTap() {
        this.showDialog=false;
    },
    textonchange(e) {
        this.textvalue = e.newValue;
    },

    setInfo1(e) {
        if(e.checked){
            this.useIcon = true;
        }else{
            this.useIcon = false;
        }
    },
    setInfo2(e) {
        if(e.checked){
            this.useButton = true;
        }else{
            this.useButton = false;
        }
    },
};
