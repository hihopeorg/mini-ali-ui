import router from '@system.router';
export default{
    openPage(e) {
        router.push({
            uri:e.target.attr.dataset.url
        })
    },
};
