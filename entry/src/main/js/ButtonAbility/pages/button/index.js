export default{
  data: {
    title: '按钮操作 Normal',
    subtitle: '',
    disabled: false,
    dataName: '1',
    type: '',
    shape: 'default',
    capsuleSize: 'medium',
    capsuleMinWidth: false,
    showLoading: false,
    types: [
      { name: 'default', value: 'default', checked: true },
      { name: 'primary', value: 'primary', checked: false },
      { name: 'ghost', value: 'ghost', checked: false },
      { name: 'text', value: 'text', checked: false },
      { name: 'warn', value: 'warn', checked: false },
      { name: 'warn-ghost', value: 'warn-ghost', checked: false },
      { name: 'light', value: 'light', checked: false },
    ],
    shapes: [
      { name: 'default', value: 'default', checked: true },
      { name: 'capsule', value: 'capsule', checked:false },
    ],
    capsuleSizes: [
      { name: 'small', value: 'small' ,checked:false},
      { name: 'medium', value: 'medium', checked: true },
      { name: 'large', value: 'large' ,checked:false},
    ],
  },
  onLoad() {
  },
  typeChange(e) {
    var idx = e.target.attr.id;
    var oldStatus = this.types[idx].checked;
    if (oldStatus) {
      return;
    }
    for (var i = 0; i < this.types.length; i++) {
      this.types[i].checked = (i == idx) ? true : false;
    }
    this.type = this.types[idx].value;
  },
  shapeChange(e) {
    var idx = e.target.attr.id;
    var oldStatus = this.shapes[idx].checked;
    if (oldStatus) {
      return;
    }
    for (var i = 0; i < this.shapes.length; i++) {
      this.shapes[i].checked = (i == idx) ? true : false;
    }
    this.shape = this.shapes[idx].value;
  },
  sizeChange(e) {
    var idx = e.target.attr.id;
    var oldStatus = this.capsuleSizes[idx].checked;
    if (oldStatus) {
      return;
    }
    for (var i = 0; i < this.capsuleSizes.length; i++) {
      this.capsuleSizes[i].checked = (i == idx) ? true : false;
    }
    this.capsuleSize = this.capsuleSizes[idx].value;
  },
  titleChange(e) {
    this.title = e.value;
  },
  subtitleChange(e) {
    this.subtitle= e.value;
  },
  onDisableChange(e) {
    this.disabled=e._detail.checked;
  },
  onMinWidthChange(e) {
    console.info("onMinWidthChange in, e = " + JSON.stringify(e));
    this.capsuleMinWidth= e._detail.checked;
  },
  onTest() {
    // e.target.dataset.name
  },
  onLoadingChange(e) {
    this.showLoading= e._detail.checked;
  },
};
