import prompt from '@system.prompt';


export default{
  data: {
    inputValue: '内容',
    inputFocus: true,
    bank: [],
    name: '',
    input:'',
    verifyCode:'',
    longPassword:'',
    banks: ['网商银行', '建设银行', '工商银行', '浦发银行'],
    showPicker: false,
  },
  onAutoFocus() {
    this.inputFocus = true;
  },
  onExtraTap() {
    prompt.showDialog({
      title: 'extra tapped',
      buttons: [{text:'确认',color:'#1677ff'}],
    })
  },
  onItemInput() {

  },
  onItemFocus() {
    this.inputFocus = false;
  },
  onItemBlur() {},
  onItemConfirm() {},
  onClear() {

  },
  onPickerTap() {
    this.showPicker = true;
  },
  onSend() {
    prompt.showDialog({
      title: 'verify code sent',
      buttons: [{text:'确认',color:'#1677ff'}],
    })
  },
  forbid() {
    this.showPicker = false;
  },
  getItem(e){
    console.info("e = " + JSON.stringify(e));
    this.bank = [e];
    this.showPicker = false;
  }
}
