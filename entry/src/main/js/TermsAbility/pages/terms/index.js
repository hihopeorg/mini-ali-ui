const cfg = {
    c1: {
        related: false,
        agreeBtn: {
            title: '同意协议并开通',
            data: 'custom data 1',
        },
        cancelBtn: {
            title: '暂不开通，仅手动缴费',
            data: 'custom data 2',
        },
        hasDesc: false,
    },
    c2: {
        related: false,
        agreeBtn: {
            title: '同意协议并开通',
        },
        hasDesc: true,
    },
    c3: {
        related: true,
        agreeBtn: {
            checked: true,
            title: '提交',
        },
    },
    c4: {
        related: true,
        agreeBtn: {
            title: '提交',
        },
    },
    c5: {
        related: false,
        agreeBtn: {
            title: '同意协议并提交',
        },
    },
    c6: {
        related: true,
        fixed: true,
        agreeBtn: {
            checked: true,
            title: '提交',
            data: 'custom data 1',
        },
    },
};
import router from '@system.router';
import prompt from '@system.prompt';
export default{
    data: cfg,
    onInit() {
    },
    onSelect(e) {
        const selectedData = (e._detail.currentTarget.dataset.name || '').toString();
        prompt.showDialog({title:'Terms Btns',
        message:selectedData,
        buttons:[{text:"确定",color:"#1677ff"}]});
    },
    //没有类似navigator组件  用下面实现
    goWeb(){
        console.info("goWeb");
        router.push({uri:'pages/terms/webs/index'})
    },
};
