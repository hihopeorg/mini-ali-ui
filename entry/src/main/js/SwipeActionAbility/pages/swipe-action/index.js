import prompt from '@system.prompt';
export default{
  data: {
    swipeIndex: null,
    list: [
      { right: [{ type: 'delete', text: '删除', bgColor: '#f00', fColor: 'black' }], content: '更换文字颜色', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏', fColor: 'rgba(0,0,0,0.5)' }, { type: 'delete', text: '删除', fColor: '#FFFF00' }, { type: 'other', text: '新增一个',fColor: '#ffffff' }], content: '改变文字颜色', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏', bgColor: '#333333', fColor: 'white'}, { type: 'delete', text: '删除', fColor: '#ffffff'}], content: '其中一个背景色变化', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏', bgColor: '#cccccc', fColor: '#f00' }, { type: 'delete', text: '删除', bgColor: '#0ff', fColor: '#333' }], content: '文字和背景色同时改变', swiping: false },
      { right: [{ type: 'edit', text: '取消收藏取消收藏取消', fColor: 'white'}, { type: 'delete', text: '删除删除删除删除', fColor: 'white'}], content: '默认颜色样式', swiping: false },
      { right: [{ type: 'edit', text: '取消关注', fColor: 'white'}, { type: 'other', text: '免打扰', fColor: 'white' }, { type: 'delete', text: '删除', fColor: 'white' }], content: '三个选项的卡片', swiping: false },
      { right: [{ type: 'edit', text: '取消关注', fColor: 'white' }, { type: 'other', text: '免打扰', fColor: 'white' }, { type: 'delete', text: '删除', fColor: 'white' }], content: '三个选项的卡片三个选项的卡片三个...', swiping: false },
    ],
  },
  onRightItemClick(e) {
    const type = e.detail.type;
    const item = e.detail.item;
    const index = e.detail.index;
    prompt.showDialog({
      title: '温馨提示',
      message: item + "-item" + index + '-' + JSON.stringify(this.list[this.swipeIndex].right[item]),
      buttons: [{text:'取消', color: '#000000'}, {text:'确定', color: '#1677ff'}],
      success:  (result) => {
        if (result.index == 1) {
          if (type === 'delete') {
            prompt.showToast({
              message: '确认 => 可进行删除数据操作',
              duration: 2000
            })
          }
          this.list[this.swipeIndex].swiping = false;
        } else {
          prompt.showToast({
            message: '取消 => 滑动删除状态保持不变',
            duration: 2000
          })
        }
      }
    })
  },
  onItemTap(e) {
    prompt.showDialog({
      message: `dada${e.detail.index}`,
      buttons: [{text:'确定', color: '#1677ff'}]
    })
    for (var i = 0; i < this.list.length; i++) {
        this.list[i].swiping = false;
    }
  },
  onSwipe(e) {
    const { list } = this
    this.swipeIndex = e.detail.index;
    for (var i = 0; i < this.list.length; i++) {
      if (this.swipeIndex === i) {
        list[i].swiping = true;
      } else {
        list[i].swiping = false;
      }
    }

  },
}
